import React from 'react';
import '../src/dimsum.scss';
import './StoryContainer.scss';
const StoryContainer = story => {
  return <div className="dimsum">{story()}</div>;
};

export { StoryContainer };
