import { setOptions } from '@storybook/addon-options';
import { addDecorator, configure } from '@storybook/react';
import { StoryContainer } from './StoryContainer';

addDecorator(StoryContainer);
setOptions({
  name: 'DimSum',
  addonPanelInRight: true
});

const context = require.context('../src/components', true, /.stories.js$/);

function loadStories() {
  // note: the import sequence here determines order in left nav in storybook UI
  require('../src/docs/Welcome.stories');
  require('../src/docs/Grid/Grid.stories');
  context.keys().forEach(file => context(file));
}

configure(loadStories, module);
