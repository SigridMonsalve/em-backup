const path = require('path');
const eslintFormatter = require('react-dev-utils/eslintFormatter');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx|mjs)$/,
        enforce: 'pre',
        use: [
          {
            options: {
              formatter: eslintFormatter,
              eslintPath: require.resolve('eslint'),
              baseConfig: {
                extends: [require.resolve('eslint-config-react-app')]
              },
              ignore: false,
              useEslintrc: false
            },
            loader: require.resolve('eslint-loader')
          }
        ],
        include: path.resolve(__dirname, '..', 'src'),
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader?sourceMap'],
        include: path.resolve(__dirname, '../')
      },
      {
        test: /\.(woff|woff2|ttf|eot|svg)$/,
        loaders: ['file-loader']
      }
    ]
  }
};
