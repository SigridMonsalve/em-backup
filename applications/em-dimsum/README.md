> ## 🛠 Status: In Development
>
> DimSum is currently in development. It's on the fast track to a 1.0 release, so we encourage you to use it and give us your feedback, but there are things that haven't been finalized yet and you can expect some changes.

## DimSum [![Build Status](https://jenkins.dco.elmae/buildStatus/icon?job=Encompass-NextGen/subsystems/dimsum)](https://jenkins.dco.elmae/job/Encompass-NextGen/job/subsystems/job/dimsum/)

DimSum is a toolkit of reusable, styles, components and other interconnected parts that when combined make great experiences for Ellie Mae's users. These parts are systemic and logical, as they all follow the same universal principles.

The accompanying component library of DimSum, gives developers a collection of re-usable ReactJS components for building their products.

This repository hosts a reference implementation of [**Dim Sum Design System**](http://dimsum.elliemae.io/).

A live version of this implementation is available at [AWS](http://react.dimsum.ux.rd.elliemae.io.s3-website-us-east-1.amazonaws.com/) and [Github Pages](http://githubdev.dco.elmae/pages/design-system/dimsum/)

To see associated code for this implementation, please go to corresponding UI element in [Github](http://githubdev.dco.elmae/design-system/dimsum)

## Quick Start

* Ensure you followed all steps mentioned in [Developer Setup](#developer-setup).
* This project is based on [React Storybook](https://storybook.js.org). Read mode on [Why Storybook](#why-storybook).
* With Storybook, we have enabled these addons:
  * [actions](https://www.npmjs.com/package/@storybook/addon-actions)
  * [a11y](https://www.npmjs.com/package/@storybook/addon-a11y)
  * [viewport](https://www.npmjs.com/package/@storybook/addon-viewport)
  * [readme](https://www.npmjs.com/package/storybook-readme)
  * [Screenshots](https://github.com/tsuyoshiwada/storybook-chrome-screenshot)
* In addition to above addons, we are currently evaluating:
  * [storyshots](https://www.npmjs.com/package/@storybook/addon-storyshots)
  * [jest](https://www.npmjs.com/package/@storybook/addon-jest)
  * [background](https://www.npmjs.com/package/@storybook/addon-backgrounds)
  * [links](https://www.npmjs.com/package/@storybook/addon-links)
  * [storysource](https://www.npmjs.com/package/@storybook/addon-storysource)
  * [Info Addon](https://github.com/storybooks/storybook/tree/master/addons/info) and
  * [styles](https://github.com/Sambego/storybook-styles) addons.
* If you find a new storybook extension useful, please contact the [project contributors](http://githubdev.dco.elmae/design-system/dimsum/graphs/contributors)

## Why Storybook

* Storybook is a development environment for UI components. It allows you to browse a component library, view the different states of each component, and interactively develop and test components.
* Storybook runs outside of your app. This allows you to develop UI components in isolation, which can improve component reuse, testability, and development speed. You can build quickly without having to worry about application-specific dependencies.

## Project Structure
* TODO: Document project structure

## Developer Setup

* Install git for your OS

  * If you are on mac, launch Terminal.app and type command `git --version`. This will prompt you to install git if you don’t have it installed already.
  * For other OS and other installation methods for Mac, follow [official install method](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

* Install Node.js for your OS

  * If you are on Mac, [NVM](https://github.com/creationix/nvm#install-script) is preferred installation method.
  * If you are on Mac, but prefer Mac Package Managers (Homebrew, MacPorts or pkgsrc), use [Native](https://nodejs.org/en/download/package-manager/#macos) installation method.
  * If you are on Windows, follow the default [`.msi` install method](https://nodejs.org/en/)

* Install few packages globally

  `npm install -g yarn preup`

* Once you have git and node.js installed, clone this repo to your local machine using git clone command:

  ```
  git clone http://githubdev.dco.elmae/design-system/dimsum.git
  ```

* After cloning this repo locally, just run `setup` command:

  ```
  cd dimsum
  yarn run setup
  ```

* To run storybook locally, run `start` command:

  ```
  yarn start
  ```

  This will start local development server on http://localhost:5000

* To take screenshots of each stories, run `screenshot` command:

  ```
  yarn screenshot
  ```

  Read more about [storybook-chrome-screenshot](https://github.com/tsuyoshiwada/storybook-chrome-screenshot)

## Editor Setup

  * If you are using [Visual Studio Code](https://code.visualstudio.com/), add below extensions for better coding experience:

    * [React Food Truck](https://marketplace.visualstudio.com/items?itemName=burkeholland.react-food-truck) extensions.
    * [Complete JSDoc Tags](https://marketplace.visualstudio.com/items?itemName=HookyQR.JSDocTagComplete)
    * [Import Cost](https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost)

  * If you are using [WebStorm](https://www.jetbrains.com/webstorm/), add below plugins for better coding experience:

    * [Prettier](https://plugins.jetbrains.com/plugin/10456-prettier)

## Git Hooks Setup

  * This repository uses [Prettier](https://prettier.io/). Prettier is an opinionated code formatter with support for JavaScript (including ES2017), JSX, Flow, TypeScript, CSS, Less, SCSS, JSON, GraphQL, Markdown (including GFM). It removes all original styling\* and ensures that all outputted code conforms to a consistent style.
  * We use Prettier with a pre-commit tool. This re-formats your files that are marked as "staged" via git add before you commit. More documentation for such setup is available at [Prettier Website](https://prettier.io/docs/en/precommit.html#option-1-lint-staged-https-githubcom-okonet-lint-staged)
