#!/usr/bin/env bash

# This is very temporary code for creating new components based on template...
# Only works on unixy os... Replace with some sensible system...

components_path=src/components
template_path=template/component/*

component_name=$1
component_name_lower=`echo ${component_name} | tr "[:upper:]" "[:lower:]" `
component_path="${components_path}/$1"

echo "Generating new component at ${component_path}"

mkdir -p $component_path
cp $template_path $component_path

for i in ".stories." ".test." "."; do mv $component_path/{Component,$component_name}${i}js; done;
for i in "."; do mv $component_path/{Component,$component_name}${i}md; done;
for i in "."; do mv $component_path/{Component,$component_name}${i}scss; done;

find ${component_path} -type f -iname "$component_name*js" -exec sed -i.bak "s/Component/$component_name/g" {} \;
find ${component_path} -type f -iname "$component_name*js" -exec sed -i.bak "s/component/$component_name_lower/g" {} \;
find ${component_path} -type f -iname "$component_name*md" -exec sed -i.bak "s/Component/$component_name/g" {} \;
find ${component_path} -type f -iname "$component_name*scss" -exec sed -i.bak "s/component/$component_name_lower/g" {} \;
rm ${component_path}/*.bak
echo "export { ${component_name} } from './${component_name}/${component_name}';" >> ${components_path}/index.js
echo "@import './${component_name}/${component_name}.scss';" >> ${components_path}/index.scss
