#!/usr/bin/env node

// special node script to concatenate mixins from dimsum
// the concatenated mixins are exported from dimsum so that
// consuming application can still use all the mixins

const path = require('path');
const fs = require('fs');

// ok I admit, maintaining this list manually might not be best idea
const files = [
  'src/styles/base/color.scss',
  'src/styles/base/typography.scss',
  'src/styles/base/spacing.scss',
  'src/styles/base/motion.scss',
  'src/styles/base/depth.scss',
  'src/styles/base/iconography.scss',
  'src/styles/base/functions/color-functions.scss',
  'src/styles/base/functions/typography-functions.scss',
  'src/styles/base/functions/spacing-functions.scss',
  'src/styles/base/functions/motion-functions.scss',
  'src/styles/base/functions/depth-functions.scss',
  'src/styles/base/mixins/color-mixins.scss',
  'src/styles/base/mixins/typography-mixins.scss',
  'src/styles/base/mixins/spacing-mixins.scss',
  'src/styles/base/mixins/motion-mixins.scss',
  'src/styles/base/mixins/depth-mixins.scss',
  'src/styles/base/mixins/input-mixins.scss',
  'src/styles/base/mixins/position-mixin.scss'
];

let data = '\n';

files.forEach(file => {
  data += fs.readFileSync(path.join(__dirname, file), 'utf-8') + '\n';
});

// create dist directory...
try {
  fs.mkdirSync(path.join(__dirname, 'dist'));
} catch (e) {}

fs.writeFileSync(path.join(__dirname, 'dist', 'dimsum.mixins.scss'), data, 'utf-8');
