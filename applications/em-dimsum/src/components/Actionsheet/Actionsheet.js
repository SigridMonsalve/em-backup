import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from '../Modal/Modal';

const Actionsheet = ({ title, onClose, customClasses, ...props }) => {
  return (
    <Modal
      closeTimeoutMS={250} // Needed to smooth transition on closing actionsheet.
      portalClassName={`em-ds-actionsheet ${customClasses}`}
      title={title}
      closeable={false}
      onClose={onClose}
      {...props}
    >
      {props.children}
    </Modal>
  );
};

Actionsheet.propTypes = {
  title: PropTypes.string,
  onClose: PropTypes.func
};
Actionsheet.defaultProps = {
  customClasses: ''
};

export { Actionsheet };
