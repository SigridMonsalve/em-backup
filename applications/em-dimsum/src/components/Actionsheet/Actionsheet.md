**Usage**
````
<Actionsheet
    isOpen={this.state.actionsheetIsOpen}
    onAfterOpen={this.afterOpenActionsheet}
    onRequestClose={this.closeActionsheet}
    contentLabel="Example Actionsheet"
    >

    <div style={contentStyle}>
    <h2 ref={subtitle => (this.subtitle = subtitle)}>Actionsheet</h2>
    <div>This is an actionsheet</div>
    </div>
</Actionsheet>
````

**isOpen** - pass initial state - true for open / false for closed state.

**onAfterOpen** - Perform any actions after actionsheet is opened. 

**contentLabel** - This will be the Label read out when voice over enabled.