import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Actionsheet } from './Actionsheet';
import docs from './Actionsheet.md';
import ExampleActionsheet from './ExampleActionsheet';

const stories = storiesOf('Components/Actionsheet', module).addDecorator(withReadme(docs));

//  Refer ExampleActionsheet as a example for app level implementation.
stories.add('default', _ => <ExampleActionsheet />);
