import React from 'react';
import { shallow } from 'enzyme';
import { Actionsheet } from './Actionsheet';

describe('Actionsheet', () => {
  it('renders without crashing', () => {
    const closeCallback = () => {};
    shallow(<Actionsheet onClose={closeCallback} />);
  });
});
