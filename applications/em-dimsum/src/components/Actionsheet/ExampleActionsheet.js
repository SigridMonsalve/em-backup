/* 
This is a sample implementation of actionsheet.
*/
import React, { Component } from 'react';
import { Actionsheet } from './Actionsheet';

class ExampleActionsheet extends Component {
  constructor() {
    super();

    this.state = {
      actionsheetIsOpen: false
    };

    this.openActionsheet = this.openActionsheet.bind(this);
    this.afterOpenActionsheet = this.afterOpenActionsheet.bind(this);
    this.closeActionsheet = this.closeActionsheet.bind(this);
  }

  openActionsheet() {
    this.setState({ actionsheetIsOpen: true });
  }

  afterOpenActionsheet() {
    // Any action after opening action sheet. References are now sync'd and can be accessed.
    this.subtitle.style.color = ' #1a8af3';
    // this.content.style.filter =  'blur(1px)';
  }

  closeActionsheet() {
    this.setState({ actionsheetIsOpen: false });
    // this.content.style.filter =  '';
  }

  render() {
    const buttonStyle = {
      background: `border-box`,
      padding: `8px`
    };

    const contentStyle = {
      padding: `16px`
    };
    return (
      <div ref={content => (this.content = content)} style={contentStyle}>
        <button style={buttonStyle} onClick={this.openActionsheet}>
          Open Actionsheet
        </button>

        <Actionsheet
          isOpen={this.state.actionsheetIsOpen}
          onAfterOpen={this.afterOpenActionsheet}
          onClose={this.closeActionsheet}
          contentLabel="Example Actionsheet"
        >
          <div style={contentStyle}>
            <h2 ref={subtitle => (this.subtitle = subtitle)}>Actionsheet</h2>
            <div>This is an actionsheet</div>
          </div>
        </Actionsheet>
      </div>
    );
  }
}

export default ExampleActionsheet;
