import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';

const Button = ({ children, primary, secondary, text, fluid, icon, fab, ...props }) => {
  const classes = objstr({
    'em-ds-button': true,
    'em-ds-button-primary': primary,
    'em-ds-button-secondary': secondary,
    'em-ds-button-text': text,
    'em-ds-button-icon': icon || fab,
    'em-ds-button--fluid': fluid,
    'em-ds-button-fab': fab,
    'em-ds-button-fab--left': fab && fab === 'left',
    'em-ds-button-fab--top': fab && fab === 'top'
  });
  return (
    <button className={classes} {...props}>
      {children}
    </button>
  );
};

Button.propTypes = {
  disabled: PropTypes.bool
};
Button.defaultProps = {
  disabled: false
};

export { Button };
