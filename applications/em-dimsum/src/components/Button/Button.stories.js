import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Button } from './Button';
import docs from './Button.md';
import { ICONS, Icon } from '../Icon/Icon';

const stories = storiesOf('Components/Button', module).addDecorator(withReadme(docs));

stories.add('Primary', _ => <Button primary>Primary</Button>);
stories.add('Secondary', _ => <Button secondary>Secondary</Button>);
stories.add('Text', _ => <Button text>Text</Button>);
stories.add('Icon', _ => (
  <Button primary icon>
    <Icon icon={ICONS.SEARCH} width={16} height={16} />
  </Button>
));
stories.add('Primary - Disabled', _ => (
  <Button primary disabled>
    Primary
  </Button>
));
stories.add('Secondary - Disabled', _ => (
  <Button secondary disabled>
    Secondary
  </Button>
));
stories.add('Text - Disabled', _ => (
  <Button text disabled>
    Text
  </Button>
));
stories.add('Icon - Disabled', _ => (
  <Button icon text disabled>
    <Icon icon={ICONS.SEARCH} width={16} height={16} />
  </Button>
));
stories.add('Fab', _ => (
  <Button fab primary>
    <Icon icon={ICONS.ADD} width={16} height={16} />
  </Button>
));
stories.add('Fab left', _ => (
  <Button fab="left" primary>
    <Icon icon={ICONS.ADD} width={16} height={16} />
  </Button>
));
stories.add('Fab top', _ => (
  <Button fab="top" primary>
    <Icon icon={ICONS.ADD} width={16} height={16} />
  </Button>
));
