import PropTypes from 'prop-types';
import React from 'react';
import { Carousel } from '../Carousel/Carousel';
import { ButtonGroup } from '../ButtonGroup/ButtonGroup';

const ButtonCarousel = ({ buttons, columns, rows, showNav, onClick }) => {
  const slidesCount = Math.ceil(buttons.length / columns / rows);

  const createRowData = (buttons, columns) => {
    const results = [];
    // Creating a clone of the props.button to use for splice operation.
    const buttonGroup = [...buttons];

    while (buttonGroup.length) {
      results.push(buttonGroup.splice(0, columns));
    }
    return results;
  };

  const rowData = createRowData(buttons, columns);

  const createRow = (column, key) => {
    return (
      <div className="em-ds-carousel-row" key={key}>
        {column.map((button, i) => {
          return (
            <div className="em-ds-carousel-column" key={i}>
              <input
                type="checkbox"
                defaultValue={button.value}
                id={button.id}
                name={button.name}
                onClick={onClick}
                defaultChecked={button.defaultChecked}
              />
              <label htmlFor={button.id} aria-label={button.label}>
                {button.label}
              </label>
            </div>
          );
        })}
      </div>
    );
  };

  const createSlide = buttons => {
    const cols = [];

    for (let i = 0; i < rows; i++) {
      rowData.length && cols.push(createRow(rowData.shift(), i));
    }

    return cols;
  };

  const getSlides = buttons => {
    let slides = [];
    for (let i = 0; i < slidesCount; i++) {
      slides.push(
        <div className="em-ds-buttoncarousel-slide" key={i}>
          {createSlide(buttons)}
        </div>
      );
    }

    return slides;
  };

  return (
    <div className="em-ds-buttoncarousel">
      <Carousel loop={false} showNav={showNav} selected={0} showArrows={false} slideWidth={80}>
        {getSlides(buttons)}
      </Carousel>
    </div>
  );
};

ButtonCarousel.propTypes = {
  buttons: PropTypes.arrayOf(PropTypes.object),
  columns: function(columns, slidesCount) {
    if (columns < 2 || slidesCount < 2) {
      return new Error(
        `Cannot fit buttons properly with this row/column structure. Recommended to use 3x2 structure per slide`
      );
    }
  },
  rows: PropTypes.number
};

ButtonCarousel.defaultProps = {
  showNav: true,
  rows: 3,
  columns: 2
};

export { ButtonCarousel };
