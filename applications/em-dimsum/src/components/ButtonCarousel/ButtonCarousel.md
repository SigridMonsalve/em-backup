**Usage**

````
const buttons = [
  { name: 'button1', label: 'Button 1', id: 'button1', value: 'button1' },
  { name: 'button2', label: 'Button 2', id: 'button2', value: 'button2' },
  { name: 'button3', label: 'Button 3', id: 'button3', value: 'button3' },
  { name: 'button4', label: 'Button 4', id: 'button4', value: 'button4' },
  { name: 'button5', label: 'Button 5', id: 'button5', value: 'button5' },
  { name: 'button6', label: 'Button 6', id: 'button6', value: 'button6' },
  { name: 'button7', label: 'Button 7', id: 'button7', value: 'button7' },
  { name: 'button8', label: 'Button 8', id: 'button8', value: 'button8' },
  { name: 'button9', label: 'Button 9', id: 'button9', value: 'button9' },
  { name: 'button10', label: 'Button 10', id: 'button10', value: 'button10' }
];

<ButtonCarousel buttons={buttons} rows={3} columns={2} />;
````
**Supported Attributes**
- ````buttons```` - An array of objects. Provide name, id, value, label for buttons.
- ````rows```` - Number of rows to be displayed. Default 3.
- ````columns```` - Number of columns per slide. Default 2