import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ButtonGroup } from './ButtonGroup';
import docs from './ButtonGroup.md';

const stories = storiesOf('Components/ButtonGroup', module).addDecorator(withReadme(docs));

const buttons = [
  { name: 'button1', label: 'Button 1', id: 'button1', value: 'button1' },
  { name: 'button2', label: 'Button 2', id: 'button2', value: 'button2' },
  { name: 'button3', label: 'Button 3', id: 'button3', value: 'button3' },
  { name: 'button4', label: 'Button 4', id: 'button4', value: 'button4' }
];
stories.add('default', _ => <ButtonGroup buttons={buttons} rows={2} columns={2} />);
