import React from 'react';
import PropTypes from 'prop-types';

const Card = ({ children, ...props }) => {
  return <div className="em-ds-card">{children}</div>;
};

Card.propTypes = {
  children: PropTypes.node
};
export { Card };
