import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Card } from './Card';
import docs from './Card.md';

const stories = storiesOf('Components/Card', module).addDecorator(withReadme(docs));

stories.add('default', () => (
  <Card>
    <div>Some content could be here</div>
  </Card>
));
