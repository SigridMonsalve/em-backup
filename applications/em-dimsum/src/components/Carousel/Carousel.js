import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Carousel extends Component {
  constructor(props) {
    super(props);
    const { selected } = this.props;
    this.state = {
      swipeStart: 0,
      swipeStartTime: new Date(),
      index: selected ? selected : 0,
      lastIndex: selected ? selected : 0,
      transition: false
    };
  }

  handleSwipeStart(event) {
    this.setState({
      swipeStart: event.touches[0].pageX,
      swipeStartTime: new Date(),
      transition: false,
      slideWidth: ReactDOM.findDOMNode(this.refs.carousel).offsetWidth
    });
  }

  handleSwipeMove(event) {
    const { swipeStart, lastIndex, slideWidth } = this.state;

    const offset = swipeStart - event.touches[0].pageX;
    const percentageOffset = offset / slideWidth;
    const newIndex = lastIndex + percentageOffset;

    this.setState({
      index: newIndex
    });
  }

  handleSwipeEnd() {
    const { children } = this.props;
    const { swipeStartTime, index, lastIndex } = this.state;

    const timeElapsed = new Date().getTime() - swipeStartTime.getTime();
    const offset = lastIndex - index;

    // Get speed of swipe action and decide which slide to land.
    const swipeSpeed = Math.round((offset / timeElapsed) * 10000);
    let newIndex = Math.round(index);
    if (Math.abs(swipeSpeed) > 5) {
      newIndex = swipeSpeed < 0 ? lastIndex + 1 : lastIndex - 1;
    }

    // Reset the index/active slide in case the new index goes out of bound.
    // This way slide will bounce back to first or last one.
    if (newIndex < 0) {
      newIndex = 0;
    } else if (newIndex >= children.length) {
      newIndex = children.length - 1;
    }

    this.setState({
      swipeStart: 0,
      index: newIndex,
      lastIndex: newIndex,
      transition: true
    });
  }

  goToSlide(index, event) {
    const { children, loop } = this.props;

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (index < 0) {
      index = loop ? children.length - 1 : 0;
    } else if (index >= children.length) {
      index = loop ? 0 : children.length - 1;
    }

    this.setState({
      index: index,
      lastIndex: index,
      transition: true
    });
  }

  renderNav() {
    const { children } = this.props;
    const { lastIndex } = this.state;

    const nav = React.Children.map(children, (slide, i) => {
      const buttonClasses = objstr({
        'em-ds-carousel-nav-button': true,
        'em-ds-carousel-nav-button--active': i === lastIndex
      });

      return <button className={buttonClasses} key={i} onClick={event => this.goToSlide(i, event)} />;
    });

    return <div className="em-ds-carousel-nav">{nav}</div>;
  }

  renderArrows() {
    const { children, loop } = this.props;
    const { lastIndex } = this.state;
    const arrowContainerClasses = 'em-ds-carousel-arrow-container';

    return (
      <div className={arrowContainerClasses}>
        {loop || lastIndex > 0 ? (
          <button
            className="em-ds-carousel-arrow em-ds-carousel-arrow--left"
            onClick={event => this.goToSlide(lastIndex - 1, event)}
          />
        ) : null}
        {loop || lastIndex < children.length - 1 ? (
          <button
            className="em-ds-carousel-arrow em-ds-carousel-arrow--right"
            onClick={event => this.goToSlide(lastIndex + 1, event)}
          />
        ) : null}
      </div>
    );
  }

  render() {
    const { children, showArrows, showNav, slideWidth } = this.props;
    const { index, transition } = this.state;
    const slidesStyles = {
      transform:
        index === children.length - 1 && slideWidth !== 100
          ? `translateX(${-((100 - slideWidth) * index) * (children.length / (children.length - 1))}%)`
          : `translateX(${-1 * index * (100 / children.length)}%)`,
      width: `${slideWidth * children.length}%`
    };

    const slidesClasses = objstr({
      'em-ds-carousel-slides': true,
      'em-ds-carousel-slides--transition': transition
    });

    return (
      <div className="em-ds-carousel-wrapper" ref="carousel">
        {showArrows ? this.renderArrows() : null}
        {showNav ? this.renderNav() : null}

        <div
          className="em-ds-carousel"
          onTouchStart={event => this.handleSwipeStart(event)}
          onTouchMove={event => this.handleSwipeMove(event)}
          onTouchEnd={() => this.handleSwipeEnd()}
        >
          <div className={slidesClasses} style={slidesStyles}>
            {React.Children.map(children, (child, idx) => {
              if (idx === index) {
                return React.cloneElement(child, {
                  className: child.props.className + ' em-ds-carousel-slide--active'
                });
              }
              return child;
            })}
          </div>
        </div>
      </div>
    );
  }
}

Carousel.propTypes = {
  selected: PropTypes.number,
  loop: PropTypes.bool,
  showArrows: PropTypes.bool,
  showNav: PropTypes.bool,
  slideWidth: PropTypes.number
};

Carousel.defaultProps = {
  loop: false,
  selected: 0,
  showArrows: true,
  showNav: true,
  slideWidth: 100
};

export { Carousel };
