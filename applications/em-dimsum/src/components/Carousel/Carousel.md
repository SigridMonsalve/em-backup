**Usage**
````
<Carousel loop={false} showNav={true} selected={3} showArrows={true}>
    <div style={style}>Item 0</div>
    <div style={style}>Item 1</div>
    <div style={style}>Item 2</div>
    <div style={style}>Item 3</div>
    <div style={style}>Item 4</div>
</Carousel>
````

**Supported Attributes**
````
loop - boolean
showNav - boolean
selected - number 
showArrows - boolean
slideWidth - number (px)
````