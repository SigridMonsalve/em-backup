import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Carousel } from './Carousel';
import docs from './Carousel.md';

const stories = storiesOf('Components/Carousel', module).addDecorator(withReadme(docs));

stories.add('default', _ => {
  const style = {
    padding: '100px 0',
    fontSize: '2em',
    color: '#fff',
    marginRight: '1px',
    textAlign: 'center',
    background: '#006AA9'
  };

  return (
    <div className="carousel-sample">
      <h4>Slider with custom options</h4>
      <Carousel loop={false} showNav={true} selected={3} showArrows={true}>
        <div style={style}>Item 0</div>
        <div style={style}>Item 1</div>
        <div style={style}>Item 2</div>
        <div style={style}>Item 3</div>
        <div style={style}>Item 4</div>
      </Carousel>
    </div>
  );
});
