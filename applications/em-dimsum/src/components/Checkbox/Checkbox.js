import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';

const Checkbox = ({ shape, disabled, id, ...props }) => {
  const classes = objstr({
    'em-ds-checkbox': true,
    'em-ds-checkbox--oval': shape === 'oval',
    'em-ds-checkbox--disabled': disabled === 'disabled'
  });

  return <input type="checkbox" id={id} disabled={disabled} className={classes} {...props} onChange={props.onChange} />;
};

Checkbox.propTypes = {
  disabled: PropTypes.string,
  shape: PropTypes.string
};
Checkbox.defaultProps = {
  shape: ''
};

export { Checkbox };
