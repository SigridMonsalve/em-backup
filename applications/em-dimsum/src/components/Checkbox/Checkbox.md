Add Documentation for Checkbox

### Props
| Props        | Options           | Default  | Description                   |
| ------------ |:-----------------:|:--------:| ----------------------------- |
| disabled     | string            | -        |                               |
| id           | -                 | -        |                               |
| shape        | string            | ' '      |                               |
**Other than this it accepts all the props which can be given to an input[type='checkbox'] tag.**