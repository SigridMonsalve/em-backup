import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Checkbox } from './Checkbox';
import docs from './Checkbox.md';

const stories = storiesOf('Components/Checkbox', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Checkbox id="checkbox" />);
