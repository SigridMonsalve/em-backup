import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import { Consumer } from '../InputGroupContext/InputGroupContext';
import { Label } from '../Label/Label';

const CheckboxOption = ({ id, label, ...props }) => {
  const classes = objstr({
    'em-ds-checkboxoption': true
  });
  return (
    <Consumer>
      {value => {
        return (
          <React.Fragment>
            <input type="checkbox" id={id} name={value.name} onChange={value.onChange} className={classes} {...props} />
            <Label htmlFor={id}>{label}</Label>
          </React.Fragment>
        );
      }}
    </Consumer>
  );
};

CheckboxOption.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string.isRequired
};

export { CheckboxOption };
