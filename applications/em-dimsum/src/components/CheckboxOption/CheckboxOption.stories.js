import { storiesOf } from '@storybook/react';
import { withReadme } from 'storybook-readme';
import docs from './CheckboxOption.md';

const stories = storiesOf('CheckboxOption', module).addDecorator(withReadme(docs));

// stories.add('default', _ => <CheckboxOption>Hello</CheckboxOption>);

export { stories };
