import React from 'react';
import { Modal } from '../Modal/Modal';
import { Button } from '../Button/Button';

const ConfirmationModal = ({ type, closeButton, actionButton, children, title, isOpen, onClose, ...props }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} title={title} type={type} {...props}>
      <div className="em-ds-conmodal--content">{children}</div>
      <div>
        <div className="em-ds-modal--container" onClick={closeButton['action']}>
          <Button secondary>{closeButton['text']}</Button>
        </div>
        <div className="em-ds-modal--container" onClick={actionButton['action']}>
          <Button primary>{actionButton['text']}</Button>
        </div>
      </div>
    </Modal>
  );
};

ConfirmationModal.defaultProps = {};

export { ConfirmationModal };
