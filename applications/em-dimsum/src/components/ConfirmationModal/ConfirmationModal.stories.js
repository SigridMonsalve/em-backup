import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ConfirmationModal } from './ConfirmationModal';
import docs from './ConfirmationModal.md';

const stories = storiesOf('Components/ConfirmationModal', module).addDecorator(withReadme(docs));

stories.add('default', () => (
  <ConfirmationModal
    type="confirm"
    title="Title"
    isOpen={true}
    onClose={() => {
      return false;
    }}
    closeButton={{ text: 'Discard', action: action('closed') }}
    actionButton={{ text: 'save', action: action('closed') }}
  >
    Are you sure to switch to pipeline to create a new loan using this scenaron data?
  </ConfirmationModal>
));
