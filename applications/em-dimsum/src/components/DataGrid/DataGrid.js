import objstr from 'obj-str';
import React, { Component } from 'react';
import Immutable from 'immutable';
import PropTypes from 'prop-types';
import { Column, Table, AutoSizer } from 'react-virtualized';
import { ICONS, Icon } from '../Icon/Icon';

class DataGrid extends Component {
  static classes = objstr({
    'em-data-grid': true
  });
  // static contextTypes = {
  //   list: PropTypes.instanceOf(Immutable.List).isRequired,
  // };

  constructor(props) {
    super(props);
    this.state = {
      primaryColumns: [], // primaryColumns - These columns will display in semi bold (header & data)
      list: '', // list - data to be displayed in the grid
      columns: [], // columns - List of columns to be rendered
      headers: [], // headers - Columns headers for the grid
      sortBy: '',
      sortDirection: ''
    };
  }
  componentDidMount() {
    this.setState({
      primaryColumns: this.props && this.props.primaryColumns ? this.props.primaryColumns : [],
      headers: this.props.headers ? Object.keys(this.props.headers) : [],
      list: this.props && this.props.list ? Immutable.List(this.props.list) : '',
      columns: Object.keys(this.props.list[0])
    });
    if (this.props.moreActionsIcon) {
      this.setState(prev => ({
        columns: prev.columns.push('info'),
        list: prev.list.map(item => {
          item.info = '';
          return item;
        }),
        headers: [...prev.headers, 'actionIcon']
      }));
    }
  }
  _sort = ({ sortBy, sortDirection }) => {
    this.props.onSort(sortBy, sortDirection);
    this.setState({ sortBy, sortDirection });
  };
  renderCustomCell = cellData => {
    if (cellData.dataKey === 'actionIcon') {
      return (
        <Icon
          icon={ICONS.MORE_OPTIONS_VERT}
          width={16}
          height={16}
          onClick={event => {
            console.log(event);
            this.props.moreActionsFunc(event, cellData);
          }}
        />
      );
    }
    return cellData.cellData;
  };
  getRow = arg => {
    return this.state.list.get(arg.index);
  };

  render() {
    const { headers, list, columns, primaryColumns, sortBy, sortDirection } = this.state;
    return (
      <div className={DataGrid.classes}>
        <AutoSizer>
          {({ height, width }) => (
            <Table
              className="data-table"
              width={width}
              height={height}
              headerHeight={48}
              rowHeight={48}
              rowCount={list.length || 10}
              rowGetter={this.getRow}
              onHeaderClick={this.handleClick}
              sortBy={sortBy}
              sortDirection={sortDirection}
              sort={this._sort}
            >
              {headers.map((item, index) => (
                <Column
                  key={index}
                  label={item === 'actionIcon' ? '' : this.props.headers[item]}
                  cellRenderer={this.renderCustomCell}
                  dataKey={item}
                  width={150}
                  className={primaryColumns.indexOf(item) > -1 ? 'primary' : ''}
                />
              ))}
            </Table>
          )}
        </AutoSizer>
      </div>
    );
  }
}

DataGrid.propTypes = {
  list: PropTypes.array,
  headers: PropTypes.object,
  primaryColumns: PropTypes.array,
  moreActionsIcon: PropTypes.bool,
  onSort: PropTypes.func
};
DataGrid.defaultProps = {};

export { DataGrid };
