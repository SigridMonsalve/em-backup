# DataGrid 
This project uses react-virtualized and currently supports the `<Table>` functionality. 
https://github.com/bvaughn/react-virtualized/blob/master/docs/Table.md

## How to use
```
<DataGrid list={arrayOfObjects} headers={simpleObject} primaryColumns={arrayOfStrings} moreActionsIcon={boolean}>
```

```
list 
Type: Array of objects

headers
Type: A flat object with key/value pairs
key: Must be same as the key for data to be rendered from the "list" 
value: Header title in grid

primaryColumns 
Type: Array of strings
Use: Strings that match the column headers will display those columns in semi bold

moreActionsIcon: Shows the vertical ... icon when set to true. 
Type: Boolean

moreActionsFunc: Function to be executed when icon is clicked on
Type: Function
```
