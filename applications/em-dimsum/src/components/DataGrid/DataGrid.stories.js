import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { DataGrid } from './DataGrid';
import docs from './DataGrid.md';

const stories = storiesOf('Components/DataGrid', module).addDecorator(withReadme(docs));
const list = [
  { name: 'Brian Vaughn', description: 'Aoftware engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Arian Vaughn', description: 'Boftware engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Drian Vaughn', description: 'Coftware engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Crian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' },
  { name: 'Brian Vaughn', description: 'Software engineer', extraProp: 'no column header here' }
];
const headerList = { name: 'Borrower Name', description: 'Job description' };
const primaryColumns = ['name'];
const moreActionsIcon = true;
const moreActionsFunc = (event, celldata) => alert('Function passed as prop executed');

stories.add('default', _ => (
  <DataGrid
    list={list}
    headers={headerList}
    primaryColumns={primaryColumns}
    moreActionsIcon={moreActionsIcon}
    moreActionsFunc={moreActionsFunc}
  />
));
