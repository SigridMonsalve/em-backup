import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ICONS, Icon } from '../Icon/Icon';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';

class DropdownComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: props.selectedIndex,
      activeIndex: null,
      isOpen: false
    };
    this.dropdownTrigger = React.createRef();
  }

  handleClickOutside(event) {
    this.hideDropdown();
  }

  handleClick(e, selectedIndex) {
    e.preventDefault();
    this.optionSelected(selectedIndex);
    this.props.onClick && this.props.onClick();
  }

  handleKeyDown(e, index) {
    const keyCode = e.keyCode;
    const isShift = e.shiftKey;
    const listLength = this.props.options.length || 0;
    let hideDropdown;
    let setFocusToTrigger;
    switch (keyCode) {
      case 9:
        if ((!isShift && index === listLength - 1) || (isShift && index === 0)) {
          hideDropdown = true;
        }
        break;
      case 27:
        hideDropdown = true;
        setFocusToTrigger = true;
        break;
    }

    hideDropdown && this.hideDropdown(setFocusToTrigger);
    this.props.onKeyDown && this.props.onKeyDown(index);
  }

  handleFocus(event, index) {
    this.setState({
      activeIndex: index
    });
    this.props.onFocus && this.props.onFocus(index);
  }

  handleBlur(event, index) {
    this.setState({
      activeIndex: null
    });
    this.props.onBlur && this.props.onBlur(index);
  }

  optionSelected(selectedIndex) {
    this.setState({
      selectedIndex: selectedIndex,
      isOpen: false
    });
    this.props.onSelect && this.props.onSelect(this.props.options[selectedIndex]);
    this.dropdownTrigger.current.focus();
  }

  setDropdownHeaderValue() {
    const selectedIndex = this.state.selectedIndex;
    const props = this.props;
    return typeof selectedIndex === 'number' ? props.options[selectedIndex][props.displayKey] : this.props.defaultValue;
  }

  hideDropdown(setFocusToTrigger) {
    this.setState({
      isOpen: false,
      activeIndex: null
    });

    setFocusToTrigger && this.dropdownTrigger.current.focus();
    this.props.onClose && this.props.onClose();
  }

  toggleDropdown(e) {
    const eventName = this.state.isOpen ? 'onClose' : 'onOpen';
    e.preventDefault();
    this.setState({
      activeIndex: null,
      isOpen: !this.state.isOpen
    });

    this.props[eventName] && this.props[eventName]();
  }

  setItemClassName(index) {
    return this.state.selectedIndex === index ? 'active' : '';
  }

  renderOptions(options) {
    const items = Array.isArray(options) ? options : [];

    return items.map((el, index) => {
      return (
        <li key={index} className={this.state.activeIndex === index ? 'active' : ''}>
          <a
            href="#"
            onClick={event => this.handleClick(event, index)}
            onFocus={event => this.handleFocus(event, index)}
            onBlur={event => this.handleBlur(event, index)}
            onKeyDown={event => this.handleKeyDown(event, index)}
          >
            {el[this.props.displayKey]}
          </a>
        </li>
      );
    });
  }

  renderList(options) {
    const list = <ul>{this.renderOptions(options)}</ul>;
    return this.state.isOpen ? list : '';
  }

  render() {
    return (
      <div className="em-ds-dropdown">
        <div className="dropdown-header">
          <span>{this.setDropdownHeaderValue()}</span>
          <a href="#" onClick={event => this.toggleDropdown(event)} ref={this.dropdownTrigger}>
            <Icon icon={ICONS.CHEVRON_DOWN} />
          </a>
        </div>
        {this.renderList(this.props.options)}
      </div>
    );
  }
}

DropdownComponent.propTypes = {
  defaultValue: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object),
  displayKey: PropTypes.string,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  onSelect: PropTypes.func
};

DropdownComponent.defaultProps = {
  defaultValue: 'Select an option',
  displayKey: 'name'
};

const Dropdown = onClickOutside(DropdownComponent);

export { Dropdown };
