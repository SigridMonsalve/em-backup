import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Dropdown } from './Dropdown';
import docs from './Dropdown.md';

const stories = storiesOf('Components/Dropdown', module).addDecorator(withReadme(docs));

const options = [
  { name: 'Name1', value: 'value1' },
  { name: 'Name2', value: 'value2' },
  { name: 'Name3', value: 'value3' },
  { name: 'Name4', value: 'value4' }
];

stories.add('default', _ => <Dropdown options={options} defaultValue="Select an option" />);
