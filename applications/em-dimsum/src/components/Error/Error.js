import objstr from 'obj-str';
import React from 'react';

const Error = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-error': true
  });
  return (
    <div className={classes} {...props}>
      {children}
    </div>
  );
};

Error.propTypes = {};
Error.defaultProps = {};

export { Error };
