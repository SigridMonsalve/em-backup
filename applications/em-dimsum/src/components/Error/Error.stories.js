import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Error } from './Error';
import docs from './Error.md';

const stories = storiesOf('Components/Error', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Error>Hello</Error>);
