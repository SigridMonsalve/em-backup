import objstr from 'obj-str';
import React from 'react';

/* 
Scaffolding component for those who cannot run create.sh script (Windows users).
*/
const ExampleComponent = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-examplecomponent': true
  });
  return (
    <div className={classes} {...props}>
      {children}
    </div>
  );
};

ExampleComponent.propTypes = {};
ExampleComponent.defaultProps = {};

export { ExampleComponent };
