import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ExampleComponent } from './ExampleComponent';
import docs from './ExampleComponent.md';

const stories = storiesOf('Components/ExampleComponent', module).addDecorator(withReadme(docs));

stories.add('default', _ => <ExampleComponent>Hello</ExampleComponent>);
