import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import { Checkbox } from '../Checkbox/Checkbox';
import { RadioOption } from '../RadioOption/RadioOption';
import { Error } from '../Error/Error';
import { Input } from '../Input/Input';
import { Label } from '../Label/Label';
import { Select } from '../Select/Select';

const Field = ({ error, id, name, invalid, label, size, type, ...props }) => {
  const classes = objstr({
    'em-ds-field': true,
    'em-ds-field-small': size === 'small',
    'em-ds-field-text': type === 'text',
    'em-ds-field-select': type === 'select'
  });

  const inputProps = {
    id,
    name,
    type,
    invalid,
    'aria-describedby': `${name}Error`,
    ...props
  };

  const renderInputFirst = type === 'checkbox' || type === 'radio';
  const components = {
    text: Input,
    checkbox: Checkbox,
    radio: RadioOption,
    select: Select
  };
  const CustomComponent = components[type];

  return (
    <div className={classes}>
      {renderInputFirst && <CustomComponent {...inputProps} />}
      {/* TODO: Needs to be replaced with a Label component*/}
      {label && (
        <Label htmlFor={inputProps.id} aria-label={label}>
          {label}
        </Label>
      )}
      {renderInputFirst || <CustomComponent {...inputProps} />}
      {invalid &&
        error && (
          <Error id={`${name}Error`} role="alert" palette="danger">
            {error}
          </Error>
        )}
    </div>
  );
};

Field.propTypes = {
  invalid: PropTypes.bool,
  type: PropTypes.string,
  id: PropTypes.string.isRequired || PropTypes.number.isRequired
};

Field.defaultProps = {
  type: 'text'
};

export { Field };
