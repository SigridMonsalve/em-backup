Add Documentation for Field

### Props
| Props        | Options           | Default  | Description |
| ------------ |:-----------------:|:--------:| ----------------------------- |
| id           | -                 | -        |                               |
| invalid      | boolean           | false    |                               |
| label        | string            | -        |                               |
| name         | -                 | -        |                               |
| type         | string            | 'text'   | type of field you wish to use |

**Other than this it accepts all the props which can be given to a based on type you selected.**