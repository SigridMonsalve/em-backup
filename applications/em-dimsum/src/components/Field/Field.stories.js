import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Field } from './Field';
import docs from './Field.md';

let stories = storiesOf('Components/Field/Checkbox', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Field type="checkbox" label="Checkbox" name="chkbox-default" />);
stories.add('oval', _ => <Field type="checkbox" label="Checkbox oval" name="chkbox-oval" shape="oval" />);
stories.add('disabled', _ => (
  <Field type="checkbox" label="Checkbox disabled" name="chkbox-default-disabled" disabled="disabled" />
));
stories.add('disabled checked', _ => (
  <Field
    type="checkbox"
    label="Checkbox disabled checked"
    name="chkbox-default-disabled"
    disabled="disabled"
    checked="checked"
  />
));

stories = storiesOf('Components/Field/Input', module).addDecorator(withReadme(docs));
stories.add('Input', _ => <Field type="text" label="Label" name="input-default" placeholder="Placeholder..." />);

stories = storiesOf('Components/Field/Select', module).addDecorator(withReadme(docs));
stories.add('Select', _ => <Field type="select" label="Label" name="input-default" placeholder="Placeholder..." />);
