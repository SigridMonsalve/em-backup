import objstr from 'obj-str';
import React from 'react';

const Footer = ({ children, customClasses, ...props }) => {
  const classes = objstr({
    'em-ds-footer': true
  });

  return (
    <div className={`${classes} ${customClasses}`} {...props}>
      {children}
    </div>
  );
};

Footer.propTypes = {};
Footer.defaultProps = {
  customClasses: ''
};

export { Footer };
