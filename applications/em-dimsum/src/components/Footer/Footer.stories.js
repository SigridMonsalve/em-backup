import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Footer } from './Footer';
import docs from './Footer.md';

const stories = storiesOf('Components/Footer', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Footer>Hello</Footer>);
