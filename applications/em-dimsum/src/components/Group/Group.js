import PropTypes from 'prop-types';
import React from 'react';
import { Error } from '../Error/Error';
import { Provider } from '../InputGroupContext/InputGroupContext';

const Group = ({ error, children, name, onClick, onChange, ...props }) => {
  return (
    <div className="em-ds-group">
      <Provider value={{ name, onChange, onClick }} {...props}>
        {children}
      </Provider>
      {error && (
        <Error id={`${name}Error`} role="alert" palette="danger">
          {error}
        </Error>
      )}
    </div>
  );
};

Group.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  children: PropTypes.node.isRequired,
  error: PropTypes.oneOfType([PropTypes.node, PropTypes.string])
};

Group.defaultProps = {};

export { Group };
