**Usage:**

This way of gropuping helps in adding additional styles and html tags in between or to contain radio/checkbox options. 

**Radio group example.**
````
<Group name="countries" onChange={onRadioChange}>
    <RadioOption label="USA" value="USA" />
    <RadioOption label="Germany" value="Germany" />
    <RadioOption label="Australia" value="Australia" />
</Group>
````

**Checkbox group example.**
````
<Group name="countries" onChange={onCheckboxChange}>
    <CheckboxOption label="USA" value="USA" />
    <CheckboxOption label="Germany" value="Germany" />
    <CheckboxOption label="Australia" value="Australia" />
</Group>
````

Multi select/Single select operations can be handled through the `onChange` handler in the application level.

**Internally uses React Context** - Context provides a way to share values between components without having to explicitly pass a prop through every level of the tree.
Read more about context - https://reactjs.org/docs/context.html




