import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Group } from './Group';
import docs from './Group.md';
import { RadioOption } from '../RadioOption/RadioOption';
import { CheckboxOption } from '../CheckboxOption/CheckboxOption';

let stories = storiesOf('Components/InputGroup/RadioGroup', module).addDecorator(withReadme(docs));

let onRadioChange = event => {
  console.log('Event: radio  selection changed.');
};
stories.add('Radio Group', _ => (
  <Group name="countries" onChange={onRadioChange}>
    <RadioOption label="USA" value="USA" id="USA" />
    <RadioOption label="Germany" value="Germany" id="Germany" />
    <RadioOption label="Australia" value="Australia" id="Australia" />
  </Group>
));

stories = storiesOf('Components/InputGroup/CheckboxGroup', module).addDecorator(withReadme(docs));

let onCheckboxChange = event => {
  console.log('Event: checkbox selection changed.');
};
stories.add('Checkbox Group', _ => (
  <Group name="countries" onChange={onCheckboxChange}>
    <CheckboxOption label="USA" value="USA" id="USA" />
    <CheckboxOption label="Germany" value="Germany" id="Germany" />
    <CheckboxOption label="Australia" value="Australia" id="Australia" />
  </Group>
));
