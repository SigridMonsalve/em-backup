import objstr from 'obj-str';
import React from 'react';
import PropTypes from 'prop-types';
import { ICONS, Icon } from '../Icon/Icon';

const ModalHeader = ({ children, customClasses, ...props }) => {
  const modalHeaderClasses = objstr({
    'em-ds-header': true
  });

  return (
    <header className={`${modalHeaderClasses} ${customClasses}`} {...props}>
      {children}
    </header>
  );
};

const Header = ({ children, onNavigation, onAction, customClasses, ...props }) => {
  const pageHeaderClasses = objstr({
    'em-ds-page-header': true
  });

  return (
    <header className={`${pageHeaderClasses} ${customClasses}`}>
      {onNavigation && (
        <div className="em-ds-header-navigation">
          <Icon icon={ICONS.CHEVRON_LEFT} width={17} height={17} onClick={onNavigation} />
        </div>
      )}

      <div className="em-ds-header-title">{children}</div>

      {/* TODO: Add in options for sort and search */}

      {onAction && (
        <div className="em-ds-header-moreactions">
          <Icon icon={ICONS.MORE_OPTIONS_VERT} width={17} height={17} onClick={onAction} />
        </div>
      )}
    </header>
  );
};

Header.propTypes = {
  onNavigation: PropTypes.func,
  onAction: PropTypes.func,
  customClasses: PropTypes.string
};
Header.defaultProps = {
  customClasses: ''
};

ModalHeader.propTypes = {
  customClasses: PropTypes.string
};

ModalHeader.defaultProps = {
  customClasses: ''
};

export { Header, ModalHeader };
