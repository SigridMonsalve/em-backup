import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ModalHeader, Header } from './Header';
import docs from './Header.md';

const stories = storiesOf('Components/Header', module).addDecorator(withReadme(docs));

stories.add('default', _ => <ModalHeader>Hello</ModalHeader>);

stories.add('Page Header', _ => <Header>Hello</Header>);
