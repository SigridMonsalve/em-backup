import objstr from 'obj-str';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon, ICONS } from '../Icon/Icon';
import { Panel } from '../Panel/Panel';
import { Modal } from '../Modal/Modal';
// import { About } from '../About/About';
import { Button } from '../Button/Button';

class HeaderLogo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panelIsOpen: false,
      aboutIsOpen: false
    };
  }

  openPanel = () => {
    this.setState({ panelIsOpen: true });
  };

  closePanel = () => {
    this.setState({ panelIsOpen: false });
  };

  onNavItemClick = event => {
    event.preventDefault();
    switch (event.target.id) {
      case 'about':
        this.setState({ aboutIsOpen: true });
        break;
      default:
        break;
    }
  };

  closeAboutModal = () => {
    this.setState({ aboutIsOpen: false });
  };

  renderFooter = () => (
    <Button primary onClick={this.closeAboutModal}>
      Ok
    </Button>
  );

  render() {
    const { bottomtext, sidebarAction, toptext, aboutContent } = this.props;
    const classes = objstr({
      'em-ds-headerlogo': true
    });

    if (bottomtext && !sidebarAction) {
      return (
        <div className={classes} onClick={sidebarAction}>
          <Icon
            icon={ICONS.ENC_LOGO_COLOR}
            width={28}
            height={28}
            className="header-logo"
            alt="logo"
            onClick={this.openPanel}
          />
          <div className="header-text">
            {toptext && <div className="header-text-top">{toptext}</div>}
            <div className={toptext ? 'header-text-bottom' : 'header-text-bottom-no-top'}>{bottomtext}</div>
          </div>
        </div>
      );
    }
    //panel action exists
    else {
      return (
        <div className={classes}>
          {/* <Panel Code /> */}
          <Panel title="Main Menu" isOpen={this.state.panelIsOpen} onClose={() => this.closePanel()}>
            <ul className="left-nav-items">
              <li>
                <a href="/pipeline">Pipeline</a>
              </li>
              <li>
                <a href="#" id="loan_scenario" onClick={event => this.onNavItemClick(event)}>
                  Loan Scenario
                </a>
              </li>
              <li>
                <a href="#" id="about" onClick={event => this.onNavItemClick(event)}>
                  About
                </a>
              </li>
            </ul>
          </Panel>

          <Icon
            icon={ICONS.ENC_LOGO_COLOR}
            width={28}
            height={28}
            className="header-logo"
            alt="logo"
            onClick={this.openPanel}
          />
          <div className="header-text">
            <div className="header-text-bottom-no-top">{bottomtext}</div>
          </div>
          {/* <AboutModal Code /> */}
          <Modal
            title="About Encompass"
            closeable
            onClose={this.closeAboutModal}
            isOpen={this.state.aboutIsOpen}
            hasFooter={false}
            headerLevel={2}
            closeButtonAlign="right"
            footerContent={this.renderFooter}
          >
            {/* <About resources={aboutContent.resources} info={aboutContent.info} /> */}
          </Modal>
        </div>
      );
    }
  }
}

HeaderLogo.propTypes = {
  bottomtext: PropTypes.string,
  aboutContent: PropTypes.object.isRequired
};
HeaderLogo.defaultProps = {
  bottomtext: 'Opportunities'
};

export { HeaderLogo };
