import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { HeaderLogo } from './HeaderLogo';
import docs from './HeaderLogo.md';
import { ICONS } from '../index';

const stories = storiesOf('Components/HeaderLogo', module).addDecorator(withReadme(docs));

stories.add('default', _ => <HeaderLogo>Hello</HeaderLogo>);
stories.add('top and bottom text', _ => (
  <HeaderLogo toptext="Loan Scenarios" bottomtext="Opportunities">
    Hello
  </HeaderLogo>
));

stories.add('header with sidebar functionality', _ => (
  <HeaderLogo toptext="Loan Scenarios" bottomtext="Opportunities" sidebarAction={true} />
));

stories.add('changeable icon flag', _ => (
  <HeaderLogo toptext="Loan Scenarios" bottomtext="Opportunities" iconType={ICONS.CHEVRON_LEFT} />
));
