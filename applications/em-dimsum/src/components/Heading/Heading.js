import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';

const Heading = ({ level, children, ...props }) => {
  const classes = objstr({
    'em-ds-heading': true,
    [`em-ds-heading--h${level}`]: true
  });
  const Tag = `h${level}`;
  return (
    <Tag className={classes} {...props}>
      {children}
    </Tag>
  );
};

Heading.propTypes = {
  level: PropTypes.number,
  children: PropTypes.node.isRequired
};
Heading.defaultProps = {
  level: 1
};

export { Heading };
