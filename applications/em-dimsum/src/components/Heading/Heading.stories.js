import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Heading } from './Heading';
import docs from './Heading.md';

const stories = storiesOf('Components/Heading', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Heading>Hello Heading</Heading>);

[1, 2, 3, 4, 5].forEach(l => {
  stories.add(`level ${l}`, _ => <Heading level={l}>Hello Heading</Heading>);
});
