import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import ICONS from './raw/selected.json';

const Icon = ({ width, height, icon, disabled, ...props }) => {
  const classes = objstr({
    'em-ds-icon': true,
    'em-ds-icon--disabled': disabled
  });

  return Array.isArray(icon.path) ? (
    <svg viewBox={icon.viewBox} className={classes} width={width} height={height} disabled={disabled} {...props}>
      {icon.path.map((p, idx) => <path d={p} key={idx} className="em-ds-icon-path" />)}
    </svg>
  ) : (
    <svg
      viewBox={icon.viewBox}
      className={classes}
      width={width}
      height={height}
      dangerouslySetInnerHTML={{ __html: icon.path }}
      {...props}
    />
  );
};

Icon.propTypes = {
  size: PropTypes.number,
  icon: PropTypes.any, // need better proptypes for icons
  disabled: PropTypes.bool
};

Icon.defaultProps = {
  width: 20,
  height: 20,
  disabled: false,
  icon: ICONS.LOGO_ENC_BUG
};

export { Icon, ICONS };
