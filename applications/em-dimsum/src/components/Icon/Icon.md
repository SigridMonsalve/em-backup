### Usage:

```
import { Icon, ICONS } from '@elliemae/dimsum';

// add an icon component with named icon
<Icon icon={ICONS.LOGO_ENC_BUG}/>

// specify optional size
<Icon icon={ICONS.LOGO_ENC_BUG} width={30} height={30}/>

// optionally mark icon as disabled
<Icon icon={ICONS.LOGO_ENC_BUG} disabled/>
```
