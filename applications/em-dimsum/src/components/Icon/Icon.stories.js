import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Icon, ICONS } from './Icon';
import docs from './Icon.md';

const stories = storiesOf('Components/Icon', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Icon />);
stories.add('named', _ => <Icon icon={ICONS.LOGO_ENC_BUG} />);
stories.add('disabled', _ => <Icon icon={ICONS.LOGO_ENC_BUG} disabled />);
stories.add('Encompass Color Logo', _ => <Icon icon={ICONS.ENC_LOGO_COLOR} width={28} height={28} />);
stories.add('EllieMae Name', _ => <Icon icon={ICONS.EM_NAME} width={160} height={36} />);
stories.add('No Opportunities', _ => <Icon icon={ICONS.NO_OPPORTUNITIES} width={343} height={125} />);

stories.add('showcase', _ => (
  <div
    style={{
      display: 'flex',
      flexWrap: 'wrap'
    }}
  >
    {Object.keys(ICONS)
      .sort()
      .map(icon => (
        <div
          key={icon}
          style={{
            flexGrow: 1,
            width: '50%',
            padding: '16px',
            display: 'flex',
            alignItems: 'center'
          }}
        >
          <Icon icon={ICONS[icon]} />
          <span style={{ paddingLeft: '24px' }}>{icon}</span>
        </div>
      ))}
  </div>
));
