To update icons

* obtain a new version of `DimSum-Icons-vXXX.zip`
* place the zip in raw folder
* Extract the zip using `unzip` command
* run `transform.js` using node, eg:
  ```
  node ./transform.js
  ```
