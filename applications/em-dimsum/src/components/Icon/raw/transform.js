#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const selection = require('./selection.json');

const files = fs.readdirSync('svgs/');
let svgFiles = [];
files.forEach(function(file) {
  if (path.extname(file) === '.svg' && !fs.statSync(path.join('svgs', file)).isDirectory()) {
    svgFiles.push(path.join('svgs', file));
  }
});

let out = {};

selection.icons.forEach(item => {
  let name = item.properties.name;
  let path = item.icon.paths;
  name = name.replace(/-/g, '_');
  name = name.toUpperCase();
  out[name] = {
    viewBox: '0 0 1024 1024',
    path: path
  };
});

svgFiles.forEach((svgFile, idx) => {
  let svg = fs.readFileSync(path.join(__dirname, svgFile), 'utf-8');
  svg = svg.replace(/\"/g, "'");
  svg = svg.replace(/[\n\t]/g, ' ');
  let name = svg.match(/(<title>)(.*)(<\/title>)/)[2];
  name = name.replace(/-/g, '_');
  name = name.toUpperCase();
  let viewBox = svg.substring(svg.indexOf('viewBox'));
  viewBox = viewBox.replace(/viewBox='/, '');
  viewBox = viewBox.substring(0, viewBox.indexOf("'"));

  let svgData = svg.substring(svg.indexOf('>'));
  svgData = svgData.substring(0, svgData.lastIndexOf('<'));

  out[name] = {
    viewBox: viewBox,
    path: svgData
  };
});

fs.writeFileSync('selected.json', JSON.stringify(out, null, '  '));
