import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import NumberFormat from 'react-number-format';

const Input = ({ formatType, extension, zipLength, disabled, ...props }) => {
  const classes = objstr({
    'em-ds-input-text': true,
    'em-ds-input-currency': formatType === 'currency',
    'em-ds-input-percent': formatType === 'percent',
    'em-ds-input-text--disabled': disabled === 'disabled'
  });

  const getInputMask = formatType => {
    let formatMask;

    switch (formatType) {
      case 'phone':
        if (extension) {
          props.placeholder = '___-___-____ ____';
          formatMask = '###-###-#### ####';
        } else {
          props.placeholder = '___-___-____';
          formatMask = '###-###-####';
        }
        break;
      case 'zipcode':
        if (zipLength === 9) {
          props.placeholder = '_____-____';
          formatMask = '#####-####';
        } else {
          formatMask = '#####';
        }
        break;
      case 'ssn':
        formatMask = '###-##-####';
        break;
      case 'creditscore':
        formatMask = '###';
        break;
    }

    return formatMask;
  };

  if (formatType && formatType !== '') {
    let spanLabel;
    if (formatType === 'currency') spanLabel = <span className="formatter em-prefix">$</span>;
    else if (formatType === 'percent') spanLabel = <span className="formatter em-suffix">%</span>;

    return (
      <React.Fragment>
        {formatType === 'currency' || formatType === 'percent' ? (
          <React.Fragment>
            <NumberFormat thousandSeparator={true} fixedDecimalScale={true} className={classes} {...props} />
            {spanLabel}
          </React.Fragment>
        ) : (
          <NumberFormat format={getInputMask(formatType)} className={classes} {...props} />
        )}
      </React.Fragment>
    );
  } else {
    return <input className={classes} {...props} />;
  }
};

Input.propTypes = {
  disabled: PropTypes.string,
  type: PropTypes.oneOf(['text', 'tel', 'number', 'password']),
  formatType: PropTypes.oneOf(['percent', 'currency', 'phone', 'zipcode', 'ssn', 'date', 'creditscore']),
  decimalScale: PropTypes.number,
  zipLength: PropTypes.number,
  extension: PropTypes.bool
};

Input.defaultProps = {
  type: 'text'
};

export { Input };
