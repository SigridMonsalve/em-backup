Add Documentation for Input

### Props
| Props        | Options           | Default  | Description                   |
| ------------ |:-----------------:|:--------:| ----------------------------- |
| onChange     | function          | -        |                               |
| onValueChange| function          | -        |                               |
| decimalScale | number            | -        |                               |
| disabled     | string            | -        |                               |
| extension    | boolean           | -        |                               |
| formatType   | string            | -        | 'percent', 'currency', 'phone', 'zipcode', 'ssn', 'date', 'credit' |
| placeholder  | string            | -        | input placeholder             |
| required     | -                 | -        |                               |
| type         | string            | 'text'   | 'text', 'tel', 'number', 'password' |
| zipLength    | number            | -        |                               |

**Other than this it accepts all the props which can be given to an input tag based on type you selected.**
