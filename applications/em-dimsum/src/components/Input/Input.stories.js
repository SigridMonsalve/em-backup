import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Input } from './Input';
import docs from './Input.md';

const stories = storiesOf('Components/Input', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Input type="text" />);
stories.add('disabled', _ => <Input type="text" disabled="disabled" />);
stories.add('percent with decimals', _ => <Input type="text" formatType="percent" decimalScale={3} />);
stories.add('percent without decimals', _ => <Input type="text" formatType="percent" decimalScale={0} />);
stories.add('currency with decimals', _ => <Input type="text" formatType="currency" decimalScale={2} />);
stories.add('currency without decimals', _ => <Input type="text" formatType="currency" decimalScale={0} />);
stories.add('password', _ => <Input type="password" />);
stories.add('number', _ => <Input type="number" />);
stories.add('ssn', _ => <Input type="text" formatType="ssn" />);
stories.add('phone', _ => <Input type="text" formatType="phone" />);
stories.add('phone with extension', _ => <Input type="text" formatType="phone" extension={true} />);
stories.add('zipcode', _ => <Input type="text" formatType="zipcode" />);
stories.add('zipcode with 9 digits', _ => <Input type="text" formatType="zipcode" zipLength={9} />);
