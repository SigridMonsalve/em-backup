import React from 'react';

const InputGroupContext = React.createContext();
const Provider = InputGroupContext.Provider;
const Consumer = InputGroupContext.Consumer;

export { Consumer, Provider };
