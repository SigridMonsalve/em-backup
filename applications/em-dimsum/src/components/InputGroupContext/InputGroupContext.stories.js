import { storiesOf } from '@storybook/react';
import { withReadme } from 'storybook-readme';
import docs from './InputGroupContext.md';

const stories = storiesOf('InputGroupContext', module).addDecorator(withReadme(docs));

export { stories };
