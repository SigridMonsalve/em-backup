import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import NumberFormat from 'react-number-format';

const Label = ({ children, disabled, size, ...props }) => {
  const classes = objstr({
    'em-ds-label': true,
    'em-ds-label-small': size === 'small',
    'em-ds-label--disabled': disabled
  });

  switch (props.formatType) {
    case 'currency':
      return (
        <NumberFormat
          className={classes}
          value={props.labelValue}
          displayType={'text'}
          thousandSeparator={true}
          prefix={'$'}
          fixedDecimalScale={true}
          {...props}
        />
      );
    case 'percent':
      return (
        <NumberFormat
          className={classes}
          value={props.labelValue}
          displayType={'text'}
          thousandSeparator={true}
          suffix={'%'}
          fixedDecimalScale={true}
          {...props}
        />
      );
    case 'number':
      return (
        <NumberFormat
          className={classes}
          value={props.labelValue}
          displayType={'text'}
          thousandSeparator={true}
          fixedDecimalScale={true}
          {...props}
        />
      );
    default:
      return (
        <label className={classes} {...props}>
          {children}
        </label>
      );
  }
};

Label.propTypes = {
  disabled: PropTypes.bool,
  formatType: PropTypes.oneOf(['percent', 'currency']),
  decimalScale: PropTypes.number
};

Label.defaultProps = {
  disabled: false
};

export { Label };
