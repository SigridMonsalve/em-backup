import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Label } from './Label';
import docs from './Label.md';

const stories = storiesOf('Components/Label', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Label>Hello</Label>);
stories.add('disabled', _ => <Label disabled>Hello</Label>);
stories.add('currencyLabel', _ => (
  <Label formatType="currency" decimalScale={2} labelValue={12345.66}>
    Hello
  </Label>
));
stories.add('percentLabel', _ => (
  <Label formatType="percent" decimalScale={3} labelValue={75.25}>
    Hello
  </Label>
));
