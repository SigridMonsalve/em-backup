import React from 'react';
import { shallow } from 'enzyme';
import { Label } from './Label';

describe('Label', () => {
  it('renders without crashing', () => {
    shallow(<Label />);
  });

  it('renders a label', () => {
    const wrapper = shallow(<Label>Hello</Label>);
    expect(wrapper.contains('Hello')).toEqual(true);
  });
});
