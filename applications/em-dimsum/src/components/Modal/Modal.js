import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import ReactModal from 'react-modal';
import { ICONS, Icon } from '../Icon/Icon';
import { ModalHeader } from '../Header/Header';
import { Button } from '../Button/Button';
import { Heading } from '../Heading/Heading';
import { Footer } from '../Footer/Footer';

document && document.getElementById('#app') && ReactModal.setAppElement && ReactModal.setAppElement('#root');

const Modal = ({
  type,
  children,
  title,
  closeable,
  onClose,
  customClasses,
  closeButtonAlign,
  hasFooter = false,
  footerContent,
  ...props
}) => {
  const hasHeader = title || closeable;
  const modalTitle = title || '';
  const classes = objstr({
    'em-ds-modal': true,
    'em-ds-conmodal': type === 'confirm'
  });
  const contentClasses = objstr({
    'em-ds-modal--content': true,
    'adjust-padding': hasFooter
  });
  return (
    <ReactModal
      className={classes}
      overlayClassName={`em-ds-modal--overlay ${customClasses}`}
      contentLabel={modalTitle}
      onRequestClose={onClose}
      hasHeader={hasHeader}
      {...props}
    >
      {hasHeader && (
        <ModalHeader customClasses={`${closeButtonAlign}`}>
          {closeable &&
            closeButtonAlign === 'left' && <Icon icon={ICONS.CLOSE} onClick={onClose} width={15} height={15} />}
          <div className="em-ds-modal-heading">
            <Heading level={props.headerLevel || 2}>{modalTitle}</Heading>
            {props.subHeader && <Heading level={props.subHeaderLevel || 2}>{props.subHeader}</Heading>}
          </div>
          {closeable &&
            closeButtonAlign !== 'left' && <Icon icon={ICONS.CLOSE} onClick={onClose} width={15} height={15} />}
        </ModalHeader>
      )}
      <div className={contentClasses}>{children}</div>
      {hasFooter && <Footer>{footerContent()}</Footer>}
    </ReactModal>
  );
};

Modal.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  closeable: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  closeButtonAlign: PropTypes.string,
  customClasses: PropTypes.string
};
Modal.defaultProps = {
  closeButtonAlign: 'right'
};

export { Modal };
