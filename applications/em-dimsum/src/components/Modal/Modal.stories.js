import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Modal } from './Modal';
import docs from './Modal.md';

const stories = storiesOf('Components/Modal', module).addDecorator(withReadme(docs));

stories.add('default', () => (
  <Modal onClose={action('closed')} isOpen>
    Ullamco et reprehenderit magna cillum ullamco consectetur et enim aliqua.
  </Modal>
));
stories.add('with title', () => (
  <Modal onClose={action('closed')} title="Hello" isOpen>
    Ullamco et reprehenderit magna cillum ullamco consectetur et enim aliqua.
  </Modal>
));
stories.add('closeable', () => (
  <Modal onClose={action('closed')} closeable isOpen title="Title" closeButtonAlign="left">
    Ullamco et reprehenderit magna cillum ullamco consectetur et enim aliqua.
  </Modal>
));
stories.add('desktop closeable', () => (
  <Modal onClose={action('closed')} closeable isOpen title="Title" closeButtonAlign="right" hasFooter={true}>
    Ullamco et reprehenderit magna cillum ullamco consectetur et enim aliqua.
  </Modal>
));
stories.add('long body desktop', () => (
  <Modal onClose={action('closed')} reverse isOpen hasFooter={true}>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint sit optio magnam veniam deserunt recusandae laborum
      similique, saepe illum inventore maiores. Beatae at vero minima dolorum illo id mollitia harum!
    </div>
  </Modal>
));
