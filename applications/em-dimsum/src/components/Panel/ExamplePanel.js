/*
This is a sample implementation of Panel.
*/
import React, { Component } from 'react';
import { Panel } from './Panel';

class ExamplePanel extends Component {
  constructor() {
    super();

    this.state = {
      panelIsOpen: false
    };

    this.openPanel = this.openPanel.bind(this);
    this.afterOpenPanel = this.afterOpenPanel.bind(this);
    this.closePanel = this.closePanel.bind(this);
  }

  openPanel() {
    this.setState({ panelIsOpen: true });
  }

  afterOpenPanel() {
    // Any action after opening action sheet. References are now sync'd and can be accessed.
    // this.subtitle.style.color = ' #1a8af3';
    // this.content.style.filter =  'blur(1px)';
  }

  closePanel() {
    this.setState({ panelIsOpen: false });
    // this.content.style.filter =  '';
  }

  render() {
    const buttonStyle = {
      background: `border-box`,
      padding: `8px`
    };

    const contentStyle = {
      padding: `16px`
    };
    return (
      <div ref={content => (this.content = content)} style={contentStyle}>
        <button style={buttonStyle} onClick={this.openPanel}>
          Open Panel {this.props.direction}
        </button>

        <Panel
          isOpen={this.state.panelIsOpen}
          onAfterOpen={this.afterOpenPanel}
          onClose={this.closePanel}
          contentLabel="Example Panel"
          direction={this.props.direction}
        >
          <div style={contentStyle}>
            <h2 ref={subtitle => (this.subtitle = subtitle)}>Panel</h2>
            <div>This is a Panel</div>
          </div>
        </Panel>
      </div>
    );
  }
}

export default ExamplePanel;
