import objstr from 'obj-str';
import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from '../Modal/Modal';

const Panel = ({ ...rest }) => {
  const classes = objstr({
    'em-ds-panel': true,
    left: rest.direction === 'left',
    right: rest.direction === 'right'
  });

  return (
    <Modal
      closeTimeoutMS={250} // Needed to smooth transition on closing actionsheet.
      portalClassName={classes}
      title={rest.title}
      closeable={false}
      onClose={rest.onClose}
      {...rest}
    >
      {rest.children}
    </Modal>
  );
};

Panel.propTypes = {
  onClose: PropTypes.func.isRequired,
  direction: PropTypes.string
};
Panel.defaultProps = {
  direction: 'left'
};

export { Panel };
