import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Panel } from './Panel';
import docs from './Panel.md';
import ExamplePanel from './ExamplePanel';

const stories = storiesOf('Components/Panel', module).addDecorator(withReadme(docs));

stories.add('Left', _ => <ExamplePanel direction="left" />);
stories.add('Right', _ => <ExamplePanel direction="right" />);
