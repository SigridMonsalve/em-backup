import React from 'react';
import { shallow } from 'enzyme';
import { Panel } from './Panel';

describe('Panel', () => {
  it('renders without crashing', () => {
    const closeCallback = () => {};
    shallow(<Panel onClose={closeCallback} />);
  });
});
