import React from 'react';
import PropTypes from 'prop-types';
import { ResponsiveContainer, PieChart, Pie, Cell, Label } from 'recharts';

const PieGraph = ({ data, colors, titleTextChart, valueTextChart, ...props }) => {
  return (
    <ResponsiveContainer height={110} width="100%" className="piechart-container">
      <PieChart>
        <Pie
          data={[{ value: 100 }]}
          dataKey="value"
          innerRadius="0%"
          outerRadius="98%"
          paddingAngle={0}
          fill="#697489"
          isAnimationActive={false}
        />
        <text x="50%" y="38%" fill="white" textAnchor="middle" dominantBaseline="middle" className="title-text-chart">
          {titleTextChart}
        </text>
        <Pie
          data={data}
          cx="50%"
          cy="50%"
          innerRadius="88%"
          outerRadius="100%"
          paddingAngle={0}
          isAnimationActive={false}
          dataKey="value"
        >
          {data.map((entry, index) => <Cell key={colors[index]} fill={colors[index % colors.length]} />)}
          <Label value={valueTextChart} fill="white" position="center" className="value-text-chart" />
        </Pie>
      </PieChart>
    </ResponsiveContainer>
  );
};

PieGraph.propTypes = {
  data: PropTypes.array,
  colors: PropTypes.array,
  titleTextChart: PropTypes.string,
  valueTextChart: PropTypes.string
};

export { PieGraph };
