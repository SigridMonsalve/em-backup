**Usage**
````
  <PieGraph
    colors={COLORS}
    data={data}
    titleTextChart="Title"
    valueTextChart="Text Center"
  />
````
This project uses reacharts library for piecharts. 
https://github.com/recharts/recharts


````
**data** -  Data for the chart (object = {name: '' , value: ''})-

**titleTextChart** - Title inside the chart-

**valueTextChart** - Text center inside the chart-

**colors** -  List of colors to show in the Pie chart
````
