import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { PieGraph } from './PieGraph';
import docs from './PieGraph.md';

const stories = storiesOf('Components/PieGraph', module).addDecorator(withReadme(docs));

const data = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 }
];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

stories.add('default', _ => (
  <PieGraph colors={COLORS} data={data} titleTextChart="Title" valueTextChart="Text Center" />
));
