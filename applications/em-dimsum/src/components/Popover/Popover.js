import React from 'react';
import PropTypes from 'prop-types';
import objstr from 'obj-str';

import { Modal } from '../Modal/Modal';

const Popover = ({ children, handleClose, isOpen, arrowRight, arrowLeft, target, ...props }) => {
  // Decided to handle event directly instead of leaving positioning calculations to the parent
  const { top = 0, left = 0, width = 0, height = 0 } = target.getBoundingClientRect();

  const popoverX = arrowRight ? left - (242 - width) : left; // Calculates which side of popover is going to align with parent
  const popoverY = top + height;
  const popoverPosition = {
    content: { top: `${popoverY}px`, left: `${popoverX}px` }
  };

  const arrowStyle = objstr({
    'arrow-point': true,
    left: arrowLeft && !arrowRight,
    right: arrowRight
  });

  return (
    <Modal
      style={popoverPosition}
      onClose={handleClose}
      isOpen={isOpen}
      className="em-ds-popover-modal"
      overlayClassName="em-ds-modal--overlay em-ds-popover--overlay"
      {...props}
    >
      {children}
      <div className={arrowStyle} />
    </Modal>
  );
};

Popover.propTypes = {
  isOpen: PropTypes.bool,
  handleClose: PropTypes.func,
  arrowLeft: PropTypes.bool,
  arrowRight: PropTypes.bool,
  target: PropTypes.object.isRequired
};

Popover.defaultProps = {
  isOpen: false,
  arrowRight: false,
  arrowLeft: true
};

export { Popover };
