**Usage**
````JSX
<Popover
  isOpen={true}
  handleClose={() => alert('Popover closed')}
  arrowLeft={} // optional. Default: true
  arrowRight={true} // optional. Default: false
  target={event.target} // To do: find a better approach
/>
````
**Description**

Popover triggered by click on element. Will be displayed on coordinates determined by click event.

**isOpen** - Pass initial state - true for open / false for closed state.

**handleClose** - Handler required to close popover on a click outside the component.

**target** - Target obtained by 'onClick' event.

**arrowLeft / arrowRight** - Arrow head alignment. Default is left.


