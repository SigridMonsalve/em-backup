import { storiesOf } from '@storybook/react';
import React, { Component } from 'react';
import { withReadme } from 'storybook-readme';
import docs from './Popover.md';
import { Popover } from './Popover';

const stories = storiesOf('Components/Popover', module).addDecorator(withReadme(docs));

stories.add('open', _ => <PopoverExample />);

/* This is a sample implementation of Popover. */
class PopoverExample extends Component {
  static buttonStyle = {
    padding: '8px',
    marginLeft: '40px'
  };
  constructor() {
    super();
    this.state = {
      popoverOpen: false,
      popoverTarget: null
    };
  }
  openPopover = (event, align) => {
    this.setState({ popoverOpen: true, popoverTarget: event.target, alignRight: align === 'right' });
  };
  closePopover = () => {
    this.setState({ popoverOpen: false, popoverTarget: null });
  };
  renderPopover = () => (
    <Popover
      isOpen={this.state.popoverOpen}
      target={this.state.popoverTarget}
      handleClose={this.closePopover}
      arrowRight={this.state.alignRight}
    >
      <div>Custom content passed as children</div>
    </Popover>
  );

  render() {
    return (
      <div style={PopoverExample.contentStyle}>
        <button style={PopoverExample.buttonStyle} onClick={event => this.openPopover(event, 'left')}>
          Open Popover / Left
        </button>
        <button style={PopoverExample.buttonStyle} onClick={event => this.openPopover(event, 'right')}>
          Open Popover / Right
        </button>
        {this.state.popoverOpen && this.renderPopover()}
      </div>
    );
  }
}
