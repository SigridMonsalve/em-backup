import objstr from 'obj-str';
import React from 'react';

const ProgressBar = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-progressbar': true
  });
  let currentProgress = props.progress * 100;
  if (props.show) {
    return (
      <div className={classes} {...props}>
        <div className="em-ds-progressbar em-ds-progress" style={{ width: currentProgress + '%' }} />
      </div>
    );
  } else {
    return <span />;
  }
};

ProgressBar.propTypes = {};
ProgressBar.defaultProps = {};

export { ProgressBar };
