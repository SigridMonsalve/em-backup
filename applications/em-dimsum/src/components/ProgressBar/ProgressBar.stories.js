import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ProgressBar } from './ProgressBar';
import docs from './ProgressBar.md';

const stories = storiesOf('Components/ProgressBar', module).addDecorator(withReadme(docs));

stories.add('default', _ => <ProgressBar>Hello</ProgressBar>);
