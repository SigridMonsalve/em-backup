import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import { Consumer } from '../InputGroupContext/InputGroupContext';
import { Label } from '../Label/Label';

const RadioOption = ({ id, label, ...props }) => {
  const classes = objstr({
    'em-ds-radiooption': true
  });

  return (
    <Consumer>
      {value => {
        return (
          <React.Fragment>
            <input
              id={id}
              type="radio"
              name={value.name}
              onChange={value.onChange}
              onClick={value.onClick}
              className={classes}
              {...props}
            />
            <Label htmlFor={id}>{label}</Label>
          </React.Fragment>
        );
      }}
    </Consumer>
  );
};

RadioOption.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string.isRequired
};

RadioOption.defaultProps = {};

export { RadioOption };
