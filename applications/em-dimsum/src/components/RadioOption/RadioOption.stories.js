import { storiesOf } from '@storybook/react';
import { withReadme } from 'storybook-readme';
import docs from './RadioOption.md';

const stories = storiesOf('RadioOption', module).addDecorator(withReadme(docs));

export { stories };
