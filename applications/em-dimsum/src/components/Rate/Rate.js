import React, { Component } from 'react';
import { Field } from '../Field/Field';

class Rate extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      amount: props.amount || 0,
      rate: props.rate || 0
    };
  }

  convertToFloat(numberString) {
    return parseFloat((numberString === '' ? '0' : numberString).replace(/[^0-9\.-]+/g, ''));
  }

  handleAmountChange = event => {
    const amount = event.target.value;
    const { baseAmount, onAmountChange, onRateChange } = this.props;
    const rate = (this.convertToFloat(amount) / baseAmount) * 100;
    const newState = isNaN(rate) ? { amount } : { amount, rate };
    this.setState(newState);
    typeof onAmountChange === 'function' && this.props.onAmountChange(amount);
    typeof onRateChange === 'function' && !isNaN(rate) && this.props.onRateChange(rate);
  };

  handleRateChange = event => {
    const rate = event.target.value;
    const { baseAmount, onAmountChange, onRateChange } = this.props;
    const amount = (this.convertToFloat(rate) * baseAmount) / 100;
    const newState = isNaN(amount) ? { rate } : { amount, rate };
    this.setState(newState);
    typeof onAmountChange === 'function' && !isNaN(amount) && this.props.onAmountChange(amount);
    typeof onRateChange === 'function' && this.props.onRateChange(rate);
  };

  render() {
    const { rateFieldName, amountFieldName } = this.props;
    return (
      <div className="em-ds-rate">
        <Field
          id={amountFieldName}
          name={amountFieldName}
          type="text"
          label={this.props.label}
          formatType="currency"
          decimalScale={2}
          value={this.state.amount}
          onChange={this.handleAmountChange}
        />

        <Field
          id={rateFieldName}
          name={rateFieldName}
          type="text"
          formatType="percent"
          decimalScale={3}
          value={this.state.rate}
          onChange={this.handleRateChange}
        />
      </div>
    );
  }
}

export { Rate };
