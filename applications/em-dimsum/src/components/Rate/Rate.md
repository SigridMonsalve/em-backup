Usage:
<Rate label="Down Payment" baseAmount="100000" amount="5000" rate= "5" />

For a base amount of 100000, 5% rate amounts to 5000.

Props:
label (type: string) : provide the label value for the rate field
baseAmout (type: number) : provide the base value, to which this rate field is applied to
amount (optional, type: number) : provide the amount value when the rate is applied to the base value
rate (optional, type: number) : the rate in percent 