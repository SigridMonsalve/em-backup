import objstr from 'obj-str';
import React, { Component } from 'react';
import { Icon, ICONS, Input } from '../index';

class SearchBar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const classes = objstr({
      'em-ds-search-bar': true
    });

    return (
      <div className={classes}>
        <div className="em-ds-button inner-button">
          <div className="button-align-inner" onClick={this.props.showSortOptions}>
            {this.props.buttonText}
            <Icon icon={ICONS.CHEVRON_DOWN} className="sort-button-logo" />
          </div>
        </div>
        <div className="input-field-container">
          <Icon icon={ICONS.SEARCH} className="icon-search" />
          <Input className="input-field-default" placeholder="Search" />
        </div>
      </div>
    );
  }
}
SearchBar.propTypes = {};
SearchBar.defaultProps = {
  buttonText: 'Name'
};

export { SearchBar };
