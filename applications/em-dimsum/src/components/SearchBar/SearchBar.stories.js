import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { SearchBar } from './SearchBar';
import docs from './SearchBar.md';

const stories = storiesOf('Components/SearchBar', module).addDecorator(withReadme(docs));

stories.add('default', _ => <SearchBar />);
