import objstr from 'obj-str';
import React, { Component } from 'react';
import { Input } from '../Input/Input';
import { Button } from '../Button/Button';
import { Icon, ICONS } from '../Icon/Icon';

class SearchSlider extends Component {
  constructor(props, context) {
    super(props, context);
  }

  // TODO: convert to stateful component to get ref also need function passed for instant search

  shouldComponentUpdate(nextProps) {
    return this.props.buttontext !== nextProps.buttontext;
  }

  render() {
    return (
      <div className="em-ds-search-slider">
        <div className="search-bar-fields">
          <div className="search-by-field" onClick={this.props.sortDropdown}>
            <div className="field-text">{this.props.buttontext}</div>
            <Icon icon={ICONS.CHEVRON_DOWN} className="search-options-button" />
          </div>
          <Input className="search-bar-slider-input" placeholder="Search" onChange={this.props.searchChangeHandler} />
        </div>
        <Button className="em-ds-button-Icon cancel-button" onClick={this.props.clickFunction}>
          <Icon icon={ICONS.CLOSE} className="cancel-button-icon" width={15} height={15} />
        </Button>
      </div>
    );
  }
}

SearchSlider.propTypes = {};
SearchSlider.defaultProps = {
  buttontext: 'Name'
};

export { SearchSlider };
