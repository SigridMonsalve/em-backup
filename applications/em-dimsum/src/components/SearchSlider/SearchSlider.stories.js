import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { SearchSlider } from './SearchSlider';
import docs from './SearchSlider.md';

const stories = storiesOf('Components/SearchSlider', module).addDecorator(withReadme(docs));

stories.add('closed', _ => <SearchSlider isOpen={false}>Hello</SearchSlider>);

stories.add('open', _ => <SearchSlider isOpen={true}>Hello</SearchSlider>);
