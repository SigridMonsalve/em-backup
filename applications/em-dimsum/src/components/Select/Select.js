import objstr from 'obj-str';
import React from 'react';
import PropTypes from 'prop-types';
import { ICONS, Icon } from '../Icon/Icon';

const Select = ({ ...rest }) => {
  const cssClasses = objstr({
    'em-ds-select': true
  });

  const renderOptions = options => {
    const items = Array.isArray(options) ? options : [];
    return items.map((el, index) => {
      return (
        <option value={el.value || ''} key={index}>
          {el.name || ''}
        </option>
      );
    });
  };

  return (
    <React.Fragment>
      <select className={cssClasses} {...rest}>
        <option>{rest.defaultValue}</option>
        {renderOptions(rest.options)}
      </select>
      <Icon icon={ICONS.CHEVRON_DOWN} />
    </React.Fragment>
  );
};

Select.propTypes = {
  classes: PropTypes.string,
  defaultValue: PropTypes.string,
  options: PropTypes.array,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onKeydown: PropTypes.func
};

Select.defaultProps = {
  defaultValue: 'Select'
};

export { Select };
