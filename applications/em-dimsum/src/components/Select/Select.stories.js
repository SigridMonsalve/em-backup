import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { action } from '@storybook/addon-actions';
import { Select } from './Select';
import docs from './Select.md';

const stories = storiesOf('Components/Select', module).addDecorator(withReadme(docs));
const options = [
  { name: 'Name1', value: 'value1' },
  { name: 'Name2', value: 'value2' },
  { name: 'Name2', value: 'value3' },
  { name: 'Name4', value: 'value4' }
];

stories.add('default', _ => (
  <Select
    options={options}
    defaultValue="Please Select an option"
    handleClick={action('Select Click')}
    handleFocus={action('Select Focus')}
    handleBlur={action('Select Blur')}
    handleKeydown={action('Select Keydown')}
  />
));
