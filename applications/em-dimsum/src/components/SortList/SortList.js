import objstr from 'obj-str';
import PropTypes from 'prop-types';
import React from 'react';
import { RadioOption } from '../RadioOption/RadioOption';
import { Group } from '../Group/Group';
import { Icon, ICONS } from '../Icon/Icon';

const SortList = ({ name, onChange, onClick, sortOptions, order, ...props }) => {
  const getArrow = (
    <div className="asc-desc-icons ascending">
      <Icon icon={ICONS.ARROW_REORDER_UP} disabled={order === 'ascending'} />
      <Icon icon={ICONS.ARROW_REORDER_DOWN} disabled={order === 'descending'} />
    </div>
  );

  return (
    <Group name={name} id={name} onChange={onChange} onClick={onClick}>
      <ul className="sort-list-container">
        {sortOptions.map((option, index) => {
          return (
            <li key={index} className="sort-list-options">
              <RadioOption
                defaultChecked={option.defaultChecked}
                label={option.label}
                value={option.value}
                id={option.id}
              />
              {option.defaultChecked && order && getArrow}
            </li>
          );
        })}
      </ul>
    </Group>
  );
};

SortList.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  sortOptions: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
  order: PropTypes.oneOf(['ascending', 'descending'])
};
SortList.defaultProps = {};

export { SortList };
