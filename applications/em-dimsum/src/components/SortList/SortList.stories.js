import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { SortList } from './SortList';
import docs from './SortList.md';

const stories = storiesOf('Components/SortList', module).addDecorator(withReadme(docs));
const sortOptions = [
  { id: 'payment', label: 'Payment', value: 'payment' },
  { id: 'product-type', label: 'Product Type', value: 'product type' },
  { id: 'product-term', label: 'Product Term', value: 'product term' }
];
stories.add('default', _ => <SortList name="sort-list-sample" sortOptions={sortOptions} />);
