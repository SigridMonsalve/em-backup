import objstr from 'obj-str';
import React from 'react';

const Spinner = ({ overlay, message = 'Loading...' }) => {
  if (overlay) {
    return (
      <div className={objstr({ 'em-ds-spinner': true, 'em-ds-spinner-overlay': overlay })}>
        <div className="spinner" />
        <div className="message">{message}</div>
      </div>
    );
  } else {
    return (
      <div className="em-ds-spinner">
        <div className="spinner" />
        <div className="message">{message}</div>
      </div>
    );
  }
};

Spinner.propTypes = {};
Spinner.defaultProps = {};

export { Spinner };
