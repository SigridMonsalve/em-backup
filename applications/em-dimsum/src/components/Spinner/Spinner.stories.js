import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Spinner } from './Spinner';
import docs from './Spinner.md';

const stories = storiesOf('Components/Spinner', module).addDecorator(withReadme(docs));

stories.add('without lightbox', _ => <Spinner />);
stories.add('with lightbox', _ => <Spinner overlay={true} />);
stories.add('with custom message', _ => <Spinner overlay={true} message="Evaluating Business Rules..." />);
