import objstr from 'obj-str';
import React from 'react';

const Step = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-step': true,
    'em-ds-accessed': props.access
  });
  return <li className={classes}> {props.access} </li>;
};

const Steps = ({ totalSteps, currentStep, children, ...props }) => {
  const classes = objstr({
    'em-ds-steps': true
  });
  let steps = [];
  totalSteps = totalSteps + 1;

  if (currentStep != currentStep + 1) {
    currentStep = currentStep + 1;
  }
  for (let i = 1; i <= totalSteps; i++) {
    steps.push(i <= currentStep);
  }

  return (
    <ul className={classes} {...props}>
      {steps.map((item, idx) => <Step key={idx} access={item} />)}
    </ul>
  );
};

Steps.propTypes = {};
Steps.defaultProps = {};

export { Steps };
