import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Steps } from './Steps';
import docs from './Steps.md';

const stories = storiesOf('Components/Steps', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Steps>Hello</Steps>);
