import React from 'react';
import objstr from 'obj-str';
import { ToastContainer, toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { ICONS, Icon } from '../Icon/Icon';

const Toast = ({ ...props }) => {
  const classes = objstr({
    'em-dimsum-toast': true
  });

  const Msg = ({ closeToast }) => {
    let iconType = 'CHECKMARK_CIRCLE_FILL';
    let iconClass = 'success';
    if (props.type === 'warning') {
      iconType = 'WARNING_CIRCLE_FILL';
      iconClass = 'warning';
    } else if (props.type === 'error') {
      iconType = 'WARNING_CIRCLE_FILL';
      iconClass = 'error';
    }
    return (
      <div className={classes}>
        <Icon className={'em-ds-icon ' + iconClass} icon={ICONS[iconType]} width={20} height={20} />
        <span>{props.title}</span>
        <Icon
          className={'em-ds-icon close'}
          icon={ICONS.CLOSE_MEDIUM}
          width={20}
          height={20}
          onClick={() => closeToast}
        />
        <span className="toast-subtitle">{props.subtitle}</span>
      </div>
    );
  };
  return <Msg />;
};

Toast.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string
};
Toast.defaultProps = {
  position: 'top-right'
};
export { Toast };
