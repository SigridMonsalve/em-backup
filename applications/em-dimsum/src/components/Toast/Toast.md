# Toast

DimSum uses react-toastify as the way to easily add notifications to your app. 
* Easy to setup for real, you can make it works in less than 10sec!
* Super easy to customize
* RTL support
* Swipe to close 👌
* Can display a react component inside the toast!
* Don't rely on findDOMNode or any DOM hack
* Has onOpen and onClose hooks. Both can access the props passed to the react component rendered inside the toast
* Can remove a toast programmatically
* Define behavior per toast
* Pause toast when the browser is not visible thanks to visibility API
* Fancy progress bar to display the remaining time
* Possibility to update a toast

## React Toastify Documentation
* [React Toastify Documentation on Github](https://github.com/fkhadra/react-toastify#demo)

## Important Information
* The ToastContainer component can be set with default options and then those options can be overridden at the individual toast level
* I did not style the toasts at all, the message text displayed in the example toast uses the Typography component styles, but other than that the toast styling is the default provided by React Toastify. We can style the toasts as we wish when designs are finalized.


 

