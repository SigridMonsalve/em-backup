import { storiesOf } from '@storybook/react';
import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { Toast } from './Toast';
import { withReadme } from 'storybook-readme';
import docs from './Toast.md';

const stories = storiesOf('Components/Toast', module).addDecorator(withReadme(docs));

const header = 'Toast Header';
const subtitle = 'This msg will act as subtitle. There will be a big msg to do this.';

const showToast = type => {
  toast(<Toast title={header} subtitle={subtitle} type={type} />, {
    className: 'toast-container',
    hideProgressBar: true,
    closeButton: false
  });
};

stories.add('Default Toast', _ => (
  <div>
    <div>
      <button onClick={() => showToast('warning')}>Warning</button>
    </div>
    <div>
      <button onClick={() => showToast('success')}>Success</button>
    </div>
    <div>
      <button onClick={() => showToast('error')}>Danger</button>
    </div>

    <ToastContainer />
  </div>
));
