import React, { Component } from 'react';
import { Button } from '../Button/Button';
import { Footer } from '../Footer/Footer';
import { Icon, ICONS } from '../Icon/Icon';
import { Steps } from '../Steps/Steps';

class Step extends Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}

class Wizard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0
    };
  }

  _next = event => {
    let currentStep = this.state.currentStep;
    const { onNextClick: clickHandler } = this.props.children[this.state.currentStep].props;

    const goToNext = () => {
      currentStep < this.state.totalSteps && currentStep++;
      this.setState({ currentStep: currentStep });
    };
    typeof clickHandler === 'function' ? clickHandler(event).then(() => goToNext()) : goToNext();
  };

  _prev = event => {
    let currentStep = this.state.currentStep;
    const { onPrevClick: clickHandler } = this.props.children[this.state.currentStep].props;

    const goToPrevious = () => {
      currentStep > 0 && currentStep--;
      this.setState({ currentStep: currentStep });
    };

    typeof clickHandler === 'function' ? clickHandler(event).then(() => goToPrevious()) : goToPrevious();
  };

  render() {
    this.state.totalSteps = this.props.children.length - 1;
    return (
      <React.Fragment>
        <div className="em-ds-wizard--content">{this.props.children[this.state.currentStep]}</div>
        <div className="em-ds-wizard-footer">
          <Steps totalSteps={this.state.totalSteps} currentStep={this.state.currentStep} />
          <Footer>
            {this.state.currentStep !== 0 ? (
              <span>
                <Button text icon onClick={this._prev}>
                  <Icon icon={ICONS.CHEVRON_LEFT} />
                  {this.props.children[this.state.currentStep - 1].props.title}
                </Button>
              </span>
            ) : (
              <span />
            )}
            {this.state.currentStep < this.state.totalSteps ? (
              <span>
                <Button text icon onClick={this._next}>
                  {this.props.children[this.state.currentStep + 1].props.title}
                  <Icon icon={ICONS.CHEVRON_RIGHT} />
                </Button>
              </span>
            ) : (
              <Button primary onClick={this.props.onSubmit}>
                {this.props.buttonText}
              </Button>
            )}
          </Footer>
        </div>
      </React.Fragment>
    );
  }
}

Wizard.propTypes = {};
Wizard.defaultProps = {};

export { Wizard, Step };
