import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Step, Wizard } from './Wizard';
import docs from './Wizard.md';

const stories = storiesOf('Components/Wizard', module).addDecorator(withReadme(docs));

stories.add('default', _ => (
  <Wizard>
    <Step title="Step 1" onNextClick={action('Step 1 next button')}>
      <div>Children in step 1 </div>
    </Step>
    <Step title="Step 2" onNextClick={action('Step 2 next button')} onPrevClick={action('Step 2 previous button')}>
      <div>Children in step 2 </div>
    </Step>
    <Step title="Step 3" onNextClick={action('Step 3 next button')} onPrevClick={action('Step 3 previous button')}>
      <div>Children in step 3 </div>
    </Step>
    <Step title="Step 4" onNextClick={action('Step 4 next button')} onPrevClick={action('Step 4 previous button')}>
      <div>Children in step 4 </div>
    </Step>
    <Step title="Step 5" onNextClick={action('Step 5 next button')} onPrevClick={action('Step 5 previous button')}>
      <div>Children in step 5 </div>
    </Step>
    <Step title="Step 6" onPrevClick={action('Step 6 previous button')}>
      <div>Children in step 6 </div>
    </Step>
  </Wizard>
));
