import React from 'react';

const Buttons = ({ children, ...props }) => {
  return <div {...props}>{children}</div>;
};

Buttons.propTypes = {};
Buttons.defaultProps = {};

export { Buttons };
