# Buttons

## Types
```jsx
<Button primary onClick="doSomething">Primary</Button>
<Button secondary onClick="doSomething">Secondary</Button>
<Button text onClick="doSomething">Text</Button>
<Button primary icon onClick="doSomething">
  <Icon icon={ICONS.SEARCH} width={16} height={16} />
</Button>
```
