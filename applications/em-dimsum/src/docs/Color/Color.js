import React from 'react';
import './Color.scss';

const Color = ({ children, ...props }) => {
  return (
    <div {...props}>
      <div className="colors-page-wrapper">
        <div>
          <h3>Neutral Colors</h3>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-000" style={{ border: `1px solid #ccc` }} />
            <p>000</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-050" />
            <p>050</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-080" />
            <p>080</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-100" />
            <p>100</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-200" />
            <p>200</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-300" />
            <p>300</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-400" />
            <p>400</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-500" />
            <p>500</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-600" />
            <p>600</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-700" />
            <p>700</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-800" />
            <p>800</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__ncd-900" />
            <p>900</p>
          </div>
        </div>

        <div>
          <h3>Brand Color</h3>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-100" />
            <p>100</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-200" />
            <p>200</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-300" />
            <p>300</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-400" />
            <p>400</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-500" />
            <p>500</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-600" />
            <p>600</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-700" />
            <p>700</p>
          </div>

          <div className="colors-page__color-container">
            <div className="color-page__color color-page__bc-800" />
            <p>800</p>
          </div>
        </div>

        <div>
          <h3>Feedback Colors</h3>
          <h4>Success</h4>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__fcs-300" />
            <p>300</p>
          </div>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__fcs-900" />
            <p>900</p>
          </div>

          <h4>Warning</h4>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__fcw-600" />
            <p>600</p>
          </div>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__fcw-900" />
            <p>900</p>
          </div>

          <h4>Danger</h4>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__fcd-200" />
            <p>200</p>
          </div>
          <div className="colors-page__color-container">
            <div className="color-page__color color-page__fcd-900" />
            <p>900</p>
          </div>
        </div>
      </div>
      {children}
    </div>
  );
};

Color.propTypes = {};
Color.defaultProps = {};

export { Color };
