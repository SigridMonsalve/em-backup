# Color

Dimsum groups color into 3 types; Brand, Neutral, and Feedback Colors.

- Brand Colors: These establish the personality of the brand. They should be used for accent elements such as primary buttons, CTA's and links.
- Neutral Colors: These are used for things like text, borders, shadows, background colors, etc.
- Feedback Colors: These are used to communicate status to the user and provide them with feedback. 

A color palette is all of these colors put together. DimSum offers a default palette that contains all of these types of colors. 

## Color System
Color follows a predictable scale driven by type and a range. Each heu or color has a Base `(400)` and from this base, we extract shades and tints of that heu. Each variation of this color is given a three-digit rage number. This provides a predictable color scale of "allowed" colors in the system; the higher a number is, the darker the color. 

## Usage
We make use of Sass Functions to extract colors from each type in a consistent way.

#### `em-ds-color($type, $range)`

Applies a color from our palette to a CSS property. `$type` refers to the type of color you would like to use (`brand`, `neutral`, `feedback`). `$range` refers to the scale or how dark you would like to color to be. If `$range` is not specified, the color will default to the base color for that type.

```sass
.div-container {
    background-color: em-ds-color(brand, 700); // returns a hex value for that type of color and its range
}
```

### Supporting Material
* [Color in Design Sytems](https://medium.com/eightshapes-llc/color-in-design-systems-a1c80f65fa3)
* [The Color System](https://material.io/design/color/the-color-system.html#color-usage-palettes)

