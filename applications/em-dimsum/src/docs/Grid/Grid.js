import objstr from 'obj-str';
import React from 'react';
import './Grid.scss';

const Grid = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-grid': true
  });
  return (
    <div className={classes} {...props}>
      {children}
    </div>
  );
};

export { Grid };
