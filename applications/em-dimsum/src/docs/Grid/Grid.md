# XY Grid

Dimsum uses Zurb Foundation's XY Grid system for layout. It features the following capabilities:

- Flexbox based
- The XY grid is supported in Chrome, Firefox, Safari 6+, IE10+, iOS 7+, and Android 4.4+.
- Auto sizing cells
- Fluid or fixed width grid containers
- Responsive behavior
- Vertical grids
- Use margin AND padding gutters
- Myriad of other features including but not limited to collapsible cells, block grids, push/pull, etc.

## Usage
TODO

### Supporting Material
* [Foundation XY Grid Documentation](https://foundation.zurb.com/sites/docs/xy-grid.html)

