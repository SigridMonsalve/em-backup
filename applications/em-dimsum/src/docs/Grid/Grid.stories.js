import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Input, Label } from '../../components/index';
import { Grid } from './Grid';
import docs from './Grid.md';

const stories = storiesOf('Welcome/Grid', module).addDecorator(withReadme(docs));

stories.add('Horizontal Grid', _ => (
  <Grid>
    <div className="grid-y grid-frame" id="sample-grid1">
      <div className="cell auto">
        <div className="grid-x grid-padding-x grid-padding-y large-up-3 medium-up-2 small-up-1">
          <div className="cell">
            <Label htmlFor="first-name">First Name</Label>
            <Input type="text" name="first-name" id="first-name" />
          </div>
          <div className="cell">
            <Label htmlFor="middle-name">Middle Name</Label>
            <Input type="text" name="middle-name" id="middle-name" />
          </div>
          <div className="cell">
            <Label htmlFor="Last Name">Last Name</Label>
            <Input type="text" name="last-name" id="last-name" />
          </div>
          <div className="cell">
            <Label htmlFor="Address 1">Address 1</Label>
            <Input type="text" name="address1-name" id="address1-name" />
          </div>
          <div className="cell">
            <Label htmlFor="city">City</Label>
            <Input type="text" name="city" id="city" />
          </div>
          <div className="cell">
            <Label htmlFor="state">State</Label>
            <Input type="text" name="state" id="state" />
          </div>
          <div className="cell">
            <Label htmlFor="zip">Zip</Label>
            <Input type="text" name="zip" id="zip" formatType="zipcode" />
          </div>
          <div className="cell">
            <Label htmlFor="marital-status">Marital Status</Label>
            <Input type="text" name="marital-status" id="marital-status" />
          </div>
          <div className="cell">
            <Label htmlFor="phone-number">Phone Number</Label>
            <Input type="phone" formatType="phone" name="phone-number" id="phone-number" />
          </div>
          <div className="cell">
            <Label htmlFor="loan-amount">Loan Amount</Label>
            <Input type="text" formatType="currency" name="loan-amount" id="loan-amount" decimalScale={2} />
          </div>
        </div>
      </div>
    </div>
  </Grid>
));

stories.add('Vertical Grid', _ => (
  <Grid>
    <div className="grid-y grid-frame grid-padding-x grid-padding-y" id="sample-grid2">
      <div className="cell">
        <h2>Header</h2>
      </div>
      <div className="cell auto">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam erat ante, finibus eget nisi non, laoreet
          hendrerit nisl. Sed quis consectetur urna. Sed eget cursus libero, vel sagittis lacus. Nulla auctor lorem id
          ante auctor tincidunt. Nam nec ex diam. Ut accumsan metus sed hendrerit semper. Maecenas faucibus mattis risus
          sit amet tincidunt. Proin nec lacus in orci vulputate varius.
        </p>
      </div>
    </div>
  </Grid>
));

export { stories };
