import React from 'react';
import './Spacing.scss';

const Space = ({ children, ...props }) => {
  return (
    <div {...props}>
      <div className="spacing-page__wrapper">
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-xxs" />
          xxs
        </div>
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-xs" />
          xs
        </div>
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-s" />
          s
        </div>
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-m" />
          m
        </div>
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-l" />
          l
        </div>
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-xl" />
          xl
        </div>
        <div>
          <div className="spacing-page__space__visual spacing-page__space__visual-xxl" />
          xxl
        </div>
      </div>

      {children}
    </div>
  );
};

Space.propTypes = {};
Space.defaultProps = {};

export { Space };
