# Spacing

DimSum uses an 8-point grid system as its base for overall space. This means that space uses multiples of 8px to define dimensions, padding, and margin of both block and inline elements. This ensures all measures follow the same spacing rules and avoids many headaches when designers are discussing space with developers. Smaller components, such as iconography and typography, can align to a 4px grid. Regardless of form-factor, most popular screen sizes are divisible by 8 on at least one axis - usually both. This allows us to achieve symmetry in our UI. 

## Spacing System
DimSum provides a predictable scale for fixed space using t-shirt sizing. 

```scss
$em-ds-spacing-data: (
  none: 0,                           
  xxs:  4px,
  xs:   8px,
  s:    16px,
  m:    24px,
  l:    32px,
  xl:   48px,
  xxl:  64px
);
```

## Usage
We make use of Sass functions specify the space we want to apply to objects in our mark up. 

### Functions

#### `em-ds-space($space)`
Applies spacing rules to a CSS property. `$space` refers to the scale in 8px of how much space you need. The higher the number, the bigger the font-size.

```sass
.div-container {
  padding: em-ds-space(m) em-ds-space(xl); // returns a pixel value for those variables
}
```

### Supporting Material
* [Intro to the 8-point Grid System](https://builttoadapt.io/intro-to-the-8-point-grid-system-d2573cde8632)
* [The Layout System](https://material.io/design/layout/understanding-layout.html#usage)


 
