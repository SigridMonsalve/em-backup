import React from 'react';
import './Typography.scss';

const Typography = ({ children, ...props }) => {
  return (
    <div {...props}>
      <h1 className="typography-page-heading1">H1 - The quick brown fox jumps over the lazy dog</h1>
      <h2 className="typography-page-heading2">H2 - The quick brown fox jumps over the lazy dog</h2>
      <h3 className="typography-page-heading3">H3 - The quick brown fox jumps over the lazy dog</h3>
      <h4 className="typography-page-heading4">H4 - The quick brown fox jumps over the lazy dog</h4>
      <h5 className="typography-page-heading5">H5 - The quick brown fox jumps over the lazy dog</h5>
      <p className="typography-page-section-header">Section Header - The quick brown fox jumps over the lazy dog</p>
      <p className="typography-page-body">Body - The quick brown fox jumps over the lazy dog</p>
      <p className="typography-page-body-small">Body Small - The quick brown fox jumps over the lazy dog</p>
      <a className="typography-page-link">Link - The quick brown fox jumps over the lazy dog</a>
      {children}
    </div>
  );
};

Typography.propTypes = {};
Typography.defaultProps = {};

export { Typography };
