# Typography

DimSum uses Proxima Nova as it Font Family with specific typographic styles that define a hierarchy for both mobile and web. 
* H1 
* H2
* H3
* H4
* H5
* section-header
* body
* body-small

## Type System
Font size, font weight, and line-height have their own predictive scales.

### Font Size
We use three digits to convey in size. The higher the range number, the higher the font-size. 
```scss
$em-ds-font-size-data: (
  700: 36px,
  600: 24px,
  500: 18px,
  400: 16px,
  300: 14px,
  200: 13px,
  100: 12px
);
```

### Font Weight
For font-weight we use words.
```sass
$em-ds-font-weight-data: (
  bold: 900,
  semibold: 600,
  base: 400,
  light: 300
);
```

### Line Height
We use three digits to convey in line-height. The higher the range number, the higher the line-height. 
```sass
$em-ds-line-height-data: (
  normal: normal,
  600: 1.5rem,
  500: 1.25rem,
  400: 1rem,
  300: .90rem,
  200: .75rem
);
```

## Usage
We make use of Sass functions and mixins to use in our mark up. 

### Functions

#### `em-ds-font-size($range)`
Applies a font size from the allowed sizes in the system to a CSS property. `$range` refers to the scale of how big you would like the font-size to be. The higher the number, the bigger the font-size.

```sass
.div-container {
    font-size: em-ds-font-size(500); // returns a pixel value for that range
}
```

#### `em-ds-font-weight($weight)`
Applies a font weight from the allowed weights in the system to a CSS property. `$weight` refers to either bold, semibold, base, light, etc.

```sass
.div-container {
    font-size: em-ds-font-weight(bold); // returns a weight value for that range (300 - 900)
}
```

#### `em-ds-line-height($range)`
Applies a line height from the allowed line heights in the system to a CSS property. `$range` refers to how high you would like the line-height to be. The higher the number, the higher the line-height.

```sass
.div-container {
    font-size: em-ds-font-weight(bold); // returns a weight value for that range (300 - 900)
}
```

### Mixins

#### `em-ds-h1-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-h1-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-h2-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-h2-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-h3-text-style($weight)`

Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-h3-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-h4-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-h4-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-h5-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-h5-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-section-header-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-section-header-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-body-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-body-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-body-small-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-body-small-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-body-list-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-body-list-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

#### `em-ds-body-link-text-style($weight)`
Applies all the text styling needed by Typographic Style defined in the system to a CSS declaration. `$weight` refers to the weight in the system you would like to use - optional parameter. 

```sass
.div-container {
    @include em-ds-body-link-text-style(bold); // returns all the styles applied to the type style (font-size, line-height, color and font-weight).
}
```

### Supporting Material
* [Responsive Typography With Sass Maps](https://www.smashingmagazine.com/2015/06/responsive-typography-with-sass-maps/)
* [The Type System](https://material.io/design/typography/the-type-system.html#type-scale)
* [Modular Scale](http://www.modularscale.com/)


 
