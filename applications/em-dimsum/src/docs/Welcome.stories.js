import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';

// intro
import { Welcome } from './Welcome/Welcome';
import docs from './Welcome/Welcome.md';

// color
import { Color } from './Color/Color';
import color from './Color/Color.md';

// typography
import { Typography } from './Typography/Typography';
import typography from './Typography/Typography.md';

// space
import { Space } from './Spacing/Spacing';
import space from './Spacing/Spacing.md';

const stories = storiesOf('Welcome', module);

stories.add('Intro', withReadme(docs, _ => <Welcome />));
stories.add('Color', withReadme(color, _ => <Color />));
stories.add('Typography', withReadme(typography, _ => <Typography />));
stories.add('Space', withReadme(space, _ => <Space />));
