import React from 'react';
import docs from '../../../README.md';

const Welcome = () => {
  return <div dangerouslySetInnerHTML={{ __html: docs }} />;
};

export { Welcome };
