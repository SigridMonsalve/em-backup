import objstr from 'obj-str';
import React from 'react';

const Component = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-component': true
  });
  return (
    <div className={classes} {...props}>
      {children}
    </div>
  );
};

Component.propTypes = {};
Component.defaultProps = {};

export { Component };
