import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { Component } from './Component';
import docs from './Component.md';

const stories = storiesOf('Component', module).addDecorator(withReadme(docs));

stories.add('default', _ => <Component>Hello</Component>);
