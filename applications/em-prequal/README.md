# em-prequal

Awesome new em-prequal.


### Setup Preup && Node >= 6.11.5
Ensure you have working Node.js version that is above 6.11.5 and Git setup and access to Ellie Mae private accounts
on NPM and Git. For information on how to set it up, look up our
[Dev Setup Guide](node_and_git.md)

Also install Preup with npm

```bash
npm install preup
```

### Install Dependencies
In the home folder with the package.json file in it. Run the following command

```bash
yarn run setup
```

### Run local server
Navigate down to em-prequal/applications/em-prequal

```bash
yarn run start
```