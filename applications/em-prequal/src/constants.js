const mockBaseUrl = window.location.hostname === 'localhost' ? '' : '/em-prequal';
const loanOpportunityBase = `${window.__env.apiHost}/los/v1`;
const osbUrl = `${window.__env.apiHost}/vendor/v2`;
export const URLS = {
  USER: `${window.__env.apiHost}/encompass/v1/users/aa01`,
  SETTINGS: `${window.__env.apiHost}/encompass-cg/v2/settings/CLIENT?Setting=CLIENTID`,
  OPPORTUNITIES: `${mockBaseUrl}/api/opportunities.json`,
  OPPORTUNITY: `${mockBaseUrl}/api/opportunity.json`,
  SCENARIOS: `${mockBaseUrl}/api/scenarios.json`,
  SCENARIO: `${mockBaseUrl}/api/scenario.json`,
  EVP: `${osbUrl}/vendor/transactions`,
  SCENARIO_RESULTS: `${mockBaseUrl}/api/scenarioResult.json`,
  OSB: osbUrl
};
