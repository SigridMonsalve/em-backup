export const EMResources = [
  { name: 'Encompass Support', url: 'https://resourcecenter.elliemae.com/resourcecenter/welcome.aspx' },
  { name: 'Encompass Training', url: 'https://resourcecenter.elliemae.com/resourcecenter/Training.aspx' },
  { name: 'Privacy Policies', url: 'https://resourcecenter.elliemae.com/resourcecenter/privacy.aspx' }
];
