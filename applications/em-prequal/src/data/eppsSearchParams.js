export const eppsSearchParams = {
  RepresentativeCreditScore: '789',
  LoanType: ['Conventional', 'FHA'],
  LoanDocumentationType: 'FullDocumentation',
  AmortizationType: ['Fixed'],
  LoanTerm: ['180', '300'],
  LoanPurpose: 'Purchase',
  //TODO: Loan Purpose/LoanTerms/LoanType based on the values change other values
  PurposeofRefinance: '',
  PurchasePrice: '500000',
  EstimatedValue: '',
  AppraisedValue: '500000',
  LockPeriod: '',
  ARM1stTerm: [''],
  SubordinateFinancingBalance: '',
  BaseLoanAmount: '300000',
  MI_MIP_FF_Financed: '',
  TotalLoanAmount: '300000',
  LTV: '',
  CLTV: '',
  SubjectPropertyState: 'CA',
  PostalCode: '94568',
  NumberofUnits: '1',
  PropertyType: 'Attached',
  OccupancyType: 'PrimaryResidence',
  // 'FrontEndDTI': this.fronEndDTI || 119.849,// todo
  // 'BackEndDTI': this.amountRequiredToClose || 120.906, // todo
  TotalMonthlyIncome: '',
  Assets: '',
  AUS_Engine: '',
  Recommendation_LP: '', // this.setRecommendation, // todo
  Recommendation_DU: '', // todo
  //  'FHATotalScoreCard': 100, //  todo
  ImpoundWaiver: '',
  SelfEmployed: 'false',
  PrepaymentPenalty: '',
  '12moHousingPaymentHistory': '', // todo
  InterestOnly: 'false',
  LOCompensationPaidBy: 'Borrower Paid', // todo
  TargetPrice: '',
  TargetRate: '3.5',
  LienPosition: 'FirstLien'
  // todo - Add ENCW fields after getting new json
  // 'ProductType': this.ProductType,
  // 'CashOut': this.cashFromToBorrowerAmount
};
