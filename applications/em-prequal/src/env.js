/**
 * WARNING: DO NOT CHANGE THIS FILE UNLESS YOU KNOW HOW THE JENKINS BUILDS WORKS
 * WARNING: DO NOT CHANGE THIS FILE UNLESS YOU KNOW HOW THE JENKINS BUILDS WORKS
 * WARNING: DO NOT CHANGE THIS FILE UNLESS YOU KNOW HOW THE JENKINS BUILDS WORKS
 */

window.__env = {};

/**
 * A Domain indicates if we are running on DEV/QA/STAGE, etc environments...
 */
window.__env.domain = 'LOCALHOST'; // this will be replace in the deploy

/**
 * An api host is an API hostname which gets replaced with appropriate hostname per environment during jenkins builds
 * you can use this apiHost variable to compose other URLS.. in env.derived.js file
 *
 * idpHost and serverUrl are variables needed for Ping ID Two Factor authentication
 */
if (window.__env.domain === 'LOCALHOST') {
  // local host
  window.__env.apiHost = 'https://int.api.ellielabs.com'; // -au
  window.__env.idpHost = 'https://int.idp.ellielabs.com'; // -tu
  // additional hacks for localhost
  if (window.location.pathname === '/') {
    window.location.pathname = '/em-prequal';
  }
} else if (window.__env.domain === 'DEV') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'DEV2') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'QA') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'QA2') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'QA3') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'RELQA') {
  window.__env.apiHost = 'https://encompass-staging-api.elliemae.com/v2';
  window.__env.idpHost = 'https://stg.idp.elliemae.com/authorize';
} else if (window.__env.domain === 'ETET') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'XP') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'OAPI') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'INT') {
  window.__env.apiHost = 'https://int.api.ellielabs.com';
  window.__env.idpHost = 'https://int.idp.ellielabs.com';
} else if (window.__env.domain === 'PEG-OSB1') {
  window.__env.apiHost = 'https://encompass-peg-api.elliemae.com/v2';
  window.__env.idpHost = 'https://peg.idp.ellielabs.com/authorize';
} else if (window.__env.domain === 'PEG-OSB2') {
  window.__env.apiHost = 'https://encompass-peg2-api.elliemae.com/v2';
  window.__env.idpHost = 'https://peg.idp.ellielabs.com/authorize';
} else if (window.__env.domain === 'PEG-OAPI-WEST') {
  window.__env.apiHost = 'https://peg2-west.api.ellielabs.com/encompass-cg/v2';
  window.__env.idpHost = 'https://peg.idp.ellielabs.com/authorize';
} else if (window.__env.domain === 'PEG-OAPI-EAST') {
  window.__env.apiHost = 'https://peg2-east.api.ellielabs.com/encompass-cg/v2';
  window.__env.idpHost = 'https://peg.idp.ellielabs.com/authorize';
} else if (window.__env.domain === 'PEG2-OAPI-EAST') {
  window.__env.apiHost = 'https://peg2-east.api.ellielabs.com/encompass-cg/v2';
  window.__env.idpHost = 'https://peg.idp.ellielabs.com/authorize';
} else {
  console.error('Invalid environment', window.__env.domain);
}
