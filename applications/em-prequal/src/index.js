import './index.scss';
import * as serviceWorker from './serviceWorker';
import { checkDevice } from './utils/device';
import { login } from './utils/login';
serviceWorker.unregister(); // TODO: change to register

import(/* webpackChunkName: "env" */ './env.js').then(() => {
  login(window.__env).then(_ => {
    // TODO: Convert to promise based API rather than using callbacks...
    const device = checkDevice();
    window.__env.device = device;
    // TODO: Conditional Mobile vs Tablet vs Desktop Loading...
    if (device === 'mobile') {
      import('./mobile/pages');
    } else {
      import('./web/pages'); /*TODO: replace wrapper below with this line when we are ready for web
      import('./web/pages/wrapper').then(() => {
        import('./mobile/pages');
      });*/
    }
  });
});
