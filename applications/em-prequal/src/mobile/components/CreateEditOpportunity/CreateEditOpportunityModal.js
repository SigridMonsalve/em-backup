import { Modal, ConfirmationModal } from '@elliemae/em-dimsum';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { initOpportunityModal } from '../../../redux/OpportunityModal/opportunityModal.actions';
import { OpportunityForms } from '../CreateEditOpportunity/OpportunityForms/OpportunityForms';
import { createOpportunity, saveOpportunity, clearOpportunity } from '../../../redux/Opportunity/opportunity.actions';

const mapStateToProps = state => ({
  opportunityModal: state.opportunityModal,
  opportunity: state.opportunity,
  globalFlags: state.globalFlags
});

const mapDispatchToProps = dispatch => ({
  initOpportunityModal: () => dispatch(initOpportunityModal()),
  createOpportunity: opportunity => dispatch(createOpportunity(opportunity)),
  clearOpportunity: () => dispatch(clearOpportunity()),
  saveOpportunity: opportunity => dispatch(saveOpportunity(opportunity))
});

class CreateEditOpportunityModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type: props.type,
      title: props.title,
      subHeader: props.subHeader,
      isOpportunityModalOpen: props.isOpportunityModalOpen,
      isConfirmationModalOpen: false,
      closeOpportunityModal: props.closeOpportunityModal
    };
  }

  componentDidMount() {
    this.props.initOpportunityModal();
  }

  onOpportunityModalClose = () => {
    this.props.globalFlags.opportunityForms.isDirty
      ? this.toggleConfirmationModal()
      : this.state.closeOpportunityModal();
  };

  toggleConfirmationModal = () => {
    const { isConfirmationModalOpen } = this.state;
    this.setState({ isConfirmationModalOpen: !isConfirmationModalOpen });
    isConfirmationModalOpen && this.state.closeOpportunityModal();
  };

  discardFormChanges = () => {
    this.props.clearOpportunity();
    this.toggleConfirmationModal();
  };

  saveOpportunity = () => {
    const { opportunity } = this.props;

    if (opportunity.id) {
      return this.props.saveOpportunity(opportunity).then(response => {
        this.toggleConfirmationModal();
      });
    } else {
      this.props.createOpportunity(opportunity).then(response => {
        this.toggleConfirmationModal();
      });
    }
  };

  renderConfirmationModal = () => {
    const { opportunity } = this.props;
    return (
      <ConfirmationModal
        type="confirm"
        title="Save Opportunity"
        isOpen={true}
        closeButton={{ text: 'Discard', action: this.discardFormChanges }}
        actionButton={{ text: 'Save', action: this.saveOpportunity }}
      >
        Do you want to save this opportunity as draft?
      </ConfirmationModal>
    );
  };

  render() {
    return (
      <Modal
        isOpen={this.state.isOpportunityModalOpen}
        onClose={this.onOpportunityModalClose}
        closeable
        title={this.props.opportunityModal.header}
        headerLevel={5}
        subHeader={this.props.opportunityModal.subHeader}
        customClasses="opportunity-forms-container"
        closeButtonAlign="left"
      >
        <OpportunityForms opportunityId={this.props.opportunityId} type={this.state.type} />
        {this.state.isConfirmationModalOpen && this.renderConfirmationModal()}
      </Modal>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateEditOpportunityModal);
