import React, { Component } from 'react';
import { Field, Heading, Label, ButtonCarousel, Group, RadioOption, ButtonGroup } from '@elliemae/em-dimsum';
import { updateOpportunity } from '../../../../redux/Opportunity/opportunity.actions';
import { connect } from 'react-redux';
import { set } from 'lodash';
import loanInfoProperties from '../../../../data/loanInfoProperties.json';
import { template } from 'handlebars';

const mapDispatchToProps = dispatch => ({
  updateOpportunity: opportunity => dispatch(updateOpportunity(opportunity))
});

const mapStateToProps = state => ({
  opportunity: state.opportunity
});

const LOAN_TERMS = 'LOAN_TERMS';
const LOAN_TYPES = 'LOAN_TYPES';

const lienOptions = loanInfoProperties.lienOptions;
const occupancyOptions = loanInfoProperties.occupancyOptions;
const propertyOptions = loanInfoProperties.propertyOptions;
const documentOptions = loanInfoProperties.documentOptions;

class LoanInfoComponent extends Component {
  constructor(props) {
    super(props);

    // For deep copy.
    const loanInfoOptions = JSON.parse(JSON.stringify(loanInfoProperties));
    const { loanTypeUiOptions, loanTermUiOptions } = loanInfoOptions;
    this.state = {
      loanTermOptions: loanTermUiOptions,
      loanTypeOptions: loanTypeUiOptions,
      rateSelected: true
    };
  }

  static getDerivedStateFromProps(props, state) {
    // Get current selected loan terms and loan types from props (store).
    const { opportunity: newOpportunity } = props;
    const { loanInfo: newLoanInfo = {} } = newOpportunity;
    const { loanTerms = [], loanTypes = [] } = newLoanInfo;

    // Get all available loan types and loan terms from json (which was also stored in state).
    const { loanTermOptions, loanTypeOptions } = state;
    const getLoanOptions = (list, currentSelections) => {
      return list.map(option => {
        if (currentSelections.includes(option.value)) {
          option.defaultChecked = true;
        }
        return option;
      });
    };

    const newLoanTermOptions = getLoanOptions(loanTermOptions, loanTerms);
    const newLoanTypeOptions = getLoanOptions(loanTypeOptions, loanTypes);

    return {
      loanTermOptions: newLoanTermOptions,
      loanTypeOptions: newLoanTypeOptions
    };
  }

  handleCarouselClick = (event, type) => {
    const { value, checked } = event.target;

    const { opportunity: newOpportunity } = this.props;
    const { loanInfo: newLoanInfo = {} } = newOpportunity;
    const { loanTerms = [], loanTypes = [] } = newLoanInfo;

    const { loanTermOptions, loanTypeOptions } = this.state;
    if (type === LOAN_TERMS) {
      loanTermOptions.forEach(option => {
        if (value === option.value) {
          checked ? loanTerms.push(value) : loanTerms.splice(loanTerms.indexOf(value), 1);
          option.defaultChecked = checked;
        }
      });
      newLoanInfo.loanTerms = loanTerms;
    } else if (type === LOAN_TYPES) {
      loanTypeOptions.forEach(option => {
        if (value === option.value) {
          checked ? loanTypes.push(value) : loanTypes.splice(loanTypes.indexOf(value), 1);
          option.defaultChecked = checked;
        }
      });
      newLoanInfo.loanTypes = loanTypes;
    }

    this.setState({
      loanTermOptions: loanTermOptions,
      loanTypeOptions: loanTypeOptions
    });

    newOpportunity.loanInfo = newLoanInfo;
    this.props.updateOpportunity(newOpportunity);
  };

  handleChange(value, keyPath, type) {
    const updatedOpportunity = { ...this.props.opportunity };
    // TODO: Update with a better way to set rather than using lodash.
    keyPath && set(updatedOpportunity, keyPath, type === 'numeric' ? Number(value) : value);
    this.props.updateOpportunity(updatedOpportunity);
  }

  targetRateOrPriceChange() {
    // TODO: Confirm if the fields need to be cleared on selection change.
    this.setState(prevState => {
      return { rateSelected: !prevState.rateSelected };
    });
  }

  render() {
    const { opportunity } = this.props;
    const { loanInfo, subjectProperty } = opportunity;

    // Maintaining in local state as the API returns only selected items rather than full list.
    const { loanTermOptions, loanTypeOptions } = this.state;

    let subjectProperty2 =
      subjectProperty.loanPurpose === 'Cash Out Refinance' || subjectProperty.loanPurpose === 'No Cash Out Refinance';
    return (
      <React.Fragment>
        <div className="form-field-group">
          <Field
            type="select"
            name="lienPosition"
            id="lienPosition"
            label="Lien Position"
            defaultValue={(loanInfo && loanInfo.lienPosition) || ''}
            options={lienOptions}
            onChange={event => this.handleChange(event.target.value, 'loanInfo.lienPosition')}
            required={true}
          />
          <Field
            type="select"
            name="occupancyType"
            id="occupancyType"
            label="Occupancy Type"
            defaultValue={(loanInfo && loanInfo.occupancyType) || ''}
            options={occupancyOptions}
            onChange={event => this.handleChange(event.target.value, 'loanInfo.occupancyType')}
            required={true}
          />
          <Field
            type="select"
            name="propertyType"
            id="propertyType"
            label="Property Type"
            defaultValue={(loanInfo && loanInfo.propertyType) || ''}
            options={propertyOptions}
            onChange={event => this.handleChange(event.target.value, 'loanInfo.propertyType')}
            required={true}
          />
          <Field
            type="select"
            name="documentType"
            id="documentType"
            label="Document Type"
            defaultValue={(loanInfo && loanInfo.documentType) || ''}
            options={documentOptions}
            onChange={event => this.handleChange(event.target.value, 'loanInfo.documentType')}
            required={true}
          />
        </div>

        <div className="form-field-group">
          <div className="fields-section">
            <Heading level={3}>Loan Term</Heading>
            <ButtonCarousel
              buttons={loanTermOptions}
              rows={3}
              columns={2}
              onClick={e => this.handleCarouselClick(e, LOAN_TERMS)}
            />
            <Heading level={3}>Loan Products</Heading>
            <ButtonGroup
              buttons={loanTypeOptions}
              rows={2}
              columns={2}
              onClick={e => this.handleCarouselClick(e, LOAN_TYPES)}
            />
          </div>

          <div className="fields-section">
            <Heading level={3}>Compensation</Heading>
            <Group
              name="compensation"
              id="compensation"
              onChange={event => this.handleChange(event.target.value, 'loanInfo.compensation')}
            >
              <RadioOption
                id="lender"
                label="Lender"
                value="Lender"
                defaultChecked={loanInfo && loanInfo.compensation == 'Lender'}
              />
              <RadioOption
                id="borrower"
                label="Borrower"
                value="Borrower"
                defaultChecked={loanInfo && loanInfo.compensation == 'Borrower'}
              />
            </Group>
          </div>

          <div className="fields-section">
            <Heading level={3}>Loan Target Rate/Price</Heading>
            <Label>Enter the desired Target Rate or Price to Filter Product Options</Label>
            <Group name="loanTargetRatePrice" id="loanTargetRatePrice" onChange={e => this.targetRateOrPriceChange(e)}>
              <div className="target-rate-wrapper">
                <div className="radial-wrapper">
                  <RadioOption
                    className="rate-button"
                    id="rate"
                    label="Rate"
                    value="rate"
                    checked={this.state.rateSelected}
                  />
                </div>
                <Field
                  type="text"
                  name="targetRate"
                  id="targetRate"
                  formatType="percent"
                  placeholder={this.state.rateSelected ? '' : Number(loanInfo && loanInfo.targetRate) || ''}
                  required={true}
                  decimalScale={3}
                  defaultValue={this.state.rateSelected ? '' : Number(loanInfo && loanInfo.targetRate) || ''}
                  disabled={this.state.rateSelected ? '' : 'disabled'}
                  onValueChange={event => this.handleChange(event.floatValue, 'loanInfo.targetRate', 'numeric')}
                />
              </div>

              <div className="target-price-wrapper">
                <div className="radial-wrapper">
                  <RadioOption
                    className="price-button"
                    id="price"
                    label="Price"
                    value="price"
                    checked={!this.state.rateSelected}
                  />
                </div>
                <Field
                  type="text"
                  formatType="currency"
                  name="targetPrice"
                  id="targetPrice"
                  placeholder={!this.state.rateSelected ? '' : Number(loanInfo && loanInfo.targetPrice) || ''}
                  disabled={this.state.rateSelected ? 'disabled' : ''}
                  decimalScale={2}
                  defaultValue={!this.state.rateSelected ? '' : Number(loanInfo && loanInfo.targetPrice) || ''}
                  onValueChange={event => this.handleChange(event.floatValue, 'loanInfo.targetPrice', 'numeric')}
                />
              </div>
            </Group>
          </div>

          {subjectProperty.loanPurpose === 'Purchase' && (
            <Field
              type="text"
              name="subordinateFinancingPayment"
              id="subordinateFinancingPayment"
              formatType="currency"
              label="Subordinate Financing Payment (Optional)"
              onValueChange={event =>
                this.handleChange(event.floatValue, 'loanInfo.subordinateFinancingPayment', 'numeric')
              }
              placeholder={Number(loanInfo && loanInfo.subordinateFinancingPayment) || ''}
              decimalScale={2}
            />
          )}
          {subjectProperty2 && (
            <Field
              type="text"
              name="reSubordinatedSecondBalance"
              id="reSubordinatedSecondBalance"
              formatType="currency"
              label="Re-Subordinated Second Balance (Optional)"
              onValueChange={event =>
                this.handleChange(event.floatValue, 'loanInfo.reSubordinatedSecondBalance', 'numeric')
              }
              placeholder={Number(loanInfo && loanInfo.subordinateFinancingPayment) || ''}
              decimalScale={2}
            />
          )}
          {subjectProperty2 && (
            <Field
              type="text"
              name="reSubordinatedSecondPayment"
              id="reSubordinatedSecondPayment"
              formatType="currency"
              label="Re-Subordinated Second Payment (Optional)"
              onValueChange={event =>
                this.handleChange(event.floatValue, 'loanInfo.reSubordinatedSecondPayment', 'numeric')
              }
              placeholder={Number(loanInfo && loanInfo.subordinateFinancingPayment) || ''}
              decimalScale={2}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

const LoanInfo = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoanInfoComponent);

export { LoanInfo };
