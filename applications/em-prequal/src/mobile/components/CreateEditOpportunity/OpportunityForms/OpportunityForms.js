import { Step, Wizard, Spinner } from '@elliemae/em-dimsum';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { PersonalInfo } from './PersonalInfo';
import { PropertyInfo } from './PropertyInfo';
import { LoanInfo } from './LoanInfo';
import {
  clearOpportunity,
  createOpportunity,
  saveOpportunity
} from '../../../../redux/Opportunity/opportunity.actions';
import {
  initOpportunityModal,
  updateOpportunityModal
} from '../../../../redux/OpportunityModal/opportunityModal.actions';
import { translate } from 'react-i18next';

const MAX_STEPS = 3;
const mapStateToProps = state => ({
  opportunity: state.opportunity,
  opportunityModal: state.opportunityModal
});

const mapDispatchToProps = dispatch => ({
  createOpportunity: opportunity => dispatch(createOpportunity(opportunity)),
  clearOpportunity: () => dispatch(clearOpportunity()),
  updateOpportunityModal: modal => dispatch(updateOpportunityModal(modal)),
  initOpportunityModal: () => dispatch(initOpportunityModal()),
  saveOpportunity: opportunity => dispatch(saveOpportunity(opportunity))
});

class OpportunityFormsComponent extends Component {
  componentDidMount() {
    this.updateModal(this.props.type, 'Personal Info', false, true);
  }

  componentWillUnmount() {
    this.props.initOpportunityModal();
  }

  // TODO: Move this as a utility.
  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  updateModal = (mode, subHeader, isPrev, isCreate) => {
    const step = { ...this.props.opportunityModal }.step;
    const newStep = isCreate ? step : isPrev ? step - 1 : step + 1;
    this.props.updateOpportunityModal({
      header: `${this.capitalize(mode)} Opportunity - ${newStep} of ${MAX_STEPS}`,
      subHeader: subHeader,
      step: newStep
    });
  };

  showScenarioSearchResults = () => {
    const { opportunity } = this.props;
    if (opportunity.id) {
      this.props.saveOpportunity(opportunity).then(() => {
        // Navigate to scenarios listing page.
        this.props.history.push(`/opportunity/${opportunity.id}/scenarios`);
      });
    }
  };

  saveOpportunity = (subHeader, isPrev, isCreate) => {
    const { opportunity } = this.props;

    if (opportunity.id) {
      return this.props.saveOpportunity(opportunity).then(() => {
        this.updateModal(this.props.type, subHeader, isPrev, isCreate);
      });
    } else {
      return this.props.createOpportunity(opportunity).then(response => {
        this.updateModal(this.props.type, subHeader, isPrev, isCreate);
      });
    }
  };

  render() {
    const { opportunity, t } = this.props;
    return (
      <Wizard onSubmit={this.showScenarioSearchResults} buttonText="Show Scenarios">
        <Step title="Personal Info" onNextClick={() => this.saveOpportunity('Property Info')}>
          <PersonalInfo />
        </Step>
        <Step
          title="Property Info"
          onNextClick={() => this.saveOpportunity('Loan Info')}
          onPrevClick={() => this.saveOpportunity('Personal Info', true)}
        >
          <PropertyInfo />
        </Step>
        <Step title="Loan Info" onPrevClick={() => this.saveOpportunity('Property Info', true)}>
          <LoanInfo />
          {opportunity.showCreateOpportunitySpinner ? (
            <Spinner overlay={true} message={t('CREATE_OPPORTUNITY_SPINNER')} />
          ) : (
            ''
          )}
        </Step>
      </Wizard>
    );
  }
}

OpportunityFormsComponent.propTypes = {};
OpportunityFormsComponent.defaultProps = {};

const OpportunityForms = translate('translations')(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(OpportunityFormsComponent)
  )
);

export { OpportunityForms };
