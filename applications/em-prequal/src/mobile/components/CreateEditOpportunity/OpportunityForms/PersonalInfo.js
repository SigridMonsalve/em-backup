import { Field, Button, Icon, ICONS, Heading, Label } from '@elliemae/em-dimsum';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateOpportunity } from '../../../../redux/Opportunity/opportunity.actions';
import InputOptions from '../../../../data/options.json';
import { set } from 'lodash';

const mapStateToProps = state => ({
  opportunity: state.opportunity
});

const mapDispatchToProps = dispatch => ({
  updateOpportunity: opportunity => dispatch(updateOpportunity(opportunity))
});

class PersonalInfoComponent extends Component {
  getVeteranStatusList() {
    return InputOptions['VAVOB.X72'];
  }

  getEmptyBorrower(borrowerIndex) {
    return {
      id: '',
      firstName: '',
      lastName: '',
      suffixToName: '',
      borrowerIndex: borrowerIndex,
      emailAddressText: '',
      mobilePhone: '',
      entityDeleted: false
    };
  }

  addBorrowerHandler(event) {
    event.preventDefault();

    const { opportunity: newOpportunity } = this.props;
    if (Array.isArray(newOpportunity.borrowers)) {
      newOpportunity.borrowers.push(this.getEmptyBorrower(newOpportunity.borrowers.length));
    }

    this.props.updateOpportunity(newOpportunity);
  }

  deleteBorrowerHandler(event, bIndex) {
    const newOpportunity = { ...this.props.opportunity };
    const borrowersList = [...this.props.opportunity.borrowers];

    const borrowerIndex = borrowersList.findIndex(borrower => {
      return borrower.id ? borrower.id === bIndex : borrower.borrowerIndex === bIndex;
    });

    borrowersList[borrowerIndex].entityDeleted = true;
    newOpportunity.borrowers = [...borrowersList];
    this.props.updateOpportunity(newOpportunity);
  }

  handleChange(value, keyPath, type) {
    const updatedOpportunity = { ...this.props.opportunity };
    // TODO: Find a better immutable way to set rather than lodash set.
    keyPath && set(updatedOpportunity, keyPath, type === 'numeric' ? Number(value) : value);
    this.props.updateOpportunity(updatedOpportunity);
  }

  render() {
    const { opportunity } = this.props;
    if (!opportunity.borrowers) {
      opportunity.borrowers = [];
      opportunity.borrowers.push(this.getEmptyBorrower(0));
    }
    let borrowers = null;
    let title = null;

    if (opportunity.borrowers.length) {
      borrowers = (
        <React.Fragment>
          {opportunity.borrowers.map((borrower, index) => {
            let bIndex = borrower.id ? borrower.id : borrower.borrowerIndex;
            if (!borrower.entityDeleted) {
              if (index > 0) {
                title = (
                  <div className="heading-container">
                    <Heading level={3}>Co-borrower</Heading>
                    <Icon
                      icon={ICONS.DELETE}
                      width={16}
                      height={16}
                      onClick={event => this.deleteBorrowerHandler(event, bIndex)}
                    />
                  </div>
                );
              } else {
                title = <Heading level={3}>Borrower</Heading>;
              }
              return (
                <div className="fields-section" key={bIndex}>
                  {title}
                  <Field
                    type="text"
                    label="First Name"
                    id={`firstName-${bIndex}`}
                    name="firstName"
                    defaultValue={borrower.firstName}
                    onChange={event => this.handleChange(event.target.value, `borrowers[${bIndex}].firstName`)}
                  />

                  <Field
                    type="text"
                    label="Middle Name (Optional)"
                    id={`middleName-${bIndex}`}
                    name="middleName"
                    defaultValue={borrower.middleName}
                    onChange={event => this.handleChange(event.target.value, `borrowers[${bIndex}].middleName`)}
                  />

                  <Field
                    type="text"
                    label="Last Name"
                    id={`lastName-${bIndex}`}
                    name="lastName"
                    defaultValue={borrower.lastName}
                    onChange={event => this.handleChange(event.target.value, `borrowers[${bIndex}].lastName`)}
                  />

                  <Field
                    type="text"
                    label="Suffix (Optional)"
                    id={`suffixToName-${bIndex}`}
                    name="suffixToName"
                    defaultValue={borrower.suffixToName}
                    onChange={event => this.handleChange(event.target.value, `borrowers[${bIndex}].suffixToName`)}
                  />

                  <Field
                    type="text"
                    label="Email"
                    id={`emailAddressText-${bIndex}`}
                    name="emailAddressText"
                    defaultValue={borrower.emailAddressText}
                    onChange={event => this.handleChange(event.target.value, `borrowers[${bIndex}].emailAddressText`)}
                  />

                  <Field
                    type="text"
                    label="Mobile Phone"
                    formatType="phone"
                    id={`mobilePhone-${bIndex}`}
                    name="mobilePhone"
                    value={borrower.mobilePhone}
                    onValueChange={event =>
                      this.handleChange(event.floatValue, `borrowers[${bIndex}].mobilePhone`, 'numeric')
                    }
                  />
                </div>
              );
            }
          })}
        </React.Fragment>
      );
    }

    return (
      <form className="opportunity-forms-personal-info-container">
        <div className="form-field-group">
          {borrowers}

          <div className="fields-section">
            <Button id="addBorrower" icon secondary onClick={event => this.addBorrowerHandler(event)}>
              <Icon icon={ICONS.ADD} width={16} height={16} />
            </Button>
            <Label htmlFor="addBorrower">Add Co-Borrower</Label>
          </div>
        </div>

        <div className="form-field-group">
          <Field
            type="text"
            label="Estimated Credit Score"
            formatType="creditscore"
            value={opportunity.estimatedCreditScore}
            id="estimatedCreditScore"
            name="estimatedCreditScore"
            onValueChange={event => this.handleChange(event.floatValue, `estimatedCreditScore`, 'numeric')}
          />

          <Label size="small">
            The credit report won't be pulled, because only estimated credit score is needed to generate loan scenarios.
          </Label>
        </div>

        <div className="form-field-group">
          <Field
            type="select"
            id="veteranStatus"
            name="veteranStatus"
            label="Type of Veteran"
            options={this.getVeteranStatusList()}
            defaultValue={opportunity.veteranStatus}
            onChange={event => this.handleChange(event.target.value, `veteranStatus`)}
          />

          <Field
            type="checkbox"
            name="firstTimeHomebuyer"
            id="firstTimeHomebuyer"
            label="1st Time Homebuyer"
            defaultValue={opportunity.firstTimeHomebuyer}
            defaultChecked={opportunity.firstTimeHomebuyer === true ? 'checked' : ''}
            onChange={event => this.handleChange(event.target.checked, `firstTimeHomebuyer`)}
          />
        </div>
      </form>
    );
  }
}

PersonalInfoComponent.propTypes = {
  opportunity: PropTypes.object
};

const PersonalInfo = connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonalInfoComponent);

export { PersonalInfo };
