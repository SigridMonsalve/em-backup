import { Field, Rate } from '@elliemae/em-dimsum';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { States } from '../../../../data/states';
import InputOptions from '../../../../data/options.json';
import PropTypes from 'prop-types';
import { updateOpportunity } from '../../../../redux/Opportunity/opportunity.actions';
import { set } from 'lodash';

const mapStateToProps = state => ({
  opportunity: state.opportunity
});

const mapDispatchToProps = dispatch => ({
  updateOpportunity: newValue => dispatch(updateOpportunity(newValue))
});

class PropertyInfoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPurchasePriceVisible: false
    };
  }

  handleChange(value, keyPath, type) {
    const updatedOpportunity = { ...this.props.opportunity };
    // TODO: Find a better immutable way to set rather than lodash set.
    keyPath && set(updatedOpportunity, keyPath, type === 'numeric' ? Number(value) : value);
    this.props.updateOpportunity(updatedOpportunity);
  }

  renderPurchasePrice() {
    const { subjectProperty } = this.props.opportunity;
    return subjectProperty && subjectProperty.loanPurpose === 'Purchase' ? (
      <Field
        id="purchasePriceAmount"
        name="purchasePriceAmount"
        type="text"
        value={subjectProperty && subjectProperty.purchasePriceAmount}
        label="Purchase Price"
        fieldid="136"
        formatType="currency"
        onValueChange={event => this.handleChange(event.floatValue, 'subjectProperty.purchasePriceAmount', 'numeric')}
        decimalScale={2}
      />
    ) : (
      ''
    );
  }

  // TODO: Confirm with API team and add right API mapping.
  renderPurposeOfRefinance() {
    const { subjectProperty } = this.props.opportunity;
    return (subjectProperty && subjectProperty.loanPurpose === 'Cash-Out Refinance') ||
      (subjectProperty && subjectProperty.loanPurpose === 'NoCash-Out Refinance') ? (
      <Field id="purposeOfRefinance" type="select" label="Purpose of Refinance" options={InputOptions['299']} />
    ) : (
      ''
    );
  }

  renderUnPaidPrincipleBalance() {
    const { subjectProperty } = this.props.opportunity;
    return (subjectProperty && subjectProperty.loanPurpose === 'Cash-Out Refinance') ||
      (subjectProperty && subjectProperty.loanPurpose === 'NoCash-Out Refinance') ? (
      <Field
        id="unpaidPrincipleBalance"
        name="unpaidPrincipleBalance"
        type="text"
        label="Unpaid Principle Balance"
        value={subjectProperty && subjectProperty.unpaidPrincipleBalance}
        onValueChange={event =>
          this.handleChange(event.floatValue, 'subjectProperty.unpaidPrincipleBalance', 'numeric')
        }
        formatType="currency"
        decimalScale={2}
      />
    ) : (
      ''
    );
  }

  getBaseAmount(subjectProperty) {
    const { loanPurpose, purchasePriceAmount, estimatedPropertyValue } = subjectProperty || {};
    return loanPurpose === 'Purchase' ? purchasePriceAmount : estimatedPropertyValue;
  }

  render() {
    const { opportunity } = this.props;
    const { subjectProperty } = opportunity;
    return (
      <div className="form-field-group">
        <div className="fields-section">
          <Field
            id="loanPurpose"
            name="loanPurpose"
            type="select"
            label="Loan Purpose"
            fieldid="19"
            defaultValue={subjectProperty && subjectProperty.loanPurpose}
            options={InputOptions['19']}
            onChange={event => this.handleChange(event.target.value, 'subjectProperty.loanPurpose')}
          />
          {this.renderPurchasePrice()}
          <Rate
            label="Down Payment"
            baseAmount={this.getBaseAmount(subjectProperty)}
            rateFieldName="downPaymentPercent"
            amountFieldName="downPaymentAmount"
            amount={subjectProperty && subjectProperty.downPaymentAmount}
            onAmountChange={value => this.handleChange(value, 'subjectProperty.downPaymentAmount', 'numeric')}
            rate={subjectProperty && subjectProperty.downPaymentPercent}
            onRateChange={value => this.handleChange(value, 'subjectProperty.downPaymentPercent', 'numeric')}
          />
          <Field
            id="loanAmount"
            name="loanAmount"
            type="text"
            label="Loan Amount"
            value={subjectProperty && subjectProperty.loanAmount}
            onValueChange={event => {
              this.handleChange(event.floatValue, 'subjectProperty.loanAmount', 'numeric');
            }}
            formatType="currency"
            decimalScale={2}
          />
          {this.renderPurposeOfRefinance()}
          {this.renderUnPaidPrincipleBalance()}
          <Field
            id="estimatedPropertyValue"
            name="estimatedPropertyValue"
            type="text"
            label="Estimated Value"
            value={subjectProperty && subjectProperty.estimatedPropertyValue}
            onValueChange={event => {
              this.handleChange(event.floatValue, 'subjectProperty.estimatedPropertyValue', 'numeric');
            }}
            formatType="currency"
            decimalScale={2}
          />
        </div>

        <div className="fields-section">
          <Field
            id="FinancedMI"
            name="FinancedMI"
            type="text"
            label="Financed MI (PMI, MIP, FF) (Optional)"
            fieldid="1045"
            formatType="currency"
            decimalScale={2}
            value={subjectProperty && subjectProperty.FinancedMI}
            onValueChange={event => {
              this.handleChange(event.floatValue, 'subjectProperty.FinancedMI', 'numeric');
            }}
          />
          <Rate
            amountFieldName="annualTaxesAmount"
            rateFieldName="annualTaxesPercent"
            label="Annual Taxes"
            baseAmount={this.getBaseAmount(subjectProperty)}
            amount={subjectProperty && subjectProperty.annualTaxesAmount}
            onAmountChange={value => this.handleChange(value, 'subjectProperty.annualTaxesAmount', 'numeric')}
            rate={subjectProperty && subjectProperty.annualTaxesPercent}
            onRateChange={value => this.handleChange(value, 'subjectProperty.annualTaxesPercent', 'numeric')}
          />
          <Rate
            amountFieldName="annualInsuranceAmount"
            rateFieldName="annualInsurancePercent"
            label="Annual Insurance"
            baseAmount={this.getBaseAmount(subjectProperty)}
            amount={subjectProperty && subjectProperty.annualInsuranceAmount}
            onAmountChange={value => this.handleChange(value, 'subjectProperty.annualInsuranceAmount', 'numeric')}
            rate={subjectProperty && subjectProperty.annualInsurancePercent}
            onRateChange={value => this.handleChange(value, 'subjectProperty.annualInsurancePercent', 'numeric')}
          />
          <Field
            id="monthlyHOAFee"
            name="monthlyHOAFee"
            type="text"
            value={subjectProperty && subjectProperty.monthlyHOAFee}
            onValueChange={event => this.handleChange(event.floatValue, 'subjectProperty.monthlyHOAFee', 'numeric')}
            label="Monthly HOA Fee"
            fieldid="233"
            formatType="currency"
            decimalScale={2}
          />
        </div>

        <div className="fields-section">
          <Field
            id="zipCode"
            name="zipCode"
            type="text"
            value={subjectProperty && subjectProperty.zipCode}
            onValueChange={event => this.handleChange(event.floatValue, 'subjectProperty.zipCode', 'numeric')}
            label="Zip Code"
            formatType="zipcode"
          />
          <Field
            id="city"
            name="city"
            type="text"
            defaultValue={subjectProperty && subjectProperty.city}
            onChange={event => this.handleChange(event.target.value, 'subjectProperty.city')}
            label="City"
          />
          <Field
            id="state"
            name="state"
            type="select"
            defaultValue={subjectProperty && subjectProperty.state}
            onChange={event => this.handleChange(event.target.value, 'subjectProperty.state')}
            label="State"
            options={States}
          />
          <Field
            id="county"
            name="county"
            type="text"
            defaultValue={subjectProperty && subjectProperty.county}
            onChange={event => this.handleChange(event.target.value, 'subjectProperty.county')}
            label="County"
          />
        </div>
      </div>
    );
  }
}

PropertyInfoComponent.propTypes = {};
PropertyInfoComponent.defaultTypes = {};

const PropertyInfo = connect(
  mapStateToProps,
  mapDispatchToProps
)(PropertyInfoComponent);

export { PropertyInfo };
