import React from 'react';
import { ConfirmationModal } from '@elliemae/em-dimsum';
import PropTypes from 'prop-types';
import { session } from '../../../utils/storage';

const CREATE_LOAN_TEXT = 'Create Loan';
const CREATE_LOAN_MODAL_VERBIAGE = 'Do you want to switch to Pipeline to create a new loan using this scenario data?';

class CreateLoanModal extends React.Component {
  closeCreateLoanModal = () => {
    this.props.closeButtonCallBack();
  };

  doCreateLoanOnPipeline = () => {
    session.set('createLoanFrom', this.props.createLoanFrom);
    const params = this.props.params;
    const newHref = `/pipeline?opportunityId=${params.opportunityId}&scenarioId=${params.scenarioId}`;
    window.location.href = newHref;
  };

  render() {
    return (
      <ConfirmationModal
        type="confirm"
        title={CREATE_LOAN_TEXT}
        isOpen={this.props.isOpen}
        onClose={() => {
          return false;
        }}
        closeButton={{ text: 'No', action: this.closeCreateLoanModal }}
        actionButton={{ text: 'Yes', action: this.doCreateLoanOnPipeline }}
      >
        {CREATE_LOAN_MODAL_VERBIAGE}
      </ConfirmationModal>
    );
  }
}

CreateLoanModal.propTypes = {
  closeButtonCallBack: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  createLoanFrom: PropTypes.string.isRequired
};

export { CreateLoanModal };
