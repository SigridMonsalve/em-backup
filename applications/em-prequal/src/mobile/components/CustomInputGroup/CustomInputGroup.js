import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Group, RadioOption, Input, Error } from '@elliemae/em-dimsum';

class CustomInputGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: this.props.selectedValue
    };
  }

  onClickHandler(value) {
    this.setState({
      selectedValue: value
    });
  }

  render() {
    const { error, name, id, options, ...rest } = this.props;

    return (
      <div className="custom-input-wrapper">
        <Group name={name} id={id}>
          {options.map((option, index) => {
            return (
              <div className="custom-input-group-item" key={index}>
                <div className="custom-input-group-column">
                  <RadioOption
                    defaultChecked={this.state.selectedValue}
                    value={option.value}
                    id={option.id}
                    label={option.label}
                    onClick={this.onClickHandler.bind(this, option.value, index)}
                  />
                </div>
                <div className="custom-input-group-column">
                  <Input disabled={option.value !== this.state.selectedValue ? 'disabled' : ''} />
                </div>
              </div>
            );
          })}
        </Group>
        {error && (
          <Error id={`${name}Error`} role="alert" palette="danger">
            {error}
          </Error>
        )}
      </div>
    );
  }
}

CustomInputGroup.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string || PropTypes.number,
  options: PropTypes.arrayOf(PropTypes.object)
};
CustomInputGroup.defaultProps = {};

export { CustomInputGroup };
