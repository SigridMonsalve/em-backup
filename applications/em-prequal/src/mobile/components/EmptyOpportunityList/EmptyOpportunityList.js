import React from 'react';
import { Icon, ICONS } from '@elliemae/em-dimsum';
import { translate } from 'react-i18next';

const EmptyOpportunityList = ({ ...props }) => {
  const { t } = props;

  return (
    <div className="empty-opportunity-list">
      <Icon icon={ICONS.NO_OPPORTUNITIES} width={343} height={125} />
      <div className="empty-list-header">
        <p>No Opportunities</p>
      </div>
      <div className="empty-list-content">
        <p>
          {t('EMPTY_OPPORTUNITIES_LINE1')} <br /> {t('EMPTY_OPPORTUNITIES_LINE2')}
        </p>
      </div>
    </div>
  );
};

export default translate('translations')(EmptyOpportunityList);
