import React from 'react';
import { Button, ButtonCarousel, Heading, Rate, Footer } from '@elliemae/em-dimsum';
const LoanFilter = ({ ...props }) => {
  return (
    <div>
      <div className="loan-filter-list">
        <div>
          <div>
            <Heading level={3}>Loan Products</Heading>
            <ButtonCarousel buttons={props.loanProducts} rows={2} columns={2} showNav={false} />
            <Heading level={3}>Loan Term</Heading>
            <ButtonCarousel buttons={props.loanOptions} rows={3} columns={2} />
          </div>
          <hr />

          <div>
            <Heading level={3}>Down Payment</Heading>
            <Rate
              amount={props.opportunity.subjectProperty.downPaymentAmount}
              rate={props.opportunity.subjectProperty.downPaymentPercent}
            />
          </div>
        </div>
      </div>
      <Footer customClasses="footer">
        <Button primary>Apply</Button>
      </Footer>
    </div>
  );
};

export { LoanFilter };
