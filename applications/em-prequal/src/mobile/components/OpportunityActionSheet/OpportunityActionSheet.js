import React from 'react';
import { Actionsheet, Icon, ICONS } from '@elliemae/em-dimsum';
import { OpportunityInfoSection } from '../OpportunityInfoSection/OpportunityInfoSection';

const OpportunityActionSheet = ({ opportunity, isOpen, onClose, editAction, deleteAction, ...props }) => {
  return (
    <Actionsheet isOpen={isOpen} onClose={onClose} {...props}>
      <OpportunityInfoSection opportunity={opportunity} showOpportunityActions={false} inActionSheet={true} />
      <ul className="opportunity-actions">
        <li onClick={editAction}>
          <Icon icon={ICONS.EDIT_PENCIL} width={16} height={16} />
          <a>Edit Opportunity</a>
        </li>
        <li onClick={deleteAction}>
          <Icon icon={ICONS.DELETE} width={16} height={16} />
          <a>Delete Opportunity</a>
        </li>
      </ul>
    </Actionsheet>
  );
};

export { OpportunityActionSheet };
