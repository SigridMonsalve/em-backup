import React from 'react';
import { Icon, ICONS } from '@elliemae/em-dimsum';

const OpportunityInfoSection = ({
  opportunity,
  showOpportunityActions,
  openOpportunityActions,
  inActionSheet,
  ...props
}) => {
  const checkSubjectProperty = () => {
    return opportunity.propertyCity || opportunity.propertyState || opportunity.propertyZip;
  };

  const propertyInfo = () => {
    if (checkSubjectProperty()) {
      return (
        <React.Fragment>
          <p className="dot-separator">&bull;</p>
          <p>
            {opportunity.propertyCity ? `${opportunity.propertyCity}, ` : ''}
            {opportunity.propertyState} {opportunity.propertyZip}
          </p>
        </React.Fragment>
      );
    } else {
      return null;
    }
  };

  return (
    <div className={'info-section ' + (inActionSheet ? 'in-action-sheet' : '')}>
      <div className="personal-info-section">
        <p>
          {opportunity.borrowerLastName ? `${opportunity.borrowerLastName}, ` : ''}
          {opportunity.borrowerFirstName}
        </p>
        {showOpportunityActions && (
          <Icon icon={ICONS.MORE_OPTIONS_VERT} width={18} height={18} onClick={openOpportunityActions} />
        )}
      </div>
      <div className="property-info-section">
        <p>{opportunity.loanPurpose}</p>
        {propertyInfo()}
      </div>
    </div>
  );
};

export { OpportunityInfoSection };
