import React from 'react';
import { Card, PieGraph, Heading, Icon, ICONS } from '@elliemae/em-dimsum';

const ScenarioComparisonCard = ({ scenario, clickCard, moreOptions, ...props }) => {
  const COLORS = ['#FF9400', '#32B87C', '#52A6EC', '#006AA9'];

  const d = new Date(scenario.dateCreated); //used to format date
  const date = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;

  const generalInfo = [
    { name: 'Rate', value: `${scenario.terms ? scenario.terms.noteRate.toFixed(3) : ''} %` },
    { name: 'APR', value: `${scenario.terms ? scenario.terms.noteRate.toFixed(3) : ''} %` },
    { name: 'Fees', value: `$ ${scenario.fee[0] ? scenario.fee[0].fixedFee : ''}` }
  ];
  const graphData = [
    { name: 'P&I', value: scenario.terms ? scenario.terms.annualInsuranceAmount : '' },
    { name: 'Taxes', value: scenario.terms ? scenario.terms.annualTaxesAmount : '' },
    { name: 'Insurance', value: scenario.terms ? scenario.terms.annualInsuranceAmount : '' },
    { name: 'HOA', value: scenario.terms ? scenario.terms.annualTaxesAmount : '' } //mockup data to be defined with API integration
  ];

  const graphText = `$ ${scenario.fee[0].totalAmount.toFixed(2)}`;

  const generalInfoItems = generalInfo.map(item => {
    return (
      <div className="general-info" key={item.name}>
        <div className="general-info-title">{item.name}</div>
        <div className="general-info-content">{item.value}</div>
      </div>
    );
  });

  return (
    <div className="scenario-card" onClick={clickCard}>
      <Card>
        <div className="em-ds-card-header">
          <div className="em-ds-card-heading">
            <Heading level={2}>CONV</Heading>
            <Heading level={5}>{`${scenario.terms ? scenario.terms.term : ''}  ${
              scenario.terms ? scenario.terms.loanType : ''
            }`}</Heading>
          </div>
          <div className="header-left-info-card">{date}</div>
          <Icon className="em-ds-card-icon" icon={ICONS.MORE_OPTIONS_VERT} onClick={moreOptions} size={20} />
        </div>
        <div className="data-container">
          <div className="graph-container">
            <PieGraph colors={COLORS} data={graphData} titleTextChart="MONTHLY" valueTextChart="$33,370" />
          </div>
          <div className="info-container">
            <div className="info-title">
              5.000 Points &bull; {scenario.terms ? scenario.terms.downPaymentPercent : ''}% Down &bull; Wells Fargo
            </div>
            <div className="general-info-container">{generalInfoItems}</div>
          </div>
        </div>
      </Card>
    </div>
  );
};

ScenarioComparisonCard.propTypes = {};
ScenarioComparisonCard.defaultProps = {};

export { ScenarioComparisonCard };
