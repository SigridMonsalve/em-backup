import React from 'react';
import { Label } from '@elliemae/em-dimsum';

const ScenarioDetailsContent = ({ scenario, ...props }) => {
  return (
    <div className="scenario-details-wrapper content">
      <div className="scenario-details-container">
        {/* TODO: include the monthly payment graphical representation component */}
        <div className="scenario-details-monthly-payment column">Graph</div>

        <div className="scenario-details column">
          <section id="propertyInfo">
            <div className="section-header">Property Info</div>
            <div className="scenario-details-row">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Annual Taxes</div>
                <Label formatType="percent" labelValue={scenario.terms.annualTaxesPercent} /> |{' '}
                <Label labelValue={scenario.terms.annualTaxesAmount} formatType="currency" />
              </div>
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Annual Ins.</div>
                <Label formatType="percent" labelValue={scenario.terms.annualInsurancePercent} /> |{' '}
                <Label labelValue={scenario.terms.annualInsuranceAmount} formatType="currency" />
              </div>
            </div>
            <div className="scenario-details-row border">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Monthly MI</div>
                <Label formatType="percent" labelValue="0" /> | <Label labelValue="0.00" formatType="currency" />
              </div>
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Upfront MI</div>
                <Label formatType="percent" labelValue="0" /> | <Label labelValue="0.00" formatType="currency" />
              </div>
            </div>
            <div className="scenario-details-row">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Occupancy Type</div>
                <Label>Primary</Label>
              </div>
            </div>
            <div className="scenario-details-row">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Zip code</div>
                <Label>95132</Label>
              </div>
              <div className="scenario-details-cell">
                <div className="section-seconday-header">State</div>
                <Label>CA</Label>
              </div>
              <div className="scenario-details-cell">
                <div className="section-seconday-header">City</div>
                <Label>San Jose</Label>
              </div>
            </div>
          </section>

          <section id="loanInfo" className="item">
            <div className="section-header">Loan Info</div>
            <div className="scenario-details-row">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Loan Purpose</div>
                <Label>Purchase</Label>
              </div>
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Purchase Price</div>
                <Label formatType="currency" labelValue="7837.00" />
              </div>
            </div>
            <div className="scenario-details-row">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Down Payment</div>
                <Label formatType="percent" labelValue="8.75" /> | <Label labelValue="7837.00" formatType="currency" />
              </div>
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Loan Amount</div>
                <Label formatType="currency" labelValue="7837.00" />
              </div>
            </div>
            <div className="scenario-details-row">
              <div className="scenario-details-cell">
                <div className="section-seconday-header">Lien Position</div>
                <Label>First</Label>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export { ScenarioDetailsContent };
