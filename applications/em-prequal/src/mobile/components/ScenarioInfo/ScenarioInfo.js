import React from 'react';
import { Heading } from '@elliemae/em-dimsum';

const ScenarioInfo = ({ header, subheader, ...props }) => {
  return (
    <React.Fragment>
      <Heading level={3}>{header}</Heading>
      <Heading level={5} className="sub-header">
        {subheader}
      </Heading>
    </React.Fragment>
  );
};

export { ScenarioInfo };
