import React from 'react';
import { Actionsheet, Icon, ICONS } from '@elliemae/em-dimsum';
import { ScenarioInfo } from '../ScenarioInfo/ScenarioInfo';

const ScenarioInfoActionSheet = ({ isOpen, onClose, header, subheader, emailAction, deleteAction, ...props }) => {
  const EMAIL_TEXT = 'Email';
  const REMOVE_COMPARISON_TEXT = 'Remove from comparison';

  return (
    <Actionsheet isOpen={isOpen} onClose={onClose} {...props}>
      <div className="scenario-info-header">
        <ScenarioInfo header={header} subheader={subheader} />
      </div>
      <ul className="scenario-actions">
        <li onClick={emailAction}>
          <Icon icon={ICONS.MESSAGES} width={16} height={16} />
          <a>{EMAIL_TEXT}</a>
        </li>
        <li onClick={deleteAction}>
          <Icon icon={ICONS.DELETE} width={16} height={16} />
          <a>{REMOVE_COMPARISON_TEXT}</a>
        </li>
      </ul>
    </Actionsheet>
  );
};

export { ScenarioInfoActionSheet };
