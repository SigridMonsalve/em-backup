import objstr from 'obj-str';
import React from 'react';

import { Field, Icon, ICONS, Label } from '@elliemae/em-dimsum';

const ScenarioResultCard = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-scenarioresultcard': true
  });
  let Price = Math.abs(props.Price);

  // const _isPointNegative = point => point < 0;

  const isPointNegative = props.Point < 0;
  // const isPointNegative = _isPointNegative(props.Point)
  const pointColor = isPointNegative ? 'green' : 'red';

  const pointLabel = isPointNegative ? `+${Math.abs(props.Point)}` : props.Point;

  return (
    <div className={classes} {...props}>
      <Field id={`id-${props.id}`} label type="checkbox" onChange={props.onChange} />
      <div className="em-ds-cardfield">
        <div>
          <div>
            {props.flag}
            <label className="em-ds-boldcolor">{props.Type}</label>
            <label className="em-ds-smalltext">{props.Term} yr fixed</label>
            <Icon icon={ICONS.MORE_OPTIONS_VERT} height={18} width={18} className="em-ds-scenarioicon" />
          </div>
          <div class="em-ds-lighttext">
            <label>{props.lender}</label>
          </div>
          <div className="em-ds-scenarioalign">
            <div>
              <label className="em-ds-lighttext">Monthly</label>
              <br />
              <Label formatType="currency" labelValue={Price} />
            </div>
            <div>
              <label className="em-ds-lighttext">Rate</label>
              <br />
              <Label formatType="percent" labelValue={props.Rate} />
            </div>
            <div>
              <label className="em-ds-lighttext">Fees</label>
              <br />
              <Label formatType="currency" labelValue={props.Fees} />
            </div>
            <div>
              <label className="em-ds-lighttext">points</label>
              <br />
              <Label style={{ color: pointColor }} formatType="number" decimalScale={3} labelValue={pointLabel} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

ScenarioResultCard.propTypes = {};
ScenarioResultCard.defaultProps = {};

export { ScenarioResultCard };
