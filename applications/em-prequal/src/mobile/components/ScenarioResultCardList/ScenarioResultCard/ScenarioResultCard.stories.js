import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ScenarioResultCard } from './ScenarioResultCard';
import docs from './ScenarioResultCard.md';

const stories = storiesOf('ScenarioResultCard', module).addDecorator(withReadme(docs));

stories.add('default', _ => <ScenarioResultCard>Hello</ScenarioResultCard>);
