import objstr from 'obj-str';
import React from 'react';
import { ScenarioResultCard } from './ScenarioResultCard/ScenarioResultCard';

const getFlagFromData = function(data) {
  if (data['@FHA'] === '1') {
    return 'FHA';
  }

  if (data['@VA'] === '1') {
    return 'VA';
  }

  if (data['@USDA'] === '1') {
    return 'USDA';
  }

  return 'CONV';
};

const ScenarioResultCardList = ({ children, ...props }) => {
  const classes = objstr({
    'em-ds-scenarioresultcardList': true
  });

  return (
    <div className="em-ds-scenario-result-card-list">
      <div className="em-ds-scenarioheader">{props.program} Yr Fixed</div>
      {props.scenarios.LoanProgamRate.map((scenario, index) => {
        let id = index + '' + props.id;

        const flag = getFlagFromData(props.scenarios);
        return (
          <ScenarioResultCard
            flag={flag}
            id={id}
            onChange={props.onChange}
            lender={props.lender}
            Rate={scenario['@Rate']}
            Fees={scenario['@PnI']}
            Price={scenario['@PriceVal']}
            Point={100 - scenario['@Price']}
            Term={props.term}
            Type={props.type}
          />
        );
      })}
    </div>
  );
};

ScenarioResultCardList.propTypes = {};
ScenarioResultCardList.defaultProps = {};

export { ScenarioResultCardList };
