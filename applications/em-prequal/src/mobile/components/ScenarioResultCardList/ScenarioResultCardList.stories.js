import { storiesOf } from '@storybook/react';
import React from 'react';
import { withReadme } from 'storybook-readme';
import { ScenarioResultCards } from './ScenarioResultCardList';
import docs from './ScenarioResultCardList.md';

const stories = storiesOf('ScenarioResultCardList', module).addDecorator(withReadme(docs));

stories.add('default', _ => <ScenarioResultCardList>Hello</ScenarioResultCardList>);
