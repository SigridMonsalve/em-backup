import React, { ReactDOM, Component } from 'react';
import { connect } from 'react-redux';
import { SearchSlider, HeaderLogo, ICONS, Icon, Header } from '@elliemae/em-dimsum';

class SearchBar extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      searchQuery: '',
      sliderVisible: false,
      initialSpawn: true
    };
    this.timeout = 0;
  }

  onNavItemClick = event => {
    event.preventDefault();
  };

  //handles the restoration of the UI back to original
  toggleSlider = () => {
    this.setState(prevState => ({
      sliderVisible: !prevState.sliderVisible,
      initialSpawn: false
    }));

    this.timeout = setTimeout(() => {
      if (this.state.sliderVisible) {
        var val = document.getElementsByClassName('search-bar-slider-input');
        val[0].focus();
      } else {
        this.props.updateQuery('');
      }
    }, 500);
  };

  searchChangeHandler = event => {
    this.props.updateQuery(event.target.value);
  };

  _handleSearchEnter = e => {
    if (e.key === 'Enter') {
      this.timeout = 0;
      this.props.searchQuery();
    }
  };

  sliderName = () =>
    'slider ' +
    (this.state.sliderVisible && !this.state.initialSpawn
      ? 'openState'
      : !this.state.sliderVisible && this.state.initialSpawn
        ? 'initState'
        : 'closedState');

  render() {
    return (
      <Header>
        {/* <IconContainer /> */}
        <div className="logo-container">
          <div className="searchbar-logo">
            <HeaderLogo bottomtext="Opportunities" sidebarAction={true} />
          </div>
        </div>
        <div className="icon-container">
          <Icon
            icon={ICONS.SORT}
            className="arrow-icon"
            alt="arrow-icon"
            onClick={this.props.openActionsheet}
            width={16}
            height={16}
          />
          <Icon
            icon={ICONS.SEARCH}
            width={16}
            height={16}
            className="search-icon"
            alt="search-icon"
            onClick={this.toggleSlider}
          />
        </div>

        {/* <SliderAnimation />*/}
        <div key={this.state.sliderVisible} className={this.sliderName()}>
          <SearchSlider
            className="slider"
            clickFunction={this.toggleSlider}
            sortDropdown={this.props.showSortOptions}
            buttontext={this.props.buttontext}
            searchChangeHandler={this.searchChangeHandler}
          />
        </div>
      </Header>
    );
  }
}

SearchSlider.propTypes = {};
SearchSlider.defaultProps = {
  buttontext: 'Name'
};

export default SearchBar;
