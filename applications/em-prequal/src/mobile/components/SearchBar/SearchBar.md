Usage:
<SearchBar topText = {} bottomText = {} showActionBar = {this.openActionsheet} searchQuery = {this.searchQuery} showSideBar = {this.showSideBar}> </SearchBar>

Props:
bottomText = Opportunities: bottomText modifies the bottom header text
topText = Loan Scenarios: topText modifies the top header text

prop functions:
openActionSheet triggers action when arrow icon pressed
searchQuery is the action that is triggered when the enter key is pressed or the user enters text and lets it sit for 1000ms
showSidebar is the sidebar handler that occurs when the logo or either of the header texts are clicked
