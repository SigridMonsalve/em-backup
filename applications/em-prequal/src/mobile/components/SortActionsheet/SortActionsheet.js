import React from 'react';
import PropTypes from 'prop-types';
import { Actionsheet, SortList } from '@elliemae/em-dimsum';

const SortActionsheet = ({ title, onClose, name, onChange, onClick, sortOptions, isOpen, order, ...props }) => {
  return (
    <Actionsheet title={title} onClose={onClose} isOpen={isOpen} {...props}>
      <SortList name={name} onChange={onChange} onClick={onClick} sortOptions={sortOptions} order={order} />
    </Actionsheet>
  );
};

SortActionsheet.propTypes = {
  title: PropTypes.string,
  onClose: PropTypes.func,
  name: PropTypes.string.isRequired,
  sortOptions: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
  isOpen: PropTypes.bool,
  arrow: PropTypes.oneOf(['ascending', 'descending'])
};

SortActionsheet.defaultProps = {};

export { SortActionsheet };
