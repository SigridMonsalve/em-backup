import { Button, Icon, ICONS, Modal, Spinner } from '@elliemae/em-dimsum';
import React, { Component, StrictMode } from 'react';
import { connect } from 'react-redux';
import { fetchOpportunity } from '../../../redux/Opportunity/opportunity.actions';
import { fetchOpportunities, fetchMoreOpportunities } from '../../../redux/Opportunities/opportunities.actions';
import { updateGlobalFlags } from '../../../redux/GlobalFlags/globalFlags.actions';
import CreateEditOpportunityModal from '../../components/CreateEditOpportunity/CreateEditOpportunityModal';
import { OpportunitiesList } from './OpportunitiesList';
import EmptyOpportunityList from '../../components/EmptyOpportunityList/EmptyOpportunityList';
import { SortActionsheet } from '../../components/SortActionsheet/SortActionsheet';
import SearchBar from '../../components/SearchBar/SearchBar';
import { throttle } from 'lodash';
import { translate } from 'react-i18next';

const mapStateToProps = state => ({
  opportunities: state.opportunities,
  globalFlags: state.globalFlags
});

const mapDispatchToProps = dispatch => ({
  fetchOpportunity: () => dispatch(fetchOpportunity()),
  fetchOpportunities: params => dispatch(fetchOpportunities(params)),
  fetchMoreOpportunities: params => dispatch(fetchMoreOpportunities(params)),
  updateGlobalFlags: params => dispatch(updateGlobalFlags(params))
});

const LIMIT = 10;

class Opportunities extends Component {
  constructor(props) {
    super(props);
    // TODO: Refactor modal in Redux Modal
    this.state = {
      openAddOpportunityModal: false,
      isSortActionsheetOpen: false,
      isSearchActionsheetOpen: false,
      sortBy: 'dateUpdated',
      searchBy: 'Borrower Last Name',
      searchQuery: '',
      sortAscending: false
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', throttle(this.onScroll, 100), false);
    this.props.updateGlobalFlags({ infiniteScroll: { endOfList: false } });
    this.props.fetchOpportunities();
  }

  componentDidUpdate(prevProps) {
    if (this.props.opportunities.queryOptions != prevProps.opportunities.queryOptions) {
      this.props.updateGlobalFlags({ infiniteScroll: { endOfList: false } });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  openAddOpportunityModal = () => {
    this.setState({
      showOpportunitySpinner: true
    });
    this.props
      .fetchOpportunity()
      .then(res => {
        this.setState({
          showOpportunitySpinner: false,
          openAddOpportunityModal: true
        });
      })
      .catch(err => {
        this.setState({
          showOpportunitySpinner: false
        });
      });
  };

  closeAddOpportunityModal = () => {
    this.setState({ openAddOpportunityModal: false });
  };

  openSortActionsheet = () => {
    this.setState({
      isSortActionsheetOpen: true
    });
  };

  closeSortActionsheet = () => {
    this.setState({
      isSortActionsheetOpen: false
    });
  };

  openSearchActionsheet = () => {
    this.setState({
      isSearchActionsheetOpen: true
    });
  };

  closeSearchActionsheet = () => {
    this.setState({
      isSearchActionsheetOpen: false
    });
  };

  onSortChangeHandler = event => {
    const { queryOptions } = this.props.opportunities;
    const fieldClicked = event.target.value;
    const params = {};

    if (this.state.sortBy === fieldClicked) {
      params.sortAscending = !queryOptions.sortAscending;
    } else if (fieldClicked === 'dateUpdated') {
      params.sortAscending = false;
      this.setState({ sortBy: fieldClicked });
    } else {
      params.sortAscending = true;
      this.setState({ sortBy: fieldClicked });
    }

    if (params.sortAscending) {
      params.sortField = fieldClicked;
    } else {
      params.sortField = `-${fieldClicked}`;
    }

    this.props.fetchOpportunities(params);
    this.timeout = setTimeout(() => {
      this.closeSortActionsheet();
    }, 200);
  };

  onSearchFieldChange = event => {
    this.setState({ searchBy: event.target.value });
    this.timeout = setTimeout(() => {
      this.closeSearchActionsheet();
    }, 200);
  };

  onSearchChangeHandler = query => {
    this.setState({
      searchQuery: query
    });

    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (this.timeout !== 0) {
        const params = {
          searchField: this.state.searchBy.replace(/\s/g, ''),
          searchQuery: this.state.searchQuery
        };
        this.props.fetchOpportunities(params);
      }
    }, 500);
  };

  onScroll = () => {
    const { opportunities, globalFlags } = this.props;
    const { infiniteScroll } = globalFlags;

    if (
      !infiniteScroll.endOfList &&
      opportunities.list.length &&
      window.innerHeight + document.documentElement.scrollTop === document.body.scrollHeight
    ) {
      const params = {
        field: this.state.sortBy,
        sortAscending: opportunities.queryOptions.sortAscending,
        start: opportunities.list.length,
        limit: LIMIT
      };

      this.props.fetchMoreOpportunities(params);
    }
  };

  getSortOptions = () => {
    let sortOpts = [
      { id: 'lastmodified', label: 'Last Modified', value: 'dateUpdated' },
      { id: 'borrowername', label: 'Borrower Full Name', value: 'borrowerFullName' },
      { id: 'loanpurpose', label: 'Loan Purpose', value: 'loanPurpose' },
      { id: 'city', label: 'City', value: 'city' },
      { id: 'state', label: 'State', value: 'state' },
      { id: 'zip', label: 'Zip', value: 'zipCode' }
    ];

    const { sortBy } = this.state;
    sortOpts.forEach(val => {
      if (val.value === sortBy) {
        val.defaultChecked = `true`;
      } else {
        val.defaultChecked = undefined;
      }
    });

    return sortOpts;
  };

  getSearchOptions = () => {
    let searchOpts = [
      { id: 'lastname', label: 'Borrower Last Name', value: 'Borrower Last Name' },
      { id: 'city', label: 'City', value: 'City' },
      { id: 'state', label: 'State', value: 'State' },
      { id: 'zip', label: 'Zip', value: 'Zip' },
      { id: 'loannumber', label: 'Loan Number', value: 'Loan Number' }
    ];

    const { searchBy } = this.state;
    searchOpts.forEach(val => {
      if (val.value === searchBy) {
        val.defaultChecked = `true`;
      } else {
        val.defaultChecked = undefined;
      }
    });

    return searchOpts;
  };

  renderAddOpportunityModal = () => {
    return (
      <CreateEditOpportunityModal
        isOpportunityModalOpen={true}
        closeOpportunityModal={this.closeAddOpportunityModal}
        opportunityId={{}}
        type="create"
      />
    );
  };

  renderSortActionsheet = () => {
    const { queryOptions } = this.props.opportunities;
    return (
      <SortActionsheet
        isOpen="true"
        title="Sort By"
        onClose={this.closeActionsheet}
        name="scenario-sort"
        onClick={this.onSortChangeHandler}
        sortOptions={this.getSortOptions()}
        order={queryOptions.sortAscending ? 'ascending' : 'descending'}
      />
    );
  };

  renderSearchActionsheet = () => {
    return (
      <SortActionsheet
        isOpen="true"
        title="Search By"
        onClose={this.closeSearchActionsheet}
        name="scenario-sort"
        onClick={this.onSearchFieldChange}
        sortOptions={this.getSearchOptions()}
        arrow="ascending"
      />
    );
  };

  renderLoadOpportunitySpinner = t => {
    return <Spinner overlay={true} message={t('FETCH_OPPORTUNITY_SPINNER')} />;
  };

  render() {
    const { opportunities, t } = this.props;

    return (
      <StrictMode>
        <SearchBar
          topText="Loan Scenarios"
          bottomText="Opportunities"
          openActionsheet={this.openSortActionsheet}
          showSortOptions={this.openSearchActionsheet}
          searchQuery={this.state.searchQuery}
          showSideBar={this.showSideBar}
          buttontext={this.state.searchBy}
          updateQuery={this.onSearchChangeHandler}
        />
        {opportunities.hasOwnProperty('list') ? (
          opportunities.list.length ? (
            <OpportunitiesList opportunities={opportunities.list} />
          ) : (
            <EmptyOpportunityList />
          )
        ) : (
          <Spinner overlay={true} message={t('OPPORTUNITIES_SPINNER')} />
        )}
        <Button primary fab onClick={this.openAddOpportunityModal}>
          <Icon icon={ICONS.ADD} width={16} height={16} />
        </Button>

        {this.state.openAddOpportunityModal && this.renderAddOpportunityModal()}
        {this.state.isSortActionsheetOpen && this.renderSortActionsheet()}
        {this.state.isSearchActionsheetOpen && this.renderSearchActionsheet()}
        {this.state.showOpportunitySpinner && this.renderLoadOpportunitySpinner(t)}
      </StrictMode>
    );
  }
}

export default translate('translations')(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Opportunities)
);
