import React from 'react';
import OpportunityCard from './OpportunityListCard/OpportunityListCard';
import { AutoSizer, List, CellMeasurer, CellMeasurerCache } from 'react-virtualized';

const OpportunitiesList = ({ opportunities }) => {
  const cache = new CellMeasurerCache({
    fixedWidth: true,
    defaultHeight: 89
  });
  const renderOpportunityCards = rowData => {
    const opportunityItem = opportunities[rowData.index];
    return (
      <CellMeasurer key={rowData.key} cache={cache} parent={rowData.parent} columnIndex={0} rowIndex={rowData.index}>
        <OpportunityCard opportunity={opportunityItem} />
      </CellMeasurer>
    );
  };

  return (
    <div className="em-ds-opportunity-list">
      <AutoSizer>
        {({ height, width }) => (
          <List
            autoHeight={true}
            rowCount={opportunities.length}
            deferredMeasurementCache={cache}
            rowHeight={cache.rowHeight}
            height={height}
            width={width}
            rowRenderer={renderOpportunityCards}
          />
        )}
      </AutoSizer>
    </div>
  );
};

export { OpportunitiesList };
