import React, { Component, StrictMode } from 'react';
import { ConfirmationModal, Spinner } from '@elliemae/em-dimsum';
import CreateEditOpportunityModal from '../../../components/CreateEditOpportunity/CreateEditOpportunityModal';
import { OpportunityActionSheet } from '../../../components/OpportunityActionSheet/OpportunityActionSheet';
import { OpportunityInfoSection } from '../../../components/OpportunityInfoSection/OpportunityInfoSection';
import { fetchOpportunity } from '../../../../redux/Opportunity/opportunity.actions';
import { timeToDate } from '../../../../utils/format';
import { deleteOpportunity } from '../../../../redux/Opportunities/opportunities.actions';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

const mapStateToProps = state => ({
  opportunityModal: state.opportunityModal
});

const mapDispatchToProps = dispatch => ({
  fetchOpportunity: id => dispatch(fetchOpportunity(id)),
  deleteOpportunity: id => dispatch(deleteOpportunity(id))
});

class OpportunityListCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      routeToDetails: false,
      editOpportunityModal: false,
      isOpportunityActionSheetOpen: false
    };
  }

  toggleOpportunityActionSheet = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      isOpportunityActionSheetOpen: !this.state.isOpportunityActionSheetOpen
    });
  };

  toggleEditOpportunityModal = () => {
    this.setState({
      showOpportunitySpinner: true
    });
    this.props
      .fetchOpportunity(this.props.opportunity.id)
      .then(res => {
        this.setState({
          showOpportunitySpinner: false,
          editOpportunityModal: !this.state.editOpportunityModal
        });
      })
      .catch(err => {
        console.log('error');
      });
  };

  editAction = () => {
    this.setState({
      isOpportunityActionSheetOpen: false
    });
    this.toggleEditOpportunityModal();
  };

  deleteAction = () => {
    this.setState({
      isOpportunityActionSheetOpen: false,
      deleteOpportunityModal: true
    });
  };

  deleteOpportunity = () => {
    this.props
      .deleteOpportunity(this.props.opportunity.id)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log('error');
      })
      .finally(() => {
        this.closeDeleteOpportunityModal();
      });
  };

  closeDeleteOpportunityModal = () => {
    this.setState({
      deleteOpportunityModal: false
    });
  };

  renderEditOpportunityModal() {
    return (
      <CreateEditOpportunityModal
        isOpportunityModalOpen={true}
        closeOpportunityModal={this.toggleEditOpportunityModal}
        title={this.props.opportunityModal.header}
        subHeader={this.props.opportunityModal.subHeader}
        opportunityId={this.props.opportunity.id}
        type="edit"
      />
    );
  }

  renderLoadOpportunitySpinner = t => {
    return <Spinner overlay={true} message={t('FETCH_OPPORTUNITY_SPINNER')} />;
  };

  render() {
    const { t } = this.props;
    const { loanNumber, scenarioCount, dateUpdated } = this.props.opportunity;
    const validLoanNumber = loanNumber && loanNumber !== '';

    return (
      <StrictMode>
        <div className="opportunity-card" onClick={() => this.toggleEditOpportunityModal()}>
          <OpportunityInfoSection
            opportunity={this.props.opportunity}
            showOpportunityActions={true}
            openOpportunityActions={this.toggleOpportunityActionSheet}
          />
          <div className="secondary-info">
            {validLoanNumber ? (
              <div className="info-item loan-number">Loan No: {loanNumber}</div>
            ) : (
              <div className="info-item draft">Draft</div>
            )}
            <div className="info-item date">{timeToDate(dateUpdated)}</div>
          </div>
        </div>

        {this.state.isOpportunityActionSheetOpen && (
          <OpportunityActionSheet
            isOpen={this.state.isOpportunityActionSheetOpen}
            opportunity={this.props.opportunity}
            onClose={this.toggleOpportunityActionSheet}
            editAction={this.editAction}
            deleteAction={this.deleteAction}
          />
        )}

        {this.state.editOpportunityModal && this.renderEditOpportunityModal()}

        {this.state.deleteOpportunityModal && (
          <ConfirmationModal
            type="confirm"
            title="Delete Opportunity"
            isOpen={this.state.deleteOpportunityModal}
            onClose={() => {
              return false;
            }}
            closeButton={{ text: 'Cancel', action: this.closeDeleteOpportunityModal }}
            actionButton={{ text: 'Delete', action: this.deleteOpportunity }}
          >
            Are you sure you want to delete the opportunity?
          </ConfirmationModal>
        )}
        {this.state.showOpportunitySpinner && this.renderLoadOpportunitySpinner(t)}
      </StrictMode>
    );
  }
}

export default translate('translations')(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(OpportunityListCard)
);
