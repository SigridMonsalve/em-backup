import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Spinner } from '@elliemae/em-dimsum';
import { connect } from 'react-redux';
import Opportunities from './Opportunities/Opportunities';
import { Scenarios } from './Scenarios/Scenarios';
import { CompareScenarios } from './Scenarios/CompareScenarios';
import { ScenarioDetails } from './Scenarios/ScenarioDetails';
import { fetchUser } from '../../redux/User/user.actions';
import { fetchSettings } from '../../redux/Settings/settings.actions';

const mapStateToProps = state => ({
  user: state.user,
  setting: state.settings
});

const mapDispatchToProps = dispatch => ({
  fetchUser: () => dispatch(fetchUser()),
  fetchSettings: () => dispatch(fetchSettings())
});

class Routes extends Component {
  componentDidMount() {
    this.props.fetchUser();
    this.props.fetchSettings();
  }

  render() {
    return (
      <Router basename="/em-prequal">
        <Switch>
          <Route exact path="/" component={Opportunities} />
          <Route exact path="/opportunity/:opportunityId/scenarios" component={Scenarios} />
          <Route exact path="/opportunity/:opportunityId/scenarios/compare/" component={CompareScenarios} />
          <Route exact path="/opportunity/:opportunityId/scenario/:scenarioId" component={ScenarioDetails} />
        </Switch>
      </Router>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes);
