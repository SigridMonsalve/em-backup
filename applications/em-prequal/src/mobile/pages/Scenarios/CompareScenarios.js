import React, { Component, StrictMode } from 'react';
import { connect } from 'react-redux';
import { Spinner, Header, Heading, Icon, ICONS, Actionsheet, Label } from '@elliemae/em-dimsum';
import { withRouter } from 'react-router-dom';
import { fetchScenarios } from '../../../redux/Scenarios/scenarios.actions';
import { fetchScenario } from '../../../redux/Scenario/scenario.actions';
import { SortActionsheet } from '../../components/SortActionsheet/SortActionsheet';
import { ScenarioComparisonCardList } from '../../components/ScenarioComparisonCardList/ScenarioComparisonCardList';

const mapStateToProps = state => ({
  scenarios: state.scenarios,
  scenario: state.scenario
});

const mapDispatchToProps = dispatch => ({
  fetchScenarios: data => dispatch(fetchScenarios(data)),
  fetchScenario: (opportunityId, scenarioId) => dispatch(fetchScenario(opportunityId, scenarioId))
});

class CompareScenariosComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSortActionsheetOpen: false,
      isMoreOptionsActionSheetOpen: false
    };
  }

  showScenarioDetails(event, opportunityId, scenarioId) {
    // If there is an error, stay on the page. Otherwise, navigate to scenario details page.
    this.props
      .fetchScenario(opportunityId, scenarioId)
      .then(res => {
        console.log(res);
        this.props.history.push(`/opportunity/${opportunityId}/scenario/${scenarioId}`);
      })
      .catch(err => {
        console.log(err);
      });
  }

  manageActionSheets = (type, status) => {
    if (type === 'sort') {
      this.setState({
        isSortActionsheetOpen: status
      });
    } else {
      this.setState({
        isMoreOptionsActionSheetOpen: status
      });
    }
  };

  componentDidMount() {
    this.props.fetchScenarios();
  }

  onSortChangeHandler = event => {
    this.timeout = setTimeout(() => {
      this.manageActionSheets('sort', false);
    }, 200);
  };

  renderSortActionsheet = () => {
    return (
      <React.Fragment>
        <SortActionsheet
          isOpen={true}
          title="Sort By"
          onClose={() => this.manageActionSheets('sort', false)}
          name="scenario-sort"
          onChange={this.onSortChangeHandler}
          sortOptions={this.getSortOptions()}
        />
      </React.Fragment>
    );
  };

  renderMoreOptionsActionsheet = scenariosLength => {
    return (
      <React.Fragment>
        <Actionsheet
          title={`Scenario Comparison (${scenariosLength})`}
          isOpen={true}
          onClose={() => this.manageActionSheets('moreOptions', false)}
        >
          <div className="option-items-container">
            <div className="option-item">
              <Icon icon={ICONS.MESSAGES} className="item-icon" alt="mail-icon" size={20} />
              <Label>Email</Label>
            </div>
            <div className="option-item">
              <Icon icon={ICONS.SETTINGS} className="item-icon" alt="mail-icon" size={20} />
              <Label>Refine Search Results</Label>
            </div>
          </div>
        </Actionsheet>
      </React.Fragment>
    );
  };

  getSortOptions = () => {
    return [
      { id: 'product-type', label: 'Product Type', value: 'Product Type' },
      { id: 'payment', label: 'Payment', value: 'Payment' },
      { id: 'apr', label: 'APR', value: 'APR' },
      { id: 'term', label: 'Term', value: 'Term' }
    ];
  };

  handleBackNavigation = () => {
    this.props.history.goBack();
  };

  render() {
    // TODO: opportunityId, scenarioId we will get from props when we hook it up with redux
    const { scenarios, scenario } = this.props;
    const { opportunityId } = this.props.match.params;

    const scenariosSelected = Object.keys(scenarios).map(key => {
      return scenarios[key];
    });

    return (
      <div className="page comparison">
        <Header onNavigation={() => this.handleBackNavigation()}>
          <Heading level={3}>{`Scenario Comparison ${
            !scenarios.isLoading ? `(${scenariosSelected.length - 1})` : ''
          }`}</Heading>
          <div className="icon-container">
            <Icon
              icon={ICONS.SORT}
              className="arrow-icon"
              alt="arrow-icon"
              onClick={() => this.manageActionSheets('sort', true)}
              width={16}
              height={16}
            />
            <Icon
              icon={ICONS.MORE_OPTIONS_VERT}
              className="options-icon"
              alt="options-icon"
              onClick={() => this.manageActionSheets('moreOptions', true)}
              size={16}
            />
          </div>
        </Header>
        {scenarios && scenarios.isLoading ? (
          <Spinner />
        ) : (
          <React.Fragment>
            {scenarios && (
              <ScenarioComparisonCardList
                scenarios={scenariosSelected}
                onClick={event => this.showScenarioDetails(event, opportunityId, 0)}
              />
            )}
          </React.Fragment>
        )}
        {this.state.isSortActionsheetOpen && this.renderSortActionsheet()}
        {this.state.isMoreOptionsActionSheetOpen && this.renderMoreOptionsActionsheet(scenariosSelected.length - 1)}
      </div>
    );
  }
}

const CompareScenarios = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CompareScenariosComponent)
);
export { CompareScenarios };
