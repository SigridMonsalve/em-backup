import React, { Component } from 'react';
import { Button, Heading, Rate, Footer } from '@elliemae/em-dimsum';
import { connect } from 'react-redux';
import { fetchCurrentScenario, updateScenario } from '../../../redux/Scenario/scenario.actions';
import { set } from 'lodash';
import { SCENARIO_ERROR } from '../../../redux/Scenarios/scenarios.actions';

const MORTGAGE_INSURANCE_TEXT = 'Mortage Insurance';
const MORTGAGE_INSURANCE_ABBREVIATION = 'MI';
const GET_MI_TEXT = 'Get MI';
const GET_MI_DESCRIPTION = "Press 'Get MI' to update values or manually input adding in monthly payment.";
const LABELS = {
  monthlyMi: 'Monthly MI',
  upfrontMi: 'Upfront MI',
  downPayment: 'Down Payment',
  taxes: 'Taxes',
  insurance: 'Insurance'
};
const UPDATE_SCENARIO_TEXT = 'Update Scenario';
const TAXES_AND_INSURANCE_DESCRIPTION = 'Edit/input values manually';
const TAXES_AND_INSURANCE_TEXT = 'Taxes & Insurance';

const mapStateToProps = state => ({
  scenario: state.scenario
});

const mapDispatchToProps = dispatch => ({
  fetchCurrentScenario: () => dispatch(fetchCurrentScenario()),
  updateScenario: newValue => dispatch(updateScenario(newValue))
});

class EditScenarioComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchCurrentScenario();
  }

  getMI() {
    console.log('get mi');
  }

  handleChange(value, keyPath, type) {
    const scenario = { ...this.props.scenario };
    keyPath && set(scenario, keyPath, type === 'numeric' ? Number(value) : value);
    this.props.updateScenario(scenario);
  }

  render() {
    return (
      <div className="edit-scenario">
        <div className="grid-y grid-frame form-section">
          <div className="cell auto">
            <div className="grid-x grid-margin-x">
              <div className="cell small-12 medium-5">
                <div className="grid-x get-mi-section">
                  <div className="cell small-9 medium-6 get-mi-title">
                    <Heading level={3}>
                      {MORTGAGE_INSURANCE_TEXT.toUpperCase()}
                      <span className="abbreviation">({MORTGAGE_INSURANCE_ABBREVIATION})</span>
                    </Heading>
                  </div>
                  <div className="cell small-3 medium-6 get-mi-button">
                    <Button secondary onClick={event => this.getMI()}>
                      {GET_MI_TEXT}
                    </Button>
                  </div>
                  <div className="cell small-10 medium-12 description-text">{GET_MI_DESCRIPTION}</div>
                  <div className="cell small-12">
                    <div className="form-fields-group">
                      <div className="fields-section">
                        <Rate
                          id="monthly-mi"
                          label={LABELS.monthlyMi}
                          baseAmount="0.00"
                          amount=""
                          onAmountChange={value => this.handleChange()}
                          rate=""
                          onRateChange={value => this.handleChange()}
                        />
                        <Rate
                          id="upfront-mi"
                          label={LABELS.upfrontMi}
                          baseAmount="0.00"
                          amount=""
                          onAmountChange={value => this.handleChange()}
                          rate=""
                          onRateChange={value => this.handleChange()}
                        />
                      </div>
                      <div className="fields-section">
                        <Heading level={3}>{LABELS.downPayment.toUpperCase()}</Heading>
                        <Rate
                          id="down-payment"
                          label={LABELS.downPayment}
                          baseAmount="0.00"
                          amount=""
                          onAmountChange={value => this.handleChange()}
                          rate=""
                          onRateChange={value => this.handleChange()}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="cell small-12 medium-5 section-divider">
                <div className="fields-section">
                  <Heading level={3}>{TAXES_AND_INSURANCE_TEXT.toUpperCase()}</Heading>
                  <div class="description-text">{TAXES_AND_INSURANCE_DESCRIPTION}</div>
                  <Rate
                    id="taxes"
                    label={LABELS.taxes}
                    baseAmount="0.00"
                    amount=""
                    onAmountChange={value => this.handleChange()}
                    rate=""
                    onRateChange={value => this.handleChange()}
                  />
                  <Rate
                    id="insurance"
                    label={LABELS.insurance}
                    baseAmount="0.00"
                    amount=""
                    onAmountChange={value => this.handleChange()}
                    rate=""
                    onRateChange={value => this.handleChange()}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer>
          <Button primary onClick={event => this.updateScenario()}>
            {UPDATE_SCENARIO_TEXT}
          </Button>
        </Footer>
      </div>
    );
  }
}

EditScenarioComponent.propTypes = {};
EditScenarioComponent.defaultProps = {};

const EditScenario = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditScenarioComponent);

export { EditScenario };
