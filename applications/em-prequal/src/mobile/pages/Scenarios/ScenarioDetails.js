import { Button, Modal, Header, Footer } from '@elliemae/em-dimsum';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchCurrentScenario, fetchScenario } from '../../../redux/Scenario/scenario.actions';
import { ScenarioDetailsContent } from '../../components/ScenarioDetailsContent/ScenarioDetailsContent';
import { ScenarioInfo } from '../../components/ScenarioInfo/ScenarioInfo';
import { ScenarioInfoActionSheet } from '../../components/ScenarioInfoActionSheet/ScenarioInfoActionSheet';
import { EditScenario } from './EditScenario';
import { CreateLoanModal } from '../../components/CreateLoanModal/CreateLoanModal';

const EDIT_SCENARIO_TEXT = 'Edit Scenario';
const CREATE_LOAN_TEXT = 'Create Loan';

const mapStateToProps = state => ({
  scenario: state.scenario
});

const mapDispatchToProps = dispatch => ({
  fetchCurrentScenario: () => dispatch(fetchCurrentScenario()),
  fetchScenario: (opportunityId, scenarioId) => dispatch(fetchScenario(opportunityId, scenarioId))
});

class ScenarioDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openEditScenarioModal: false,
      isScenarioInfoOpen: false,
      isOpenCreateLoanModal: false
    };
  }

  componentDidMount() {
    this.props.fetchCurrentScenario();
  }

  editScenario(event, opportunityId, scenarioId) {
    this.setState({
      openEditScenarioModal: true
    });
  }

  closeEditScenarioModal = () => {
    this.setState({
      openEditScenarioModal: false
    });
  };
  handleBackNavigation = () => {
    this.props.history.goBack();
  };

  openScenarioInfoActionsheet = () => {
    this.setState({ isScenarioInfoOpen: true });
  };

  closeScenarioInfoActionsheet = () => {
    this.setState({ isScenarioInfoOpen: false });
  };

  openCreateLoanModal = () => {
    this.setState({ isOpenCreateLoanModal: true });
  };

  closeCreateLoanModal = () => {
    this.setState({ isOpenCreateLoanModal: false });
  };

  handleDeleteAction = () => {
    console.log('Delete action');
    this.closeScenarioInfoActionsheet();
  };

  handleEmailAction = () => {
    console.log('Email action');
    this.closeScenarioInfoActionsheet();
  };

  renderScenarioInfoActionsheet(scenario) {
    return (
      <ScenarioInfoActionSheet
        isOpen={this.state.isScenarioInfoOpen}
        onClose={this.closeScenarioInfoActionsheet}
        contentLabel="Scenario Info"
        header={`CONV | ${scenario.terms.term} ${scenario.terms.loanType}`}
        subheader="5.000 Points • Wells Fargo"
        deleteAction={this.handleDeleteAction}
        emailAction={this.handleEmailAction}
      />
    );
  }

  renderEditScenarioModal() {
    return (
      <Modal
        isOpen={this.state.openEditScenarioModal}
        onClose={this.closeEditScenarioModal}
        closeable
        title={EDIT_SCENARIO_TEXT}
        headerLevel={2}
        customClasses="opportunity-forms-container"
        closeButtonAlign="left"
      >
        <EditScenario />
      </Modal>
    );
  }

  renderCreateLoanModal() {
    return (
      <CreateLoanModal
        isOpen={this.state.isOpenCreateLoanModal}
        closeButtonCallBack={this.closeCreateLoanModal}
        params={this.props.match.params}
        createLoanFrom="scenarioDetails"
      />
    );
  }

  render() {
    const { scenario } = this.props;
    return (
      <div className="page">
        <Header
          customClasses="header"
          onNavigation={this.handleBackNavigation}
          onAction={this.openScenarioInfoActionsheet}
        >
          <ScenarioInfo
            header={`CONV | ${scenario.terms.term} ${scenario.terms.loanType}`}
            subheader="5.000 Points • Wells Fargo"
          />
        </Header>

        <div className="scenario-details container">
          <ScenarioDetailsContent scenario={scenario} />
        </div>
        <Footer customClasses="footer">
          <Button
            secondary
            onClick={(event, opportunityId, scenarioId) => this.editScenario(event, opportunityId, scenarioId)}
          >
            {EDIT_SCENARIO_TEXT}
          </Button>

          <Button primary onClick={this.openCreateLoanModal}>
            {CREATE_LOAN_TEXT}
          </Button>
          {this.state.openEditScenarioModal && this.renderEditScenarioModal()}
          {this.state.isScenarioInfoOpen && this.renderScenarioInfoActionsheet(scenario)}
          {this.state.isOpenCreateLoanModal && this.renderCreateLoanModal()}
        </Footer>
      </div>
    );
  }
}

const ScenarioDetails = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ScenarioDetailsComponent)
);
export { ScenarioDetails };
