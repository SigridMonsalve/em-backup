import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Spinner, Header, Footer, Modal, Heading, Label, Icon, ICONS } from '@elliemae/em-dimsum';
import { withRouter } from 'react-router-dom';
import { createScenarios } from '../../../redux/Scenarios/scenarios.actions';
import { LoanFilter } from '../../components/LoanFilter/LoanFilter';
import { updateOpportunity } from '../../../redux/Opportunity/opportunity.actions';
import { ScenarioResultCardList } from '../../components/ScenarioResultCardList/ScenarioResultCardList';
import { getScenarioResults } from '../../../redux/SearchResults/searchResults.actions';
import { getScenarioSettings } from '../../../redux/ScenarioSettings/scenarioSettings.actions';
import { getResultsScenario } from '../../../redux/ScenarioResults/scenarioResults.actions';
import { SortActionsheet } from '../../components/SortActionsheet/SortActionsheet';
import { eppsSearchParams } from '../../../data/eppsSearchParams';
import { getCalculatedLoanTerm } from '../../../utils/calculatedLoanTerm';
import { fetchSettings } from '../../../redux/Settings/settings.actions';
import { take, groupBy, map, each } from 'lodash';

const mapStateToProps = state => ({
  scenarios: state.scenarios,
  opportunity: state.opportunity,
  // searchResults: state.searchResults,
  scenarioResults: state.scenarioResults,
  scenarioSettings: state.scenarioSettings,
  user: state.user,
  settings: state.settings
});

const mapDispatchToProps = dispatch => ({
  createScenarios: data => dispatch(createScenarios(data)),
  updateOpportunity: opportunity => dispatch(updateOpportunity(opportunity)),
  // getScenarioResults: () => dispatch(getScenarioResults()),
  // fetchSettings: () => dispatch(fetchSettings()),
  getResultsScenario: (pricingSearchParams, clientId) => dispatch(getResultsScenario(pricingSearchParams, clientId)),
  getScenarioSettings: clientId => dispatch(getScenarioSettings(clientId))
});

// TODO: placeholder for scenarios screen
class ScenariosComponent extends Component {
  constructor(props) {
    super(props);
    // TODO: Refactor modal in Redux Modal
    this.state = {
      openModal: false,
      count: 0,
      isSortArrowAscending: true,
      isSortActionsheetOpen: false
    };
    this.calculatedLoanTerm = {};
  }

  renderSortActionsheet = () => {
    return (
      <React.Fragment>
        <SortActionsheet
          isOpen="true"
          title="Sort By"
          onClose={this.closeActionsheet}
          name="scenario-sort"
          onClick={this.onSortChangeHandler}
          sortOptions={this.getSortOptions()}
          order={this.state.isSortArrowAscending ? 'ascending' : 'descending'}
        />
      </React.Fragment>
    );
  };

  getSortOptions = () => {
    let sortOpts = [
      { id: 'monthly-payment', label: 'Monthly Payment', value: 'monthlyPayment' },
      { id: 'rate', label: 'Rate', value: 'rate' },
      { id: 'fees', label: 'Fees', value: 'fees' },
      { id: 'points', label: 'Points', value: 'points' }
    ];
    return sortOpts;
  };

  selectHandler = event => {
    let newCount = this.state.count;
    if (event.target.checked) {
      newCount++;
    } else {
      newCount--;
    }
    this.setState({ count: newCount });
  };
  deselectedHandler = () => {
    this.count--;
  };
  componentDidMount() {
    // const { pricingSearchParams, clientId } = this.props.searchResults;
    // this.props.fetchSettings();
    const clientId = this.props.settings.Value;
    const userName = this.props.user.userId;
    this.formLoanTermsObject();

    // this.props.getScenarioResults();
    // this.props.getResultsScenario(eppsSearchParams, clientId);
    let obj = this.props.getScenarioSettings({ userName, clientId }).then(res => {
      // this.props.getResultsScenario(eppsSearchParams, clientId);
      this.props.getResultsScenario(
        {
          ...eppsSearchParams,
          RepresentativeCreditScore: this.props.opportunity.estimatedCreditScore || '789',
          LoanType: this.props.opportunity.loanInfo.loanTypes,
          LoanDocumentationType: this.props.opportunity.loanInfo.documentType,
          AmortizationType: [`${this.calculatedLoanTerm.AmortizationType}`],
          LoanTerm: [`${this.calculatedLoanTerm.loanTerm}`],
          // TODO: for ARM field we need the value from opportunity form / for fixed times by 12
          LoanPurpose: this.props.opportunity.subjectProperty.loanPurpose,
          PurchasePrice: this.props.opportunity.subjectProperty.purchasePriceAmount,
          EstimatedValue: this.props.opportunity.subjectProperty.estimatedPropertyValue,
          BaseLoanAmount: this.props.opportunity.subjectProperty.loanAmount,
          TotalLoanAmount:
            this.props.opportunity.subjectProperty.loanAmount + this.props.opportunity.subjectProperty.FinancedMI,
          SubjectPropertyState: this.props.opportunity.subjectProperty.state,
          PostalCode: this.props.opportunity.subjectProperty.zipCode,
          PropertyType: this.props.opportunity.loanInfo.propertyType,
          OccupancyType: this.props.opportunity.loanInfo.occupancyType,
          LOCompensationPaidBy: this.props.opportunity.loanInfo.compensation,
          TargetRate: this.props.opportunity.loanInfo.targetRate,
          LienPosition: this.props.opportunity.loanInfo.lienPosition,
          LOCompensationPaidBy: this.props.opportunity.loanInfo.compensation
        },
        clientId
      );
    });

    console.log(obj);
    this.props.getResultsScenario(eppsSearchParams, clientId);
  }

  formLoanTermsObject() {
    each(this.props.opportunity.loanInfo.loanTerms, term => {
      this.calculatedLoanTerm = getCalculatedLoanTerm(term);
    });
  }

  compareScenarios(event, opportunityId) {
    this.setState({
      creatingScenarios: true
    });
    this.props
      .createScenarios({})
      .then(res => {
        this.props.history.push(`/opportunity/${opportunityId}/scenarios/compare/`);
      })
      .catch(err => {
        console.log(err);
      })
      .finally(() => {
        this.setState({
          creatingScenarios: false
        });
      });
  }

  openModal = () => {
    let loanTerms = this.props.opportunity.loanInfo.loanTerms;
    let loanTypes = this.props.opportunity.loanInfo.loanTypes;
    this.loanProducts.map(function(item, index) {
      if (loanTypes.includes(item.value)) {
        item.defaultChecked = true;
      }
    });
    this.loanOptions.map(function(item, index) {
      if (loanTerms.includes(item.value)) {
        item.defaultChecked = true;
      }
    });
    this.setState({
      openModal: true
    });
  };

  closeModal = () => {
    this.setState({ openModal: false });
  };

  loanOptions = [
    { name: '30 Year Fixed', label: '40 Year Fixed', id: '30-Year-Fixed', value: '30 years', defaultChecked: false },
    { name: '30 Year Fixed', label: '30 Year Fixed', id: '30-Year-Fixed', value: '30 years', defaultChecked: false },
    { name: '25 Year Fixed', label: '25 Year Fixed', id: '25-Year-Fixed', value: '25 years', defaultChecked: false },
    { name: '20 Year Fixed', label: '20 Year Fixed', id: '20-Year-Fixed', value: '20 years', defaultChecked: false },
    { name: '15 Year ARM', label: '15 Year ARM', id: '15-Year-ARM', value: '15 year arm', defaultChecked: false },
    { name: '10 Year ARM', label: '10 Year ARM', id: '10-Year-ARM', value: '10 year arm', defaultChecked: false },
    { name: '7 Year ARM', label: '7 Year ARM', id: '7-Year-ARM', value: '7 year arm', defaultChecked: false },
    { name: '5 Year ARM', label: '5 Year ARM', id: '5-Year-ARM', value: '5 year arm', defaultChecked: false },
    { name: '3 Year ARM', label: '3 Year ARM', id: '3-Year-ARM', value: '3 year arm', defaultChecked: false },
    { name: '1 Year ARM', label: '1 Year ARM', id: '1-Year-ARM', value: '1 year arm', defaultChecked: false }
  ];

  loanProducts = [
    { name: 'Conventional', label: 'Conventional', id: 'Conventional', value: 'Conventional' },
    { name: 'FHA', label: 'FHA', id: 'FHA', value: 'FHA' },
    { name: 'VA', label: 'VA', id: 'VA', value: 'VA' },
    { name: 'USDA', label: 'USDA', id: 'USDA', value: 'USDA' }
  ];

  handleBackNavigation() {
    this.props.history.goBack();
  }

  openActionsheet = () => {
    this.setState({
      isSortActionsheetOpen: true
    });
  };

  closeActionsheet = () => {
    this.setState({
      isSortActionsheetOpen: false
    });
  };

  render() {
    // TODO: This opportunityId we will get from props when we hook it up with redux

    const { opportunityId } = this.props.match.params;
    const { scenarios } = this.props;
    // const { searchResults } = this.props.searchResults;
    const { scenarioResults } = this.props.scenarioResults;
    const { loanInfo } = this.props.opportunity;
    let loanTerms = [],
      loanTypes = [];
    if (loanInfo) {
      loanTerms = loanInfo.loanTerms;
      loanTypes = loanInfo.loanTypes;
      //       { loanTerms = [], loanTypes = [] } = loanInfo;
    }

    let { downPaymentPercent } = this.props.opportunity.subjectProperty;
    let purchasePrice = this.props.opportunity.subjectProperty.purchasePriceAmount;
    let borrowers = this.props.opportunity.borrowers;

    if (Object.keys(this.props.scenarioResults).length === 0 || this.props.scenarioResults.showSearchResultsSpinner) {
      return <Spinner overlay={true} message={'Searching for scenario results...'} />;
    }
    console.log(this.props.scenarioResults.LoanPrograms);
    const takesInput = arr => {
      let removed = arr.filter(el => el['@Deleted'] === '0');

      return groupBy(removed, object => {
        return object['@Term'];
      });
    };
    let termsObject = takesInput(this.props.scenarioResults.LoanPrograms);
    console.log(termsObject);

    return (
      <div className="page">
        {/* TODO: Add other functionalities for the page. */}
        {scenarioResults ? (
          <Spinner overlay={true} />
        ) : (
          <React.Fragment>
            <Header customClasses="header" onNavigation={() => this.handleBackNavigation()}>
              <div className="content">
                <div>
                  <Heading level={4}>
                    {borrowers ? borrowers[0].firstName : null} {borrowers ? borrowers[0].lastName : null}
                  </Heading>
                  <Heading level={3}>Scenarios</Heading>
                </div>
                <div>
                  <Icon icon={ICONS.SORT} onClick={this.openActionsheet} />
                  <Icon icon={ICONS.MORE_OPTIONS_VERT} />
                </div>
              </div>
            </Header>
            <div className="em-ds-searchBar" onClick={this.openModal}>
              <label className="em-ds-serachLabel">{loanTypes.join(', ')}</label>
              <label className="em-ds-serachLabel">{loanTerms.join(', ')}</label>
              <label className="em-ds-serachLabel">{downPaymentPercent} % Down Payement</label>
              <label className="em-ds-serachLabel">{purchasePrice} Purchase Price</label>
            </div>
            <Modal
              isOpen={this.state.openModal}
              onClose={this.closeModal}
              closeable
              headerLevel={0}
              customClasses="opportunity-scenario-container"
              closeButtonAlign="left"
              title="Filter"
            >
              <LoanFilter
                loanProducts={this.loanProducts}
                loanOptions={this.loanOptions}
                opportunity={this.props.opportunity}
              />
            </Modal>
            <div className="container">
              {map(termsObject, group =>
                group.map((data, index) => (
                  <ScenarioResultCardList
                    scenarios={data}
                    program={data['@Term']}
                    id={index}
                    onChange={this.selectHandler}
                    lender={data['@Program']}
                    point={data['point']}
                    term={data['@Term']}
                    type={data['type']}
                  />
                ))
              )}
            </div>
            <Footer customClasses="footer">
              <Label>{this.state.count} Scenario Selected</Label>
              <Button primary onClick={event => this.compareScenarios(event, opportunityId)}>
                Compare
              </Button>
            </Footer>

            {this.state.creatingScenarios ? <Spinner overlay={true} message={'Comparing selected scenarios...'} /> : ''}
            {this.state.isSortActionsheetOpen && this.renderSortActionsheet()}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const Scenarios = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ScenariosComponent)
);
export { Scenarios };
