import '../../utils/polyfills';
import './index.scss';
import React from 'react';
import { render } from 'react-dom';
import { I18nextProvider } from 'react-i18next';

import i18n from '../../prequal-i18n';

import App from './App';

const root = document.getElementById('root');
render(
  <I18nextProvider i18n={i18n}>
    <App />
  </I18nextProvider>,
  root
);

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default;
    render(<NextApp />, root);
  });
}
