import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n.use(LanguageDetector).init({
  resources: {
    en: {
      translations: {
        EMPTY_OPPORTUNITIES_LINE1: 'Tap "+" to add a new entry &',
        EMPTY_OPPORTUNITIES_LINE2: 'Enter loan scenario data for your opportunity.',
        OPPORTUNITIES_SPINNER: 'Loading Opportunities...',
        FETCH_OPPORTUNITY_SPINNER: 'Loading opportunity ...',
        CREATE_OPPORTUNITY_SPINNER: 'Saving the opportunity data...',
        SEARCH_RESULTS_SPINNER: 'Searching for scenario results...',
        COMPARE_SCENARIOS_SPINNER: 'Comparing selected scenarios...'
      }
    }
  },
  fallbackLng: 'en',
  debug: true,

  // have a common namespace used around the full app
  ns: ['translations'],
  defaultNS: 'translations',

  keySeparator: false,

  interpolation: {
    formatSeparator: ','
  },

  react: {
    wait: true
  }
});

export default i18n;
