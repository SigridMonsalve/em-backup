export const UPDATE_GLOBAL_FLAGS = 'UPDATE_GLOBAL_FLAGS';

export const updateGlobalFlags = updatedGlobalFlags => dispatch => {
  dispatch({ type: UPDATE_GLOBAL_FLAGS, payload: { ...updatedGlobalFlags } });
};
