import { UPDATE_GLOBAL_FLAGS } from './globalFlags.actions';
import { merge } from 'lodash';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_GLOBAL_FLAGS:
      return merge({ ...state }, action.payload);

    default:
      return state;
  }
};
