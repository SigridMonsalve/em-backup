import { fetch } from '../../utils/api';
import { URLS } from '../../constants';
import { addParamsToURL } from '../../utils/url';
import { updateGlobalFlags } from '../GlobalFlags/globalFlags.actions';

export const INITIATE_FETCH = 'INITIATE_FETCH';
export const FETCH_OPPORTUNITIES_SUCCESS = 'FETCH_OPPORTUNITIES_SUCCESS';
export const FETCH_MORE_OPPORTUNITIES_SUCCESS = 'FETCH_MORE_OPPORTUNITIES_SUCCESS';
export const FETCH_OPPORTUNITIES_ERROR = 'FETCH_OPPORTUNITIES_ERROR';
export const FETCH_MORE_OPPORTUNITIES_ERROR = 'FETCH_MORE_OPPORTUNITIES_ERROR';

export const DELETE_OPPORTUNITY = 'DELETE_OPPORTUNITY';
export const DELETE_OPPORTUNITY_SUCCESS = 'DELETE_OPPORTUNITY_SUCCESS';
export const DELETE_OPPORTUNITY_ERROR = 'DELETE_OPPORTUNITY_ERROR';

const defaultSearchBy = 'borrowerlastname'; // TODO: change to borrower full name when API team is done updating
const defaultSearchQuery = '';
const defaultSortBy = '-dateUpdated';
const defaultSortAscending = false;

export const initiateFetch = () => dispatch => {
  dispatch({
    type: INITIATE_FETCH
  });
};

export const fetchOpportunities = (params = {}) => dispatch => {
  initiateFetch();

  const {
    sortField = defaultSortBy,
    searchField = defaultSearchBy,
    searchQuery = defaultSearchQuery,
    sortAscending = defaultSortAscending
  } = params;
  const urlParams = params === {} ? [] : [{ [searchField]: searchQuery }, { sortBy: sortField }];
  const url = addParamsToURL(URLS.OPPORTUNITIES, urlParams);

  console.log('url', url);

  return fetch(url).then(
    res =>
      dispatch({
        type: FETCH_OPPORTUNITIES_SUCCESS,
        payload: {
          queryOptions: { searchQuery: searchQuery, searchBy: searchField, sortAscending },
          list: res
        }
      }),
    err => dispatch({ type: FETCH_OPPORTUNITIES_ERROR, payload: err })
  );
};

// NOT YET IMPLEMENTED. WILL IMPLEMENT ON NEW BRANCH.
export const fetchMoreOpportunities = ({
  sortField,
  searchField,
  searchQuery,
  sortAscending,
  start,
  limit
}) => dispatch => {
  initiateFetch();

  const urlParams = [{ sortBy: sortField }, { [searchField]: searchQuery }, { start }, { limit }];
  const url = addParamsToURL(URLS.OPPORTUNITIES, urlParams);

  return fetch(url).then(
    res => {
      if (res.length) {
        dispatch({
          type: FETCH_MORE_OPPORTUNITIES_SUCCESS,
          payload: {
            queryOptions: { searchQuery: '', searchBy: defaultSearchBy, sortAscending },
            list: res
          }
        });
      } else {
        dispatch(updateGlobalFlags({ infiniteScroll: { endOfList: true } }));
      }
    },
    err => dispatch({ type: FETCH_MORE_OPPORTUNITIES_ERROR, payload: err })
  );
};

export const deleteOpportunity = id => dispatch => {
  dispatch({ type: DELETE_OPPORTUNITY, payload: {} });
  return fetch(`${URLS.OPPORTUNITY}/${id}`, {
    method: 'DELETE'
  }).then(
    res => {
      console.log(res);
      console.log(DELETE_OPPORTUNITY_SUCCESS);
      dispatch({ type: DELETE_OPPORTUNITY_SUCCESS, payload: {}, id: id });
    },
    err => dispatch({ type: DELETE_OPPORTUNITY_ERROR, payload: err })
  );
};
