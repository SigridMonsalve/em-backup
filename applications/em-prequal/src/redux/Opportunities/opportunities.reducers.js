import {
  INITIATE_FETCH,
  FETCH_OPPORTUNITIES_ERROR,
  FETCH_OPPORTUNITIES_SUCCESS,
  FETCH_MORE_OPPORTUNITIES_SUCCESS,
  FETCH_MORE_OPPORTUNITIES_ERROR,
  DELETE_OPPORTUNITY,
  DELETE_OPPORTUNITY_ERROR,
  DELETE_OPPORTUNITY_SUCCESS
} from './opportunities.actions';

//TODO SET default state
const initialState = {
  queryOptions: {}
};

function opportunities(state = initialState, action) {
  switch (action.type) {
    case (INITIATE_FETCH, DELETE_OPPORTUNITY):
      return state;
    case FETCH_OPPORTUNITIES_SUCCESS:
      return action.payload;
    case FETCH_MORE_OPPORTUNITIES_SUCCESS:
      const newList = [...state.list, ...action.payload.list];
      return { ...state, list: newList };
    case (FETCH_OPPORTUNITIES_ERROR, FETCH_MORE_OPPORTUNITIES_ERROR):
      console.error(action.payload);
      return state; // todo: handle api failure
    case DELETE_OPPORTUNITY_SUCCESS:
      console.log('success');
      const newState = { ...state };
      newState.list = state.list.filter(el => {
        return el.id !== action.id;
      });
      return newState;
    case DELETE_OPPORTUNITY_ERROR:
      console.log('not');
      return state;
    default:
      return state;
  }
}

export default opportunities;
