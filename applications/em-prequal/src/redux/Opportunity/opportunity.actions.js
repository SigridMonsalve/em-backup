import { delay, fetch } from '../../utils/api';
import { URLS } from '../../constants';
import opportunity from './opportunity.reducers';
import { updateGlobalFlags } from '../GlobalFlags/globalFlags.actions';

export const CREATE_OPPORTUNITY = 'CREATE_OPPORTUNITY';
export const CREATE_OPPORTUNITY_SUCCESS = 'CREATE_OPPORTUNITY_SUCCESS';
export const CREATE_OPPORTUNITY_ERROR = 'CREATE_OPPORTUNITY_ERROR';

export const CLEAR_OPPORTUNITY = 'CLEAR_OPPORTUNITY';

export const UPDATE_OPPORTUNITY = 'UPDATE_OPPORTUNITY';

export const FETCH_OPPORTUNITY = 'FETCH_OPPORTUNITY';
export const FETCH_OPPORTUNITY_SUCCESS = 'FETCH_OPPORTUNITY_SUCCESS';
export const FETCH_OPPORTUNITY_ERROR = 'FETCH_OPPORTUNITY_ERROR';

export const SAVE_OPPORTUNITY = 'SAVE_OPPORTUNITY';
export const SAVE_OPPORTUNITY_SUCCESS = 'SAVE_OPPORTUNITY_SUCCESS';
export const SAVE_OPPORTUNITY_ERROR = 'SAVE_OPPORTUNITY_ERROR';

export const clearOpportunity = () => dispatch => {
  dispatch({ type: CLEAR_OPPORTUNITY, payload: {} });
  dispatch(updateGlobalFlags({ opportunityForms: { isDirty: false } }));
};

export const updateOpportunity = updatedOpportunity => dispatch => {
  dispatch({ type: UPDATE_OPPORTUNITY, payload: { ...updatedOpportunity } });
  dispatch(updateGlobalFlags({ opportunityForms: { isDirty: true } }));
};

export const createOpportunity = opportunity => dispatch => {
  dispatch({ type: CREATE_OPPORTUNITY, payload: { ...opportunity, showCreateOpportunitySpinner: true } });
  return fetch(URLS.OPPORTUNITY, {
    body: JSON.stringify(opportunity),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' }
  })
    .then(
      res => dispatch({ type: CREATE_OPPORTUNITY_SUCCESS, payload: { ...res, showCreateOpportunitySpinner: false } }),
      err => dispatch({ type: CREATE_OPPORTUNITY_ERROR, payload: { ...err, showCreateOpportunitySpinner: false } })
    )
    .finally(() => dispatch(updateGlobalFlags({ opportunityForms: { isDirty: false } })));
};

export const saveOpportunity = opportunity => dispatch => {
  dispatch({ type: SAVE_OPPORTUNITY, payload: {} });
  const { id, ...updatedOpportunity } = opportunity;
  return fetch(URLS.OPPORTUNITY + `/${id}`, {
    body: JSON.stringify(updatedOpportunity),
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' }
  })
    .then(
      res => dispatch({ type: SAVE_OPPORTUNITY_SUCCESS, payload: {} }),
      err => dispatch({ type: SAVE_OPPORTUNITY_ERROR, payload: err })
    )
    .finally(() => dispatch(updateGlobalFlags({ opportunityForms: { isDirty: false } })));
};

export const fetchOpportunity = id => dispatch => {
  dispatch({ type: FETCH_OPPORTUNITY, payload: { isLoading: true, uiProperties: {} } });
  const opportunity =
    id === undefined
      ? new Promise((resolve, reject) => {
          resolve({});
        })
      : fetch(`${URLS.OPPORTUNITY}/${id}`);
  return opportunity
    .then(
      res => dispatch({ type: FETCH_OPPORTUNITY_SUCCESS, payload: { ...res } }),
      err => dispatch({ type: FETCH_OPPORTUNITY_ERROR, payload: err })
    )
    .finally(() => dispatch(updateGlobalFlags({ opportunityForms: { isDirty: false } })));
};
