import {
  CREATE_OPPORTUNITY,
  CLEAR_OPPORTUNITY,
  UPDATE_OPPORTUNITY,
  FETCH_OPPORTUNITY,
  FETCH_OPPORTUNITY_ERROR,
  FETCH_OPPORTUNITY_SUCCESS,
  CREATE_OPPORTUNITY_ERROR,
  CREATE_OPPORTUNITY_SUCCESS,
  SAVE_OPPORTUNITY,
  SAVE_OPPORTUNITY_ERROR,
  SAVE_OPPORTUNITY_SUCCESS
} from './opportunity.actions';

function opportunity(state = { subjectProperty: {} }, action) {
  switch (action.type) {
    case CLEAR_OPPORTUNITY:
      return {};
    case UPDATE_OPPORTUNITY:
      return action.payload;

    case FETCH_OPPORTUNITY:
      return { ...state, isLoading: true };
    case FETCH_OPPORTUNITY_SUCCESS:
      return { ...action.payload, isLoading: false };
    case FETCH_OPPORTUNITY_ERROR:
      return state;

    case CREATE_OPPORTUNITY:
      return action.payload;
    case CREATE_OPPORTUNITY_SUCCESS:
      return { ...state, ...action.payload, isLoading: false };
    case CREATE_OPPORTUNITY_ERROR:
      return state;

    case SAVE_OPPORTUNITY:
      return state;
    case SAVE_OPPORTUNITY_SUCCESS:
      return { ...state, isLoading: false };
    case SAVE_OPPORTUNITY_ERROR:
      return state;

    default:
      return state;
  }
}

export default opportunity;
