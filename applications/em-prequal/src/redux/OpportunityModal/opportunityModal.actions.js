export const INIT_OPPORTUNITY_MODAL = 'INIT_OPPORTUNITY_MODAL';
export const UPDATE_OPPORTUNITY_MODAL = 'UPDATE_OPPORTUNITY_MODAL';
export const GET_OPPORTUNITY_MODAL = 'GET_OPPORTUNITY_MODAL';

export const initOpportunityModal = () => dispatch => {
  dispatch({ type: INIT_OPPORTUNITY_MODAL, payload: { header: '', subHeader: '', step: 1 } });
};

export const getOpportunityModal = () => dispatch => {
  dispatch({ type: GET_OPPORTUNITY_MODAL, payload: {} });
};

export const updateOpportunityModal = modalData => dispatch => {
  dispatch({ type: UPDATE_OPPORTUNITY_MODAL, payload: { ...modalData } });
};
