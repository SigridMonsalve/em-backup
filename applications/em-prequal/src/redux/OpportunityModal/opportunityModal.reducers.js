import { INIT_OPPORTUNITY_MODAL, GET_OPPORTUNITY_MODAL, UPDATE_OPPORTUNITY_MODAL } from './opportunityModal.actions';

function opportunityModal(state = {}, action) {
  switch (action.type) {
    case INIT_OPPORTUNITY_MODAL:
      return action.payload;
    case GET_OPPORTUNITY_MODAL:
      return state;
    case UPDATE_OPPORTUNITY_MODAL:
      return action.payload;
    default:
      return state;
  }
}
export default opportunityModal;
