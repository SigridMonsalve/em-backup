import { delay, fetch } from '../../utils/api';
import { URLS } from '../../constants';

export const FETCH_SCENARIO = 'FETCH_SCENARIO';
export const UPDATE_SCENARIO = 'UPDATE_SCENARIO';
export const SCENARIO_SUCCESS = 'SCENARIO_SUCCESS';
export const SCENARIO_ERROR = 'SCENARIO_ERROR';
export const FETCH_CURRENT_SCENARIO = 'FETCH_CURRENT_SCENARIO';

export const fetchScenario = opportunityId => dispatch => {
  dispatch({ type: FETCH_SCENARIO, payload: { isLoading: true } });
  return fetch(URLS.SCENARIO)
    .then(delay())
    .then(
      res => dispatch({ type: SCENARIO_SUCCESS, payload: { ...res, isLoading: false } }),
      err => dispatch({ type: SCENARIO_SUCCESS, payload: err })
    );
};

export const fetchCurrentScenario = () => dispatch => {
  dispatch({ type: FETCH_CURRENT_SCENARIO, payload: {} });
};

export const updateScenario = updatedScenario => dispatch => {
  dispatch({ type: UPDATE_SCENARIO, payload: { ...updatedScenario } });
};
