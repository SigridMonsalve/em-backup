import {
  FETCH_SCENARIO,
  FETCH_CURRENT_SCENARIO,
  UPDATE_SCENARIO,
  SCENARIO_SUCCESS,
  SCENARIO_ERROR
} from './scenario.actions';

function scenario(state = {}, action) {
  switch (action.type) {
    case FETCH_CURRENT_SCENARIO:
      return state;
    case FETCH_SCENARIO:
      return action.payload;
    case UPDATE_SCENARIO:
      return action.payload;
    case SCENARIO_SUCCESS:
      return action.payload;
    case SCENARIO_ERROR:
      return state;
    default:
      return state;
  }
}

export default scenario;
