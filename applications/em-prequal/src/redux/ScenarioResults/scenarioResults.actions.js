import scenarioResults from './scenarioResults.reducers';
import { delay } from '../../utils/api';
import { searchProductRates } from '../../utils/eppsSearch';

export const RESULTS_SCENARIO = 'RESULTS_SCENARIO';
export const RESULTS_SCENARIO_SUCCESS = 'RESULTS_SCENARIO_SUCCESS';
export const RESULTS_SCENARIO_ERROR = 'RESULTS_SCENARIO_ERROR';

export const getResultsScenario = (pricingSearchParams, clientId) => dispatch => {
  dispatch({ type: RESULTS_SCENARIO, payload: { showSearchResultsSpinner: true } });

  return searchProductRates(pricingSearchParams, clientId).then(
    res => dispatch({ type: RESULTS_SCENARIO_SUCCESS, payload: { ...res, showSearchResultsSpinner: false } }),
    err => dispatch({ type: RESULTS_SCENARIO_ERROR, payload: { ...err, showSearchResultsSpinner: false } })
  );
};
