import { RESULTS_SCENARIO, RESULTS_SCENARIO_SUCCESS, RESULTS_SCENARIO_ERROR } from './scenarioResults.actions';

function scenarioResults(state = {}, action) {
  switch (action.type) {
    case RESULTS_SCENARIO:
      return action.payload;
    case RESULTS_SCENARIO_SUCCESS:
      return action.payload;
    case RESULTS_SCENARIO_ERROR:
      return state;

    default:
      return state;
  }
}
export default scenarioResults;
