import scenarioSettings from './scenarioSettings.reducers';
import { delay } from '../../utils/api';
import { getUserSettings } from '../../utils/eppsSearch';

export const SCENARIO_SETTINGS = 'SCENARIO_SETTINGS';
export const SCENARIO_SETTINGS_SUCCESS = 'SCENARIO_SETTINGS_SUCCESS';
export const SCENARIO_SETTINGS_ERROR = 'SCENARIO_SETTINGS_ERROR';

export const getScenarioSettings = payload => dispatch => {
  dispatch({ type: SCENARIO_SETTINGS, payload: { showSearchResultsSpinner: true } });

  return getUserSettings(payload).then(
    res => {
      dispatch({ type: SCENARIO_SETTINGS_SUCCESS, payload: { ...res, showSearchResultsSpinner: false } });
    },
    //username and password is going to store.
    err => dispatch({ type: SCENARIO_SETTINGS_ERROR, payload: { ...err, showSearchResultsSpinner: false } })
  );
};
