import { SCENARIO_SETTINGS, SCENARIO_SETTINGS_SUCCESS, SCENARIO_SETTINGS_ERROR } from './scenarioSettings.actions';

function scenarioSettings(state = {}, action) {
  switch (action.type) {
    case SCENARIO_SETTINGS:
      return action.payload;
    case SCENARIO_SETTINGS_SUCCESS:
      return action.payload;
    case SCENARIO_SETTINGS_ERROR:
      return state;

    default:
      return state;
  }
}
export default scenarioSettings;
