import { delay, fetch } from '../../utils/api';
import { URLS } from '../../constants';

export const CREATE_SCENARIOS = 'CREATE_SCENARIOS';
export const FETCH_SCENARIOS = 'FETCH_SCENARIOS';
export const SCENARIOS_SUCCESS = 'SCENARIOS_SUCCESS';
export const SCENARIOS_ERROR = 'SCENARIOS_ERROR';

export const createScenarios = () => dispatch => {
  dispatch({ type: CREATE_SCENARIOS, payload: { isLoading: true } });
  return fetch(URLS.SCENARIOS)
    .then(delay())
    .then(
      res => dispatch({ type: SCENARIOS_SUCCESS, payload: { ...res, isLoading: false } }),
      err => dispatch({ type: SCENARIOS_SUCCESS, payload: { ...err, isLoading: false } })
    );
};

export const fetchScenarios = opportunityId => dispatch => {
  dispatch({ type: FETCH_SCENARIOS, payload: { isLoading: true } });
  fetch(URLS.SCENARIOS)
    .then(delay())
    .then(
      res => dispatch({ type: SCENARIOS_SUCCESS, payload: { ...res, isLoading: false } }),
      err => dispatch({ type: SCENARIOS_SUCCESS, payload: { ...err, isLoading: false } })
    );
};
