import { CREATE_SCENARIOS, FETCH_SCENARIOS, SCENARIOS_SUCCESS, SCENARIOS_ERROR } from './scenarios.actions';

function scenarios(state = {}, action) {
  switch (action.type) {
    case CREATE_SCENARIOS:
      return action.payload;
    case FETCH_SCENARIOS:
      return action.payload;
    case SCENARIOS_SUCCESS:
      return action.payload;
    case SCENARIOS_ERROR:
      return state;
    default:
      return state;
  }
}

export default scenarios;
