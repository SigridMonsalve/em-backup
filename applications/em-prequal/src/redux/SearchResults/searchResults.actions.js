import { URLS } from '../../constants';
import searchResults from './searchResults.reducers';
import { delay, fetch } from '../../utils/api';

export const NEW_SCENARIO_RESULT = 'NEW_SCENARIO_RESULT';
export const FETCH_SCENARIO_RESULTS_SUCCESS = 'FETCH_SCENARIO_RESULTS_SUCCESS';
export const FETCH_SCENARIO_RESULTS_ERROR = 'FETCH_SCENARIO_RESULTS_ERROR';

export const getScenarioResults = () => dispatch => {
  dispatch({ type: NEW_SCENARIO_RESULT, payload: { showSearchResultsSpinner: true } });
  return fetch(URLS.SCENARIO_RESULTS)
    .then(delay(2000))
    .then(
      res => dispatch({ type: FETCH_SCENARIO_RESULTS_SUCCESS, payload: { ...res, showSearchResultsSpinner: false } }),
      err => dispatch({ type: FETCH_SCENARIO_RESULTS_ERROR, payload: { ...err, showSearchResultsSpinner: false } })
    );
};
