import {
  NEW_SCENARIO_RESULT,
  FETCH_SCENARIO_RESULTS_SUCCESS,
  FETCH_SCENARIO_RESULTS_ERROR
} from './searchResults.actions';

function searchResults(state = {}, action) {
  switch (action.type) {
    case NEW_SCENARIO_RESULT:
      return action.payload;
    case FETCH_SCENARIO_RESULTS_SUCCESS:
      return action.payload;
    case FETCH_SCENARIO_RESULTS_ERROR:
      return state;

    default:
      return state;
  }
}
export default searchResults;
