import { fetch } from '../../utils/api';
import { URLS } from '../../constants.js';

export const FETCH_SETTINGS = 'FETCH_SETTINGS';
export const FETCH_SETTINGS_SUCCESS = 'FETCH_SETTINGS_SUCCESS';
export const FETCH_SETTINGS_ERROR = 'FETCH_SETTINGS_ERROR';

export const fetchSettings = () => dispatch => {
  dispatch({ type: FETCH_SETTINGS, payload: {} });
  return fetch(URLS.SETTINGS).then(
    res => dispatch({ type: FETCH_SETTINGS_SUCCESS, payload: res }),
    err => dispatch({ type: FETCH_SETTINGS_ERROR, payload: err })
  );
};
