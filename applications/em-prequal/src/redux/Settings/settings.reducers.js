import { FETCH_SETTINGS, FETCH_SETTINGS_ERROR, FETCH_SETTINGS_SUCCESS } from './settings.actions';

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_SETTINGS:
      return state;
    case FETCH_SETTINGS_SUCCESS:
      const settings = action.payload.Settings.Setting;
      return settings;
    case FETCH_SETTINGS_ERROR:
      if ((action.payload.status === 502 || action.payload.status === 400) && window.__env.domain === 'LOCALHOST') {
        const settings = { Value: '3010000024' };
        return settings;
      } else {
        return state;
      }
    default:
      return state;
  }
};
