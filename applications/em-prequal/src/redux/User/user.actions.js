import { fetch } from '../../utils/api';

import { URLS } from '../../constants';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR';

export const fetchUser = () => dispatch => {
  dispatch({ type: FETCH_USER, payload: {} });
  return fetch(URLS.USER).then(
    res => dispatch({ type: FETCH_USER_SUCCESS, payload: res }),
    err => dispatch({ type: FETCH_USER_ERROR, payload: err })
  );
};
