import { FETCH_USER, FETCH_USER_ERROR, FETCH_USER_SUCCESS } from './user.actions';

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_USER:
      return state;
    case FETCH_USER_SUCCESS:
      return action.payload;
    case FETCH_USER_ERROR:
      return state;
    default:
      return state;
  }
};
