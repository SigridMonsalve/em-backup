import 'jest-enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

window.__env = {
  apiHost: 'http://localhost:7000'
};
global.window = window;
