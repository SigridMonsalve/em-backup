import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import opportunities from './redux/Opportunities/opportunities.reducers';
import opportunity from './redux/Opportunity/opportunity.reducers';
import opportunityModal from './redux/OpportunityModal/opportunityModal.reducers';
import user from './redux/User/user.reducers';
import settings from './redux/Settings/settings.reducers';
import scenarios from './redux/Scenarios/scenarios.reducers';
import scenario from './redux/Scenario/scenario.reducer';
import searchResults from './redux/SearchResults/searchResults.reducers';
import scenarioResults from './redux/ScenarioResults/scenarioResults.reducers';
import scenarioSettings from './redux/ScenarioSettings/scenarioSettings.reducers';
import globalFlags from './redux/GlobalFlags/globalFlags.reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// TODO: Move combineReducers to redux folder...
const store = createStore(
  combineReducers({
    opportunities,
    opportunity,
    user,
    opportunityModal,
    settings,
    scenarioResults,
    scenarioSettings,
    scenario,
    searchResults,
    globalFlags,
    scenarios
  }),
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
