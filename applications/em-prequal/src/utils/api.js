import { session } from '../utils/storage';
import { logout } from './login';

const addAuth = (options = {}) => {
  const Authorization = session.get('Authorization');
  if (Authorization) {
    options.headers = options.headers || {};
    options.headers['Authorization'] = Authorization;
  }
  return options;
};

const checkStatus = response => {
  const statusCode = response.status;
  if (statusCode === 401) {
    logout(window.__env);
  } else if (statusCode >= 200 && statusCode < 300) {
    const contentType = response.headers.get('content-type');
    if (contentType && contentType.indexOf('application/json') !== -1) {
      return response.json();
    }
    return response;
  } else {
    return Promise.reject(response);
  }
};
export const fetch = (url, options = {}) => window.fetch(url, addAuth(options)).then(checkStatus);

export const delay = (time = 3000) => result => new Promise(resolve => setTimeout(() => resolve(result), time));
