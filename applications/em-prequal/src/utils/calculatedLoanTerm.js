import { take } from 'lodash';

export const getCalculatedLoanTerm = term => {
  let calculatedLoanTerm = {};
  term = term.name || term.text || term;
  if (term) {
    let strArr = term.split(' ');
    if (strArr && strArr.length === 3) {
      let AmortType = strArr[2];
      let loanTerm = convertToMonths(take(strArr, 2).join(' '));
      //  calculatedLoanTerm.loanTerm = loanTerm; //  LoanTerm
      if (AmortType === 'ARM') {
        AmortType = 'AdjustableRate';
        calculatedLoanTerm.ARM1stTerm = loanTerm; //  ARM1stTerm
      } else if (AmortType === 'Fixed') {
        calculatedLoanTerm.loanTerm = loanTerm; //  LoanTerm
        AmortType = 'Fixed';
      }

      calculatedLoanTerm.AmortizationType = AmortType; //  AmortizationType
    }
  }
  return calculatedLoanTerm;
};

const convertToMonths = item => {
  // convert to months if years
  if (item.split(' ')[1] === 'Year') {
    return item.split(' ')[0] * 12;
  } else if (item.split(' ')[1] === 'Month') {
    return +item.split(' ')[0];
  } else {
    // TODO: Handle the condition when LoanTerm does not have a month or year in it
    // ex; HELOC, OPTION ARM
  }
};
