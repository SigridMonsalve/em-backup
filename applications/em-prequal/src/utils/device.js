/**
 * Detect device based on screen size
 */
import { session } from '../utils/storage';
export const checkDevice = () => {
  let device = 'web';
  // look at UA string to determine if its mobile like or tablet like device...
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|Mobile/i.test(window.navigator.userAgent)) {
    // look at device width and height to determine if its tablet or mobile...
    // note: device size is static and does not change when the page is re-sized or rotated.
    // note: device.availWidth and device.availHeight will be device size minus UI task bars.
    if (window.screen.width >= 1024 || window.screen.height >= 1024) {
      device = 'tablet';
    } else {
      device = 'mobile';
    }
  }

  /* Solution for Testing team to force device type */
  let entryPoint = window.location.href;
  let urlDevice = entryPoint.substr(entryPoint.indexOf('=') + 1, entryPoint.length);
  if (urlDevice === 'tablet' || urlDevice === 'mobile' || urlDevice === 'web') {
    device = urlDevice;
    session.set('device', urlDevice);
  } else {
    let localDevice = session.get('device'); // <-- preserve overrides for session to cover page refresh scenario
    if (localDevice === 'tablet' || localDevice === 'mobile' || localDevice === 'web') {
      device = localDevice;
    }
  }
  return device;
};
