import { extend } from 'lodash';
import uuid from 'uuid';
import { resolveTransaction, resolveTransactionUserSettings } from './evpTransaction.js';
import { URLS } from '../constants.js';

// export let productResults = {};

export const searchProductRates = (pricingSearchParams, clientId, username, password) => {
  let evpdata = extend(
    {},
    {
      loanId: '',
      ApplicationID: '_borrower1', //TODO: as out of loan context we are passing borrower1
      searchParameters: pricingSearchParams,
      messageId: uuid(),
      clientId: clientId,
      UserSettings: {
        Username: username || 'ng_EPPS',
        Password: password || 'Pass@word1',
        SavePassword: '0',
        UsePredefined: 'false'
      }
    }
  );
  return resolveTransaction(evpdata, '_productPricing');
};

// let payload = {
//   // username: username,
//   // clientId: clientId
//   //TODO: Get username and clientID from store this.props.user.userId && this.props.settings.value
// };

let defaultPayload = {
  loanId: 'NA',
  vendor: 'eppsSearch',
  osb: URLS.OSB,
  currentApp: 'ENC.ENCW'
};

// {
//   "loanId": "NA",
//   "vendor": "eppsSearch",
//   "osb": "https://int.api.ellielabs.com/vendor/v2/",
//   "currentApp": "ENC.ENCW",
//   "username": "aa01",
//   "clientId": "3010000024",
//   "ApplicationID": "_borrower1",
//   "messageId": "fbc62ef0-99e5-11e8-b0d6-3dd7ed626e66"
// }

export const getUserSettings = payload => {
  let evpdata = extend(payload, defaultPayload, {
    ApplicationID: '_borrower1',
    messageId: uuid()
  });
  return resolveTransactionUserSettings(evpdata, '_userSettings');
};
