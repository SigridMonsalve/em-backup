import { get, flatten, isArray, set, map, find, last, each, isEmpty, extend } from 'lodash';
import { fetch } from './api.js';
import { URLS } from '../constants.js';

let poller = {};
const apiRequestMethod = {};
let functions = {};

const resolveTransaction = (transactionData, apiType = '_userSettings') => {
  if (!transactionData.messageId) {
    throw new Error('Message ID is required parameter');
  }

  let transactionPayload = functions[apiType + 'Request'](transactionData);

  apiRequestMethod[apiType] = get(
    transactionPayload,
    'REQUEST_GROUP.REQUEST.REQUEST_DATA.EMOperations.EMOperation.EMMethod'
  );
  return fetch(URLS.EVP, {
    method: 'POST',
    body: JSON.stringify(transactionPayload),
    headers: {
      'content-type': 'application/json; charset=utf-8'
    }
  })
    .then(response => {
      console.log(response);
      //if userSettings is successful then continue else prompt the user to enter the details take the username and password and call resolveTransaction
      return _transactionResponse(apiType, transactionData, response);
    })
    .catch(_errorHandler);
};

const resolveTransactionUserSettings = (transactionData, apiType = '_userSettings') => {
  if (!transactionData.messageId) {
    throw new Error('Message ID is required parameter');
  }

  let transactionPayload = functions[apiType + 'Request'](transactionData);

  apiRequestMethod[apiType] = get(
    transactionPayload,
    'REQUEST_GROUP.REQUEST.REQUEST_DATA.EMOperations.EMOperation.EMMethod'
  );
  return fetch(URLS.EVP, {
    method: 'POST',
    body: JSON.stringify(transactionPayload),
    headers: {
      'content-type': 'application/json; charset=utf-8'
    }
  })
    .then(response => {
      console.log(response);
      if (response.RESPONSE_GROUP.RESPONSE.STATUS[0]['@_Condition'] === 'Success') {
        console.log(response);

        return _userSettingsResponse(response);
      } else {
        // return _transactionResponse(apiType, transactionData, response);
      }
    })
    .catch(_errorHandler);
};

const _userSettingsResponse = response => {
  let useful = {};

  let membersObject = get(response, 'RESPONSE_GROUP.RESPONSE.RESPONSE_DATA.params.param.value.struct.member');
  ['UserName', 'Password', 'SavePassword', 'BranchID', 'ProviderId', 'SubscriberId', 'ClientID'].map(member => {
    let obj = find(membersObject, { name: member });
    if (typeof obj !== 'undefined') {
      if (isEmpty(obj.value)) {
        useful[obj.name] = '';
      } else {
        useful[obj.name] = obj.value;
      }
    }
  });
  if (typeof useful['SavePassword'] !== 'undefined') {
    useful['SavePassword'] = useful['SavePassword'] !== '0';
  }
  return useful;
};

const _errorHandler = error => {
  console.log('EVP Transaction Service - API Error: ', error);
  let promise = new Promise((resolve, reject) => {
    reject(error);
  });
  return promise;
};

const _transactionResponse = (apiType, transactionData, response) => {
  console.log(arguments);
  return new Promise(async (resolve, reject) => {
    poller[transactionData.messageId] = {
      isProcessingService: false
    };
    /* todo: condition check if res.data.RESPONSE_GROUP.STATUS === Success and handled error conditions properly */
    let transactionResponse = get(response, 'RESPONSE_GROUP.EXTENSION.MESSAGE_GROUP_EXTENSION.TransactionData');
    let pollInterval = apiRequestMethod[apiType] === 'DataMessage' ? 2000 : 5000;
    if (transactionResponse && transactionResponse.TransactionID) {
      // checking if a transaction was created
      let payload = _usefulTransactionResponse(transactionResponse);
      transactionData.vendorEnviroment = payload.vendorEnviroment;
      transactionData.vendorKey = payload.vendorKey;
      console.log('Transaction ID: ', payload.transactionId);
      // if (!poller[transactionData.messageId].isProcessingService)
      //   poller[transactionData.messageId].isProcessingService = false;
      //poller[transactionData.messageId].interval = setInterval(() => {

      let isPending = true;
      while (isPending) {
        try {
          console.log('in while loop');
          let data = await _checkTransactionStatus(transactionData, payload, apiType);
          console.log('resolving', data);
          return resolve(data);
        } catch (e) {
          //console.error('while loop error', e);
          //
          if (
            Array.isArray(e.message) ||
            //(typeof(e) === "string" ? e !== 'pending')
            (!e.message.toLowerCase().includes('pending') && !e.message.toLowerCase().includes('timeout'))
          ) {
            console.error('rejecting', e);
            return reject(e);
          } else {
            console.error('staying in the while loop', e);
          }
        }
        // .then(data => {
        //   resolve(data);
        // })
        // .catch(err => {
        //   console.error(err);
        // });
      }

      //console.log('exited while loop');
      //}, pollInterval);
    } else {
      reject(new Error('Failed creating transaction'));
    }
  });

  //TODO: maybe put this into react.
  // poller[transactionData.messageId] = {};
  // poller[transactionData.messageId].deferred = {
  //   resolve: null,
  //   reject: null
  // };
  // let deferredResponse = new Promise((res, rej) => {
  //   poller[transactionData.messageId].deferred.resolve = data => {
  //     debugger;
  //     res(data);
  //   };
  //   poller[transactionData.messageId].deferred.reject = err => {
  //     debugger;
  //     rej(err);
  //   };
  // });

  /* todo: condition check if res.data.RESPONSE_GROUP.STATUS === Success and handled error conditions properly */
  // let transactionResponse = get(response, 'RESPONSE_GROUP.EXTENSION.MESSAGE_GROUP_EXTENSION.TransactionData');
  // let pollInterval = apiRequestMethod[apiType] === 'DataMessage' ? 2000 : 5000;
  // if (transactionResponse && transactionResponse.TransactionID) {
  //   // checking if a transaction was created
  //   let payload = _usefulTransactionResponse(transactionResponse);
  //   transactionData.vendorEnviroment = payload.vendorEnviroment;
  //   transactionData.vendorKey = payload.vendorKey;
  //   console.log('Transaction ID: ', payload.transactionId);
  //   if (!poller[transactionData.messageId].isProcessingService)
  //     poller[transactionData.messageId].isProcessingService = false;
  //   poller[transactionData.messageId].interval = setInterval(
  //     () => _checkTransactionStatus(transactionData, payload, apiType),
  //     pollInterval
  //   );
  // } else {
  //   poller[transactionData.messageId].deferred.reject('Failed creating transaction');
  // }
  //return poller[transactionData.messageId].deferred;
  // return deferredResponse;
};

const _usefulTransactionResponse = response => {
  return {
    transactionId: response.TransactionID, // 'fb09848f-c7b2-437b-b392-f2314f9b3461',
    clientIdentifier: response.ClientIdentifier,
    companyCode: response.CompanyCode,
    messageId: response.MessageID,
    vendorKey: response.VendorKey,
    vendorEnviroment: response.VendorEnvironment
  };
};

const _checkTransactionStatus = (transactionData, payload, apiType) => {
  return new Promise((resolve, reject) => {
    poller[transactionData.messageId].count = poller[transactionData.messageId].count
      ? poller[transactionData.messageId].count + 1
      : 1;
    // timeout evp transaction after 5 minutes
    if (poller[transactionData.messageId].count > 60) {
      let handle = poller[transactionData.messageId].interval;
      clearInterval(handle);
      handle = 0;
      // poller[transactionData.messageId].isProcessingService = false;
      return reject(new Error('EVP Transaction TimeOut'));
      //throw getEVPError('EVP Transaction TimeOut');
    }

    // will skip service call if isProcessingService is true
    //if (!poller[transactionData.messageId].isProcessingService) {
    poller[transactionData.messageId].isProcessingService = true;
    const sorted = Object.keys(payload)
      .sort()
      .reduce(
        (acc, key) => ({
          ...acc,
          [key]: payload[key]
        }),
        {}
      );
    let queryString = encodeQueryString(sorted);
    let url = URLS.EVP + queryString;
    return fetch(url, {
      params: payload,
      headers: {
        'content-type': 'application/json; charset=utf-8'
      }
    })
      .then(res => {
        processTransactionStatus(apiType, transactionData, res)
          //.bind(this, apiType, transactionData)
          .then(data => {
            resolve(data);
          })
          .catch(err => {
            reject(err);
          });
      })
      .catch(err => {
        console.error(err);
        _errorHandler();
      });
    //} else {
    //  resolve();
    //}
  });
};

const _userSettingsRequest = payload => {
  return {
    REQUEST_GROUP: {
      '@EMVPSVersionID': '1.0',
      '@xmlns': 'http://www.elliemae.com/evp/msgenvelop',
      REQUEST: {
        KEY: [
          {
            '@_Name': 'ApplicationRecordUID',
            '@_Value': '{' + payload.loanId + '}'
          },
          {
            '@_Name': 'SiteID',
            '@_Value': payload.siteId || ''
          }
        ],
        REQUEST_DATA: {
          EMOperations: {
            '@xmlns': 'http://www.elliemae.com/evp/operations',
            EMOperation: {
              EMMethod: 'DataMessage',
              EMAction: 'GetUserSetting'
            }
          },
          params: {
            '@xmlns': 'http://www.elliemae.com/evp/params',
            param: {
              name: 'UserSetting',
              value: 'UserSetting',
              type: 'string'
            }
          }
        }
      },
      EXTENSION: _exentionVendordata(payload)
    }
  };
};

const processTransactionStatus = (apiType, transactionData, res) => {
  return new Promise((resolve, reject) => {
    let status = flatten([get(res, 'RESPONSE_GROUP.RESPONSE.STATUS', '')]);
    let lastedStatus = last(status);
    let condition = lastedStatus['@_Condition'];
    if (status.length === 1) {
      status = status[0];
    }
    let exceptions = _isTransactionError(transactionData, status, condition, apiType, res);
    if (!exceptions) {
      if (typeof condition === 'undefined') {
        let handle = poller[transactionData.messageId].interval;
        clearInterval(handle);
        handle = 0;
        // poller[transactionData.messageId].isProcessingService = false;
        reject('Transaction status returned undefined.');
      } else if (condition.toLowerCase() === 'success') {
        //  resolve promise if this transaction was successful
        let handle = poller[transactionData.messageId].interval;
        clearInterval(handle);
        handle = 0;
        let response = get(res, 'RESPONSE_GROUP.RESPONSE.RESPONSE_DATA');
        console.log('Transaction Response: ', JSON.stringify(response));
        let resolveResponse = functions[apiType + 'Response'](response, transactionData);
        // poller[transactionData.messageId].isProcessingService = false;
        resolve(resolveResponse);

        // resolveResponse
        //   .then(data => {
        //     poller[transactionData.messageId].isProcessingService = false;
        //     resolve(resolveResponse);
        //   })
        //   .catch(err => {
        //     poller[transactionData.messageId].isProcessingService = false;
        //     reject(err);
        //   })

        // passing payload
        // back to
        // response
        // method for subsequent calls
      } else if (condition.toLowerCase() === 'pending') {
        //   // code is moved to _checkTransactionStatus because count should be checked and updated over there
        //   poller[transactionData.messageId].isProcessingService = false;
        reject(new Error(`${transactionData.messageId} is pending`));
      }
    } else {
      //poller[transactionData.messageId].deferred.reject(exceptions);
      poller[transactionData.messageId].isProcessingService = false;
      //throw getEVPError(exceptions, res && res.config);
      reject(getEVPError(exceptions, res && res.config));
    }
  });
};

const encodeQueryString = params => {
  const keys = Object.keys(params);
  return keys.length
    ? '?' + keys.map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key])).join('&')
    : '';
};

const _productPricingResponse = (response, payload) => {
  let LoanPrograms = get(response, 'EPPSLoanPrograms.LoanProgram');
  LoanPrograms = _toArray(LoanPrograms);
  let EPPSLoanID = get(response, 'EPPSLoanPrograms["@LoanID"]');

  each(LoanPrograms, prog => {
    prog.LoanProgamRate = _toArray(prog.LoanProgamRate);
    each(prog.LoanProgamRate, rate => {
      let PriceDollar = payload.searchParameters.BaseLoanAmount * ((parseFloat(rate['@Price']) - 100) / 100);
      rate['PriceDollar'] = Number(PriceDollar) ? Number(PriceDollar).toFixed(2) : '';
      rate['@Rate'] = Number(rate['@Rate']) ? Number(rate['@Rate']).toFixed(3) : '';
      rate['@Price'] = Number(rate['@Price']) ? Number(rate['@Price']).toFixed(3) : '';
      rate['@PnI'] = Number(rate['@PnI']) ? Number(rate['@PnI']).toFixed(2) : '';
    });
  });
  return {
    EPPSLoanID: EPPSLoanID,
    LoanPrograms: LoanPrograms // return all programs to get in-eligible too
  };
};

const _toArray = obj => {
  obj = obj || [];
  return isArray(obj) ? obj : [obj];
};

const _productPricingRequest = payload => {
  payload.vendor = 'eppsSearch';
  // To handle gobal scenario
  payload.searchParameters.NoLoanGUID = payload.loanId ? false : true;
  payload.searchParameters.EPPSLoanID = payload.EPPSLoanID || '';
  payload.loanId = payload.loanId || 'NA';

  let UIInputDataStruct = generateUIDataStruct(payload.searchParameters, 'GetPricing');

  return getPricingRequestPayload(payload, UIInputDataStruct);
};

const getPricingRequestPayload = (payload, UIInputDataStruct) => {
  let settings = _getProductPricingSettings(payload);
  return {
    REQUEST_GROUP: {
      '@EMVPSVersionID': '1.0',
      '@xmlns': 'http://www.elliemae.com/evp/msgenvelop',
      REQUEST: {
        KEY: [
          {
            '@_Name': 'ApplicationRecordUID',
            '@_Value': payload.loanId
          },
          {
            '@_Name': 'SiteID',
            '@_Value': payload.siteId || ''
          }
        ],
        REQUEST_DATA: {
          EMOperations: {
            '@xmlns': 'http://www.elliemae.com/evp/operations',
            EMOperation: {
              EMMethod: 'ServiceMessage',
              EMAction: 'OrderService'
            }
          },
          params: {
            '@xmlns': 'http://www.elliemae.com/evp/params',
            param: [
              {
                name: 'ApplicationID',
                value: payload.ApplicationID,
                type: 'string'
              },
              {
                name: 'UIInputData',
                value: {
                  structs: {
                    name: 'UIInputData',
                    struct: [
                      {
                        name: 'UIInputData',
                        member: UIInputDataStruct
                      }
                    ]
                  }
                },
                type: 'structs'
              },
              ...settings
            ]
          }
        }
      },
      EXTENSION: _exentionVendordata(payload)
    }
  };
};

const _exentionVendordata = payload => {
  let vendors = {
    eppsSearch: {
      VendorKey: 'VPS.PPE.CustomEPPS',
      ServiceMode: payload.ServiceMode || 1,
      CompanyCode: payload.CompanyCode || '20002001'
    },
    obSearch: {
      VendorKey: 'VPS.PPE.CustomOptimalBlue',
      ServiceMode: 1,
      CompanyCode: '20003001'
    },
    obScenariosSave: {
      VendorKey: 'VPS.PPE.CustomOptimalBlue',
      ServiceMode: 1,
      CompanyCode: '20003001'
    },
    productPricingSaveRate: {
      VendorKey: 'VPS.PPE.CustomEPPS',
      ServiceMode: payload.ServiceMode || 1,
      CompanyCode: payload.CompanyCode || '20002001'
    },
    productPricingLockRate: {
      VendorKey: 'VPS.PPE.CustomEPPS',
      ServiceMode: payload.ServiceMode || 1,
      CompanyCode: payload.CompanyCode || '20002001'
    },
    productPricingGetAdjustment: {
      VendorKey: 'VPS.PPE.CustomEPPS',
      ServiceMode: payload.ServiceMode || 1,
      CompanyCode: payload.CompanyCode || '20002001'
    },
    fetchGuidelinesDoc: {
      VendorKey: 'VPS.PPE.CustomEPPS',
      ServiceMode: payload.ServiceMode || 1,
      CompanyCode: payload.CompanyCode || '20002001'
    }
  };
  // validate payload
  if (typeof payload === 'undefined') {
    // return Promise.reject({ error: 'payload is mandatory, got undefined' });
    throw 'payload is mandatory, got undefined';
  } else if (payload) {
    each(['loanId', 'clientId', 'vendor'], field => {
      if (typeof payload[field] === 'undefined') {
        throw `payload.${field} is mandatory, got undefined`;
        // return Promise.reject({ error: `payload.${field} is mandatory, got undefined` });
      } else if (payload[field] === null) {
        throw `payload.${field} is mandatory, got null`;
        // return Promise.reject({ error: `payload.${field} is mandatory, got null` });
      }
    });
  } else {
    throw 'payload is mandatory, got null';
  }
  return {
    MESSAGE_GROUP_EXTENSION: {
      '@xmlns': 'http://www.elliemae.com/evp/extension',
      TransactionData: extend({
        ApplicationRecordUID: payload.loanId,
        ClientIdentifier: payload.clientId,
        CompanyCode:
          vendors[payload.vendor].EMCompanyCode || vendors[payload.vendor].CompanyCode || payload.companyCode || '1601',
        VendorKey: vendors[payload.vendor].VendorKey,
        ServiceMode: vendors[payload.vendor].ServiceMode,
        ConsumerApplication: payload.currentApp || 'ENC.ENCW',
        MessageID: payload.messageId || 'f29bd228-8bed-4e39-9eaa-f5f07d123456',
        UserName: payload.username || 'admin',
        VendorEnvironment: payload.VendorEnvironment || ''
      })
    }
  };
};

const _getProductPricingSettings = payload => {
  let settings = [
    {
      name: 'CompanySetting',
      value: {
        struct: {
          name: 'CompanySetting',
          member: [
            {
              name: 'UsePredefined',
              value: 'False',
              type: 'string'
            }
          ]
        }
      },
      type: 'struct'
    }
  ];

  if (!isEmpty(payload.UserSettings)) {
    let usePredefined = payload.UserSettings.UsePredefined || false;
    // UserSettings Predefined or not
    let userSettings = [
      {
        name: 'UsePredefined',
        value: 'False',
        type: 'string'
      },
      {
        name: 'UserName',
        value: 'ng_EPPS',
        type: 'string'
      },
      {
        name: 'Password',
        value: 'Pass@word1',
        type: 'string'
      }
    ];
    // If not predefined consider app values
    if (!usePredefined) {
      userSettings.push(
        {
          name: 'UserName',
          value: payload.UserSettings.Username,
          type: 'string'
        },
        {
          name: 'Password',
          value: payload.UserSettings.Password,
          type: 'string'
        },
        {
          name: 'SavePassword',
          value: payload.UserSettings.SavePassword === true ? 1 : 0,
          type: 'string'
        }
      );
    }
    userSettings = {
      name: 'UserSetting',
      value: {
        struct: {
          name: 'UserSetting',
          member: userSettings
        }
      },
      type: 'struct'
    };
    settings.push(userSettings);
  }
  return settings;
};

const generateUIDataStruct = (payload, RequestType) => {
  let UIInputDataStruct = [];
  UIInputDataStruct.push({
    name: 'RequestType',
    value: RequestType,
    type: 'string'
  });
  for (var key in payload) {
    if (payload.hasOwnProperty(key)) {
      if (isArray(payload[key])) {
        UIInputDataStruct.push({
          name: key,
          value: {
            list: {
              name: key,
              values: {
                value: payload[key]
              }
            }
          },
          type: 'list'
        });
      } else {
        UIInputDataStruct.push({
          name: key,
          value: typeof payload[key] === 'boolean' ? payload[key] : payload[key] || '',
          type: 'string'
        });
      }
    }
  }
  return UIInputDataStruct;
};

const getEVPError = (exceptions, data) => {
  function EVPError() {
    this.message = this.name = 'EVP Error';
    this.stack = new Error().stack;
  }

  EVPError.prototype = Object.create(Error.prototype);
  EVPError.prototype.constructor = EVPError;

  let evpError = new EVPError();
  evpError.message = exceptions || evpError.message;
  evpError.data = data;
  return evpError;
};

const _isTransactionError = (transactionData, status, condition, apiType, res) => {
  // multiple errors comes as an array of objects
  if (isArray(status) || (condition && condition.toLowerCase() === 'error')) {
    //  reject the promise on error
    let handle = poller[transactionData.messageId].interval;
    clearInterval(handle);
    handle = 0;
    console.log('EVP Transaction Service - Check Transaction Status  - Errors ', JSON.stringify(res));
    // Handling multiple errors as well
    let exceptions = '';
    if (isArray(status)) {
      exceptions = map(status, item => {
        return (item['@_Condition'] || '').toLowerCase() === 'error' ? item['@_Description'] : '';
      });
    } else if (!status['@_Description'] || status['@_Description'] === '') {
      exceptions = status['@_Name']; // TODO: EVP bug, they are fixing it. We should always use Description.
    } else if (status['@_Description']) {
      let duCheckListError = _isUnderwritingChecklistError(res); // this is a special error for order du. Different structure.
      exceptions = duCheckListError ? duCheckListError : status['@_Description'];
    } else {
      exceptions = 'Error - Could not find the error message at this time';
    }
    return exceptions;
  } else {
    return void 0;
  }
};

const _isUnderwritingChecklistError = res => {
  let obj = get(res, 'RESPONSE_GROUP.RESPONSE.RESPONSE_DATA.AUS_RESPONSE.EMBEDDED_FILE');
  let errorMsg = '';
  if (obj) {
    let errorName = obj['@_Name'];
    if (errorName === 'MP_STATUS_LOG') {
      errorMsg = get(res, 'RESPONSE_GROUP.RESPONSE.STATUS[@_Description]', 'Submitted Loan has Errors');
      return [errorMsg, obj['#text']];
    } else if (errorName === 'Error_Log') {
      errorMsg = 'Underwriting Checklist Error. See Checklist Log for details';
      return [errorMsg, obj['DOCUMENT']];
    }
  }
  return false;
};

// const _userSettingsResponse = response => {
//   let useful = {};
//   let membersObject = get(response, 'params.param.value.struct.member');
//   ['UserName', 'Password', 'SavePassword', 'BranchID', 'ProviderId', 'SubscriberId', 'ClientID'].map(member => {
//     let obj = find(membersObject, { name: member });
//     if (typeof obj !== 'undefined') {
//       if (isEmpty(obj.value)) {
//         useful[obj.name] = '';
//       } else {
//         useful[obj.name] = obj.value;
//       }
//     }
//   });
//   if (typeof useful['SavePassword'] !== 'undefined') {
//     useful['SavePassword'] = useful['SavePassword'] !== '0';
//   }
//   return useful;
// };

// const _obCreatedTransactionStatusRequest = transaction => {
//   let vendorConfig = this._exentionVendordata({
//     loanId: 'NA',
//     clientId: transaction.ClientIdentifier,
//     vendor: 'obSearch'
//   });
//
//   transaction.CompanyCode = get(vendorConfig, 'MESSAGE_GROUP_EXTENSION.TransactionData.CompanyCode');
//   transaction.VendorKey = get(vendorConfig, 'MESSAGE_GROUP_EXTENSION.TransactionData.VendorKey');
//
//   let transactionResponse = set({ transaction }, 'RESPONSE_GROUP.EXTENSION.MESSAGE_GROUP_EXTENSION', {
//     TransactionData: transaction
//   });
//   return this._transactionResponse('_obCreateTransactionStatus', transaction, transactionResponse);
// };

functions = {
  resolveTransaction,
  _productPricingRequest,
  _productPricingResponse,
  _userSettingsRequest,
  resolveTransactionUserSettings
};
export { resolveTransaction, resolveTransactionUserSettings };
