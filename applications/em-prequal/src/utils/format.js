export const timeToDate = timestamp => {
  const date = new Date(timestamp);
  const formattedDate = `${date.getMonth() + 1}` + '/' + date.getDate() + '/' + date.getFullYear();
  return formattedDate;
};

export const removeSpace = string => {
  return string.replace(/\s/g, '');
};
