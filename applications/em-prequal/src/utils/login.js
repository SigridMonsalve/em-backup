//login.js

import { session } from '../utils/storage';
/**
 * Static Client ID for IDP Authentication. It seems this is common for all applications as of now.
 * If it's not common for all applications, we should accept this as a parameter to login
 */
const client_id = 'tirykwfo';

/**
 * A redirect URL used for IDP to understand where it needs to redirect back after auth flow is complete
 */
const redirect_uri = window.location.protocol + '//' + window.location.host + window.location.pathname;

/**
 * Token URL used for making a token call in auth flow
 * @param {string} apiHost - base API URL to construct token URL
 */
const tokenUrl = ({ apiHost }) => apiHost + '/oauth2/v1/token';

/**
 * Introspectino URL used for 2nd shake in 2 way handshake process
 * @param {string} apiHost - base API URL to construct the introspection URL
 */
const introspectionUrl = ({ apiHost }) => tokenUrl({ apiHost }) + '/introspection';

/**
 * Logout URL used to call API to revoke current token. This is a security best practive to explicitly
 * invalidate an issued token to minimize it's misuse.
 * @param {string} apiHost - base API URL to construct the logout URL
 */
const logoutUrl = ({ apiHost }) => tokenUrl({ apiHost }) + '/revocation';

/**
 * Create a browser safe URL from a JS Object. The encodeURIComponent ensures all special case
 * caracters are properly escaped/encoded
 * @param {Object} data an object with values needed in URL
 * @return {String} an escaped/encoded URL
 * @example
 * createUrl({abc: 'def', jhi: 'jkl'}) => 'abc=def&jhi=jkl'
 */
const createUrl = data =>
  Object.keys(data)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(data[k]))
    .join('&');

/**
 * Obtain a value from current browsers search parameters
 * @param {string} key - a key to search in search parameters
 * @return {String} value obtained from URLSearchParams
 */
const valueFromUrl = key => new URLSearchParams(window.location.search).get(key);

/**
 * Redirect to IDP for authorization...
 * @param {string} idpHost - IDP Base URL to construct the final URL to redirect to
 * @return {Promise} returns a promise rejection so that application can handle error sequence
 */
const redirectToIdp = ({ idpHost }) => {
  const query =
    'authorize?' +
    createUrl({
      client_id,
      redirect_uri,
      response_type: 'code',
      scope: 'loc'
    });
  window.location = `${idpHost}/${query}`;
  return Promise.reject('NOT AUTHENTICATED');
};

/**
 * Logout Sequence ==> Clear session storage ==> call IDP revoke API ==> redirect to initial/default location
 * @todo Currently revoke API returns a 401. Discuss with OAPI team on possible resolution
 * @param {String} apiHost The API URL to use to call revoke API
 * @return {Promise}
 * @public
 */
const logout = ({ apiHost }) => {
  const Authorization = session.get('Authorization');
  return window
    .fetch(logoutUrl({ apiHost }), {
      method: 'POST',
      body: createUrl({ token: Authorization ? Authorization.split(' ')[1] : '' }),
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      }
    })
    .finally(() => {
      session.clear();
      window.location = redirect_uri;
    });
};

/**
 * Handle the introspection API response
 * @param {object} response the json response returned by introspection API
 */
const introspectionResponse = response => {
  // store the credentials in session storage
  session.set('cred', {
    BearerToken: response.bearer_token,
    Realm: response.encompass_instance_id,
    UserName: response.user_name,
    SessionId: response.bearer_token.replace(response.encompass_instance_id + '_', '')
  });

  // HTML5 history api saves a browser reload vs redirect causes reload. Use if available, otherwise a browser redirect
  if (window.history.replaceState) {
    window.history.replaceState(null, document.title, redirect_uri);
  } else {
    window.location = redirect_uri;
  }
  return response;
};

/**
 * Utility method to convert response from fetch to JSON
 * @param {Object} response object to be concerted to JSON
 */
const toJson = response => response.json();

/**
 * The infamous login flow.
 * @param {String} apiHost the apiHost to hit. Accepted from hosting application since it might change
 *  based on environment
 * @param {String} idpHost the idpHost to hit. Again accepted from hosting application since it might
 * change based on environment
 * @return {Promise} Either fulfills the login request or rejects with failure
 * @public
 */
const login = ({ apiHost, idpHost }) => {
  const Authorization = session.get('Authorization');
  if (Authorization) {
    return Promise.resolve(Authorization);
  } else {
    if (valueFromUrl('code') === null) {
      return redirectToIdp({ idpHost });
    } else {
      const formBody = createUrl({
        grant_type: 'authorization_code',
        client_id,
        redirect_uri,
        code: valueFromUrl('code')
      });

      return window
        .fetch(tokenUrl({ apiHost }), {
          method: 'POST',
          headers: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          body: formBody
        })
        .then(response => {
          if (response.status !== 200) {
            return logout({ apiHost });
          }
          return response;
        })
        .then(toJson)
        .then(response => {
          if (response && response.token_type && response.access_token) {
            const Authorization = response.token_type + ' ' + response.access_token;
            session.set('Authorization', Authorization);
            return window
              .fetch(introspectionUrl({ apiHost }), {
                method: 'POST',
                body: createUrl({
                  client_id,
                  token: response.access_token
                }),
                headers: {
                  'content-type': 'application/x-www-form-urlencoded'
                }
              })
              .then(toJson)
              .then(introspectionResponse)
              .then(_ => Authorization);
          } else {
            return logout({ apiHost });
          }
        });
    }
  }
};

export { login, logout };
