/**
 * Note: below polyfills are already added by create-react-app
 *
 * # Promise
 * promise/lib/rejection-tracking
 * window.Promise = require('promise/lib/es6-extensions.js');
 *
 * # Fetch
 * require('whatwg-fetch');
 *
 * # Object Assign
 * Object.assign = require('object-assign');
 *
 * Please see reference here: https://github.com/facebook/create-react-app/blob/next/packages/react-scripts/config/polyfills.js
 */

const svgClassList = () => {
  if (!('classList' in SVGElement.prototype)) {
    Object.defineProperty(SVGElement.prototype, 'classList', {
      get() {
        return {
          contains: className => {
            return this.className.baseVal.split(' ').indexOf(className) !== -1;
          }
        };
      }
    });
  }
};
svgClassList();
