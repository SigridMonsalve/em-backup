export const isDeleted = data => {
  return data['@Deleted'] !== '0';
};
