const prefix = 'ngStorage-';

const local = {
  get: key => JSON.parse(localStorage.getItem(prefix + key)),
  set: (key, value) => localStorage.setItem(prefix + key, JSON.stringify(value)),
  clear: () => localStorage.clear()
};

const session = {
  get: key => JSON.parse(sessionStorage.getItem(prefix + key)),
  set: (key, value) => {
    return sessionStorage.setItem(prefix + key, JSON.stringify(value));
  },
  clear: () => sessionStorage.clear()
};

export { local, session };
