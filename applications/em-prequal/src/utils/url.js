export const addParamsToURL = (url, params) => {
  let isFirstParam = true;

  Array.isArray(params) &&
    params.forEach(el => {
      let key = Object.keys(el);
      let value = key ? el[key] : undefined;

      if (value && isFirstParam) {
        url += `?${key}=${value}`;
        isFirstParam = false;
      } else if (value && !isFirstParam) {
        url += `&${key}=${value}`;
      }
    });

  return url;
};
