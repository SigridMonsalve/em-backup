import React, { Component } from 'react';
import { ICONS, Icon } from '@elliemae/em-dimsum';

class Greeting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false
    };
  }
  openMenu = () => {
    this.setState(prevState => ({ isActive: !prevState.isActive }));
    this.props.handleClick();
  };
  render() {
    return (
      <div className="greeting" onClick={this.openMenu}>
        <span>
          Hi {this.props.username}
          <Icon
            icon={this.state.isActive ? ICONS.ARROWHEAD_UP : ICONS.ARROWHEAD_DOWN}
            className="arrow-icon"
            alt="arrow-icon"
            width={16}
            height={16}
          />
        </span>
      </div>
    );
  }
}

export default Greeting;
