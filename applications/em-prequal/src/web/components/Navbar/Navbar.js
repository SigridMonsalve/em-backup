import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { HeaderLogo, Button, Modal, About } from '@elliemae/em-dimsum';
import { EMResources } from '../../../data/emResources';
import Notifications from './Notifications';
import Greeting from './Greeting';

const mapStateToProps = state => ({
  user: state.user
});

class NavbarComponent extends Component {
  static paths = ['pipeline', 'opportunities'];
  constructor(props) {
    super(props);
    this.state = {
      aboutModalOpen: false
    };
  }
  onNotificationsClick = event => {
    event.preventDefault();
  };
  onUserClick = () => {
    this.setState(prevState => ({ aboutModalOpen: !prevState.aboutModalOpen }));
  };
  onAboutClick = event => {
    event.preventDefault();
    this.setState(prevState => ({ aboutModalOpen: !prevState.aboutModalOpen }));
  };
  changeLocation = path => {
    this.props.history.push(`/${path}`);
  };
  getUserInfo = () => {
    const { company, encompassVersion, loBuild, userId, clientId } = this.props.user;
    const info = [
      { name: 'Company', value: company || '', id: 0 },
      { name: 'Build', value: encompassVersion || '', id: 1 },
      { name: 'LO Connect Build', value: loBuild || '', id: 2 },
      { name: 'Client ID', value: clientId || '', id: 3 },
      { name: 'User', value: userId || '', id: 4 }
    ];
    return info;
  };
  renderFooter = () => '';
  renderAboutModal = () => (
    <Modal
      title="About Encompass"
      closeable
      onClose={this.onAboutClick}
      isOpen={this.state.aboutModalOpen}
      hasFooter={true}
      headerLevel={2}
      closeButtonAlign="right"
      footerContent={this.renderFooter}
    >
      <About resources={EMResources} info={this.getUserInfo()} showLogo={false} />
    </Modal>
  );
  render() {
    return (
      <div className="navbar">
        <div className="logo-container">
          <HeaderLogo bottomtext="Encompass" />
          <div className="logo-side-text">
            <span>®</span>
            <span>by </span>
            <span>EllieMae</span>
          </div>
        </div>
        <div className="nav-container">
          {NavbarComponent.paths.map((item, index) => (
            <Button key={index} text>
              <Link to={`/${item}`}>{item}</Link>
            </Button>
          ))}
        </div>
        <div className="info-container">
          <Notifications amount={this.props.alertsNumber} handleClick={this.onNotificationsClick} />
          <Greeting username={this.props.username} handleClick={this.onUserClick} />
        </div>
        {this.state.aboutModalOpen && this.renderAboutModal()}
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(NavbarComponent));
