import React from 'react';
import { ICONS, Icon } from '@elliemae/em-dimsum';

const Notifications = props => (
  <div className="notification-container" onClick={props.handleClick}>
    {props.amount > 0 ? (
      <div className="notification-bubble">
        <span>{props.amount}</span>
      </div>
    ) : null}
    <Icon icon={ICONS.INBOX} className="notification-icon" alt="inbox-icon" width={21} height={21} />
  </div>
);

export default Notifications;
