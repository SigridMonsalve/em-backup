import React from 'react';
import { Button, Card, PieGraph, Heading, Icon, ICONS } from '@elliemae/em-dimsum';

const ScenarioComparisonCard = ({
  scenario,
  clickCard,
  moreOptions,
  messageClick,
  primaryClick,
  secondaryClick,
  ...props
}) => {
  const COLORS = ['#FF9400', '#32B87C', '#52A6EC', '#006AA9'];
  const d = new Date(scenario.dateCreated); //used to format date
  const date = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;

  const generalInfo = [
    { name: 'Rate', value: `${scenario.terms ? scenario.terms.noteRate.toFixed(3) : ''} %` },
    { name: 'APR', value: `${scenario.terms ? scenario.terms.noteRate.toFixed(3) : ''} %` },
    { name: 'Fees', value: `$ ${scenario.fee[0] ? scenario.fee[0].fixedFee : ''}` }
  ];
  const graphData = [
    { name: 'P&I', value: scenario.terms ? scenario.terms.annualInsuranceAmount : '' },
    { name: 'Taxes', value: scenario.terms ? scenario.terms.annualTaxesAmount : '' },
    { name: 'Insurance', value: scenario.terms ? scenario.terms.annualInsuranceAmount : '' },
    { name: 'HOA', value: scenario.terms ? scenario.terms.annualTaxesAmount : '' }
    //mockup data to be defined with API integration
  ];

  const graphText = `$ ${scenario.fee[0].totalAmount.toFixed(2)}`;

  const generalInfoItems = generalInfo.map(item => {
    return (
      <div className="general-info" key={item.name}>
        <div className="general-info-title">{item.name}</div>
        <div className="general-info-content">{item.value}</div>
      </div>
    );
  });

  const chartData = graphData.map((item, index) => {
    const bulletColor = {
      color: COLORS[index]
    };
    return (
      <li style={bulletColor} key={item.name}>
        <div className="data-title">{item.name}</div>
        <div className="data-content">${item.value.toFixed(2)}</div>
      </li>
    );
  });

  return (
    <div className="scenario-card" onClick={clickCard}>
      <Card>
        <div className="em-ds-card-header">
          <div className="top-section-header">
            <div className="em-ds-card-heading">
              <Heading level={2}>CONV</Heading>
              <Heading level={5}>{`${scenario.terms ? scenario.terms.term : ''}  ${
                scenario.terms ? scenario.terms.loanType : ''
              }`}</Heading>
            </div>
            <Icon className="em-ds-card-icon" icon={ICONS.MESSAGES} size={20} />
            <Icon className="em-ds-card-icon" icon={ICONS.MORE_OPTIONS_VERT} size={20} />
          </div>
          <div className="subtitle-info-card"> 5.000 Points &bull; Wells Fargo &bull; {date}</div>
        </div>
        <hr className="bottom-line" />
        <div className="em-ds-top-data">{generalInfoItems}</div>
        <div className="info-text">
          <div className="chart-info-title">Monthly Payment Breakdown</div>
          <div className="chart-info-subtitle">
            Down Payment {scenario.terms ? scenario.terms.downPaymentPercent : ''}% / ${scenario.terms
              ? scenario.terms.loanAmount
              : ''}
          </div>
        </div>
        <div className="data-container">
          <div className="graph-container">
            <PieGraph colors={COLORS} data={graphData} titleTextChart="MONTHLY" valueTextChart={graphText} />
          </div>
          <div className="info-container">{chartData && <ul>{chartData}</ul>}</div>
        </div>
        <hr className="bottom-line" />
        <div className="card-footer">
          <div className="em-ds-buttons-container">
            <Button secondary onClick={secondaryClick}>
              Edit Details
            </Button>
          </div>
          <div className="em-ds-buttons-container">
            <Button primary onClick={primaryClick}>
              Create Loan
            </Button>
          </div>
        </div>
      </Card>
    </div>
  );
};

export { ScenarioComparisonCard };
