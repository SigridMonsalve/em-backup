import React from 'react';
import { ScenarioComparisonCard } from './ScenarioComparisonCard/ScenarioComparisonCard';

const ScenarioComparisonCardList = ({ scenarios, ...props }) => {
  return (
    scenarios && (
      <div className="scenario-card-list">
        {scenarios.map((scenario, idx) => {
          let id = `sc${idx}`; //scenario item doesn't have an id in mockup data
          return scenario && <ScenarioComparisonCard scenario={scenario} clickCard={props.onClick} key={id} />;
        })}
      </div>
    )
  );
};

ScenarioComparisonCardList.propTypes = {};
ScenarioComparisonCardList.defaultProps = {};

export { ScenarioComparisonCardList };
