import React, { Component } from 'react';
import { connect } from 'react-redux';
import NumberFormat from 'react-number-format';

import { Icon, ICONS } from '@elliemae/em-dimsum';

const SummaryBarItem = ({ name, data }, idx) => {
  return (
    <div key={idx} className="summary-bar-item">
      <div className="summary-bar-data">{data}</div>
      <div className="summary-bar-name">{name}</div>
    </div>
  );
};

const mapStateToProps = state => ({
  borrower: state.opportunity.borrowers[0],
  loan: state.opportunity.subjectProperty
});

class SummaryBarComponent extends Component {
  formatPhoneNumber(phone) {
    return <NumberFormat value={phone} displayType={'text'} format="###-###-####" />;
  }

  fields() {
    const opportunityName = this.props.borrower.firstName;
    const email = this.props.borrower.emailAddressText;
    const { loanPurpose, city, state, zipCode } = this.props.loan;
    const mobilePhone = this.formatPhoneNumber(this.props.borrower.mobilePhone);

    return [
      { name: 'Opportunity Name', data: opportunityName || '-' },
      { name: 'Email', data: email || '-' },
      { name: 'Mobile Phone', data: mobilePhone || '-' },
      { name: 'Loan Purpose', data: loanPurpose || '-' },
      { name: 'City', data: city || '-' },
      { name: 'State', data: state || '-' },
      { name: 'Zip Code', data: zipCode || '-' }
    ];
  }

  handleClick = e => {
    console.log('hi');
  };

  render() {
    const fields = this.fields();
    console.log(fields);

    return (
      <div className="summary-bar">
        <div className="summary-bar-fields">{fields.map((f, i) => SummaryBarItem(f, i))}</div>
        <Icon icon={ICONS.MORE_OPTIONS_VERT} width={28} height={28} onClick={this.handleClick} />
      </div>
    );
  }
}

const SummaryBar = connect(mapStateToProps)(SummaryBarComponent);

export { SummaryBar };
