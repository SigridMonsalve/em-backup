import '@elliemae/em-dimsum/dist/em-dimsum.css';
import React from 'react';
import { Provider } from 'react-redux';
import store from '../../store';
import Routes from './Routes';

const App = ({ children, ...props }) => {
  return (
    <Provider className="em-prequal" store={store}>
      <Routes />
    </Provider>
  );
};

export default App;
