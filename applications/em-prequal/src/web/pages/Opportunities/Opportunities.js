import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import { SearchBar, Popover, SortList } from '@elliemae/em-dimsum';
import { CompareScenarios } from '../Scenarios/CompareScenarios';

import Navbar from '../../components/Navbar/Navbar';
import OpportunitiesList from './OpportunitiesList';
import './Opportunities.scss';

class Opportunities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBarClick: null,
      popoverOpen: false
    };
  }
  showPopover = event => {
    this.setState({ searchBarClick: event.target, popoverOpen: true });
  };
  popoverHandler = () => {
    this.setState({ searchBarClick: null, popoverOpen: false });
  };
  render() {
    return (
      <div className="opportunities-view">
        <Navbar alertsNumber={14} username={'Sigrid'} />
        <div className="content">
          <div className="header">
            <h3>Opportunities</h3>
            <SearchBar showSortOptions={this.showPopover} />
          </div>
          <Router>
            <Switch>
              {/* <Route exact path="/" component={OpportunitiesList} /> */}
              <Route exact path="/em-prequal/opportunities" component={OpportunitiesList} />
              <Route
                exact
                path="/em-prequal/opportunity/:opportunityId/scenarios/compare/"
                component={CompareScenarios}
              />
            </Switch>
          </Router>
        </div>
        {this.state.popoverOpen && (
          <Popover isOpen={true} onClose={this.popoverHandler} target={this.state.searchBarClick}>
            <SortList
              sortOptions={[
                { id: 'fullname', label: 'Name', value: 'Name' },
                { id: 'city', label: 'City', value: 'City' },
                { id: 'state', label: 'State', value: 'State' },
                { id: 'zip', label: 'Zip', value: 'Zip' },
                { id: 'loannumber', label: 'Loan Number', value: 'Loan Number' }
              ]}
            />
          </Popover>
        )}
      </div>
    );
  }
}

export { Opportunities };
