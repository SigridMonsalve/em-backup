import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { fetchOpportunity } from '../../../redux/Opportunity/opportunity.actions';
import { fetchOpportunities } from '../../../redux/Opportunities/opportunities.actions';
import { throttle } from 'lodash';
import { DataGrid, Popover, Icon, ICONS } from '@elliemae/em-dimsum';

import './OpportunitiesList.scss';

const mapStateToProps = state => ({
  opportunities: state.opportunities
});

const mapDispatchToProps = dispatch => ({
  fetchOpportunity: () => dispatch(fetchOpportunity()),
  fetchOpportunities: params => dispatch(fetchOpportunities(params))
});

class OpportunitiesList extends Component {
  static dataHeaders = {
    name: 'NAME',
    purpose: 'PURPOSE',
    city: 'CITY',
    state: 'STATE',
    zip: 'ZIP',
    loanNumber: 'LOAN NUMBER',
    lastModified: 'LAST MODIFIED'
  };
  static dataColumns = ['name', 'purpose', 'city'];

  constructor(props) {
    super(props);
    this.state = {
      opportunities: [],
      popoverOpen: false
    };
  }
  componentDidMount() {
    this.props.fetchOpportunities();
    console.log('Plop', this.props);
  }
  componentDidUpdate() {
    if (this.props.opportunities.list.length > 0 && this.state.opportunities.length === 0) {
      this.parseOpportunities();
    }
  }
  parseOpportunities = () => {
    let list = [];
    if (this.props.opportunities && this.props.opportunities.list) {
      console.log(this.props);
      list = this.props.opportunities.list.map(item => ({
        name: item.borrowerFullName,
        purpose: item.loanPurpose,
        city: item.propertyCity,
        state: item.propertyState,
        zip: item.propertyZip,
        loanNumber: item.loanNumber,
        lastModified:
          `${new Date(item.dateUpdated).getMonth() + 1}` +
          '/' +
          new Date(item.dateUpdated).getDate() +
          '/' +
          new Date(item.dateUpdated).getFullYear()
      }));
    }
    this.setState({ opportunities: list });
    return list;
  };
  popoverHandler = (event?, cellData?) => {
    if (cellData) {
      this.setState({ popoverOpen: true, rowClick: { target: event.target, data: cellData } });
    } else {
      this.setState(prev => ({ popoverOpen: !prev.popoverOpen }));
    }
  };

  renderPopover = () => {
    console.log(this.state);
    return (
      <Popover
        name={'cell-options'}
        isOpen={true}
        onClose={this.popoverHandler}
        target={this.state.rowClick.target}
        arrowRight={true}
      >
        <ul className={'popover-list'}>
          <li>
            <Icon icon={ICONS.EDIT_PENCIL} />{' '}
            <Link to="/em-prequal/opportunity/:opportunityId/scenarios/compare/">Edit Opportunity</Link>
          </li>
          <li>
            <Icon icon={ICONS.DELETE} /> Delete Opportunity
          </li>
        </ul>
      </Popover>
    );
  };

  render() {
    const { opportunities, popoverOpen } = this.state;
    const { dataHeaders, dataColumns } = OpportunitiesList;
    return (
      <div>
        <div>
          {opportunities.length > 1 ? (
            <DataGrid
              list={opportunities}
              headers={dataHeaders}
              primaryColumns={dataColumns}
              moreActionsIcon={true}
              moreActionsFunc={this.popoverHandler}
            />
          ) : (
            'Loading...'
          )}
        </div>
        {popoverOpen && this.renderPopover()}
      </div>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(OpportunitiesList)
);
