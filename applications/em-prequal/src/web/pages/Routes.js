import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { CompareScenarios } from './Scenarios/CompareScenarios';
// import { fetchUser } from '../../redux/User/user.actions';
// import { fetchSettings } from '../../redux/Settings/settings.actions';
// import { Navbar } from '../components/Navbar/Navbar';
// import { SummaryBar } from '../components/SummaryBar/SummaryBar';

// const mapStateToProps = state => ({
//   user: state.user,
//   setting: state.settings
// });

// const mapDispatchToProps = dispatch => ({
//   fetchUser: () => dispatch(fetchUser()),
//   fetchSettings: () => dispatch(fetchSettings())
// });

// class Routes extends Component {
//   componentDidMount() {
//     this.props.fetchUser();
//     this.props.fetchSettings();
//   }

//   render() {
//     return (
//       <Router basename="/em-prequal">
//         <div>
//           <Navbar />
//           <Switch>
//             <Route exact path="/opportunity/:opportunityId/scenarios/compare/" component={CompareScenarios} />
//           </Switch>
//         </div>

// import OpportunitiesList from './Opportunities/OpportunitiesList';
import { Opportunities } from './Opportunities/Opportunities';

const mapStateToProps = state => ({
  user: state.user
});

class Routes extends Component {
  render() {
    return (
      <Router basename="/em-prequal">
        <Switch>
          {/* <Route exact path="/" component={OpportunitiesList} /> */}
          {/* <Route exact path="/opportunity/:opportunityId/scenarios/compare/" component={CompareScenarios} /> */}
          <Route exact path="/" component={Opportunities} />
        </Switch>
      </Router>
    );
  }
}

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Routes);
export default connect(mapStateToProps)(Routes);
