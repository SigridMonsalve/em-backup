import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Spinner, Popover, SortList, Heading, Icon, ICONS, Label } from '@elliemae/em-dimsum';
import { fetchScenarios } from '../../../redux/Scenarios/scenarios.actions';
import { ScenarioComparisonCardList } from '../../components/ScenarioComparisonCardList/ScenarioComparisonCardList';

const mapStateToProps = state => ({
  scenarios: state.scenarios
});

const mapDispatchToProps = dispatch => ({
  fetchScenarios: data => dispatch(fetchScenarios(data))
});

class CompareScenariosComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSortPopoverOpen: false,
      isSharePopoverOpen: false,
      targetPopover: {}
    };
  }

  managePopover = (event, type, status) => {
    console.log('mangePopover');
    if (type === 'sort') {
      this.setState({
        isSortPopoverOpen: status,
        targetPopover: event.target ? event.target : this.state.targetPopover
      });
    } else {
      this.setState({
        isSharePopoverOpen: status,
        targetPopover: event.target ? event.target : this.state.targetPopover
      });
    }
  };

  componentDidMount() {
    this.props.fetchScenarios();
  }

  onSortChangeHandler = event => {
    this.timeout = setTimeout(() => {
      this.managePopover({}, 'sort', false);
    }, 200);
  };

  renderSortPopover = () => {
    return (
      <Popover
        isOpen={this.state.isSortPopoverOpen}
        target={this.state.targetPopover}
        handleClose={() => this.managePopover({}, 'sort', false)}
        arrowRight={true}
        title="Sort By"
      >
        <SortList
          name="scenario-sort"
          onChange={this.onSortChangeHandler}
          sortOptions={this.getSortOptions()}
          className="options-sort"
        />
      </Popover>
    );
  };

  renderSharePopover = scenariosLength => {
    return (
      <Popover
        title={`Scenario Comparison (${scenariosLength})`}
        isOpen={this.state.isSharePopoverOpen}
        target={this.state.targetPopover}
        handleClose={() => this.managePopover({}, 'share', false)}
        arrowRight={true}
      >
        <div className="option-items-container">
          <div className="option-item" onClick={() => this.managePopover({}, 'share', false)}>
            <Icon icon={ICONS.MESSAGES} className="item-icon" alt="mail-icon" size={20} />
            <Label>Email</Label>
          </div>
          <div className="option-item" onClick={() => this.managePopover({}, 'share', false)}>
            <Icon icon={ICONS.SETTINGS} className="item-icon" alt="mail-icon" size={20} />
            <Label>Refine Search Results</Label>
          </div>
        </div>
      </Popover>
    );
  };

  getSortOptions = () => {
    return [
      { id: 'loan-type', label: 'Loan Type', value: 'Loan Type' },
      { id: 'payment', label: 'Payment', value: 'Payment' },
      { id: 'term', label: 'Term', value: 'Term' }
    ];
  };

  render() {
    // TODO: opportunityId, scenarioId we will get from props when we hook it up with redux
    const { scenarios } = this.props;
    const { opportunityId } = 3;

    const scenariosSelected = Object.keys(scenarios).map(key => {
      return scenarios[key];
    });

    return (
      <div className="page comparison">
        <div className="header-comparison">
          <Heading level={3}>{`Scenario Comparison ${
            !scenarios.isLoading ? `(${scenariosSelected.length - 1})` : ''
          }`}</Heading>
          <div className="icon-container">
            <Icon
              icon={ICONS.SORT}
              className="sort-icon"
              alt="sort-icon"
              onClick={e => this.managePopover(e, 'sort', true)}
              size={16}
            />
            <Icon
              icon={ICONS.MORE_OPTIONS_VERT}
              className="message-icon"
              alt="options-icon"
              onClick={e => this.managePopover(e, 'share', true)}
              size={16}
            />
          </div>
        </div>
        {scenarios && scenarios.isLoading ? (
          <Spinner />
        ) : (
          <React.Fragment>{scenarios && <ScenarioComparisonCardList scenarios={scenariosSelected} />}</React.Fragment>
        )}
        {this.state.isSharePopoverOpen && this.renderSharePopover(scenariosSelected.length - 1)}
        {this.state.isSortPopoverOpen && this.renderSortPopover()}
      </div>
    );
  }
}

const CompareScenarios = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CompareScenariosComponent)
);
export { CompareScenarios };
