#!/usr/bin/env bash

# This is very temporary code for creating new application based on template...
# Only works on unixy os... Replace with some sensible system...

apps_path=applications
template_path=template/*

app_name=$1
app_name_lower=`echo ${app_name} | tr "[:upper:]" "[:lower:]" `
app_path="${apps_path}/$1"

echo "Generating new app at ${app_path}"

mkdir -p $app_path
rsync -aq --progress $template_path $app_path --exclude node_modules

find ${app_path} -type f -exec sed -i.bak "s/em-app/$app_name/g" {} \;
# find ${app_path} -type f -exec sed -i.bak "s/em-app/$app_name_lower/g" {} \;
find ${app_path} -type f -iname "*bak" -exec rm {} \;
