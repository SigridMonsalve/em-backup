node('rhel7 && docker') {
  dir("${GIT_REPO}") {
    stage('CLEANUP') {
      deleteDir()
    }

    def myEnvironment = "${ENVIRONMENT}"
    def err = null
   
    env.DEPLOY_FOLDER = "${WORKSPACE}/${GIT_REPO}/deploy"
    env.DEPLOY_FILE = "deploy.tar"
    currentBuild.result = "SUCCESS"
    env.ARTIFACT_ID = "${DEPLOY_ARTIFACT_VERSION}"
    env.ENVIRONMENT = "${ENVIRONMENT}"
    env.ENV_DEPLOY = "${ENVIRONMENT}"

    env.HIPCHAT_ROOM = "122"
    env.HIPCHAT_API_TOKEN = "5WK6G3gNwwwA2LWQqeOLApzR5GeNXhgvOgYSFbnW"
    
    if ("QA" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-qa.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-qa/em-prequal/"
      env.DISTRIBUTION_ID = "E3G3UM73ZUKZ99"
    } else if ("INT" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-int.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-int/em-prequal/"
      env.DISTRIBUTION_ID = "E3B90H4FFTNEVJ"
    } else if ("PEG" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-peg.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-peg/em-prequal/"
      env.DISTRIBUTION_ID = "E27I70WAK9HFTO"
    } else if ("PEG2" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-peg2.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-peg2/em-prequal/"
      env.DISTRIBUTION_ID = "E2PDDTRSJ1ST4C"
    } else if ("RELQA" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-relqa.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-relqa/em-prequal/"
      env.DISTRIBUTION_ID = "E283CS2EUCG45A"
    } else if ("QA2" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-qa2.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-qa2/em-prequal/"
      env.DISTRIBUTION_ID = "E19WMT3WU9UHW2"
    } else if ("QA3" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-qa3.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-qa3/em-prequal/"
      env.DISTRIBUTION_ID = "E3BE0XV837JRC7"
    } else if ("ETET" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-etet.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-etet/em-prequal/"
      env.DISTRIBUTION_ID = "E3JCC21DU3592W"
    } else if ("XP" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-xp.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-xp/em-prequal/"
      env.DISTRIBUTION_ID = "E2V5ZRUNCPQB5R"
    } else if ("OAPI" == myEnvironment) {
      env.ENV_URL = "http://em-encompass-us-west2-oapi.s3-website-us-west-2.amazonaws.com"
      env.S3_BUCKET = "s3://em-encompass-us-west2-oapi/em-prequal/"
      env.DISTRIBUTION_ID = "E4LTEL8VVBMYQ"
    } else if ("DEV2" == myEnvironment) {
      env.S3_BUCKET = "s3://em-encompass-us-west2-dev2/em-prequal/"
      env.DISTRIBUTION_ID = "E2WREIUVB6C2O4"
    } else if ("DEV" == myEnvironment) {
      env.S3_BUCKET = "s3://em-encompass-us-west2-dev/em-prequal/"
      env.DISTRIBUTION_ID = "E1D4WN4AP77GOI"
    }

    stage('ENV') {
      echo sh(returnStdout: true, script: 'env')
      echo sh(returnStdout: true, script: 'pwd')
      echo sh(returnStdout: true, script: 'ls -lart')
    }

    try {

        stage('DOWNLOAD DEPLOY FILE') {
            sh '''
            echo "deploy folder is ${DEPLOY_FOLDER}"
            rm -rf *

            curl -X GET \
            -o ${DEPLOY_FILE} \
            -u deployment:deployment123 \
            "http://eq1vutcrm01.dco.elmae:8081/nexus/service/local/artifact/maven/content?r=AppDevelopment&g=com.ellieMae.Apps&e=tar&a=${NEXUS_APP_NAME}&v=${ARTIFACT_ID}"
        
            tar -xvf ${DEPLOY_FILE}
            rm ${DEPLOY_FILE}
            echo "list deploy folder" && ls -altr ${DEPLOY_FOLDER}
            '''
        }

        stage('DEPLOYMENT PUSH') {
            println "actual push "
            wrap([$class       : 'AmazonAwsCliBuildWrapper',
                credentialsId: 'svc-jenkins-aws-non-prod',
                defaultRegion: 'us-west-2']) {
            sh '''
                echo "deploy folder ${DEPLOY_FOLDER}"
                echo "listing on deploy folder " && ls -altr ${DEPLOY_FOLDER}
                cd ${DEPLOY_FOLDER}
                
                # Figure out the filename for env.<id>.chunk.js in static folder
                # call sed to replace LOCALHOST with given ${ENVIRONMENT}
                
                export ENV_FILE=$(ls ./static/js/env.*.chunk.js)
                COMMIT_ID=$(cat commit)
                ARTIFACT_VERSION=$(cat version)
                sed -i "s/LOCALHOST/${ENV_DEPLOY}/" $ENV_FILE
                sed -i "s/REPLACE_APP_VERSION_ARTIFACTID/emprequal-${COMMIT_ID}-${ARTIFACT_VERSION}/" index.html
                rm commit
                rm version

                aws s3 sync . ${S3_BUCKET} --delete --no-progress --exclude "*docs*" --exclude "*evp/*" --exclude "*builder/*" --exclude "*release*/*" --exclude "*master/*" --exclude "*host-adaptor/*" --exclude "*vendors/*" --exclude "*renderer/*"
                aws s3 cp index.html ${S3_BUCKET} --no-progress --cache-control "max-age=0, no-cache"
                aws s3 cp service-worker.js ${S3_BUCKET} --no-progress --cache-control "max-age=0, no-cache"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.svg" --content-type "image/svg+xml"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.ttf" --content-type "application/x-font-ttf"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.otf" --content-type "application/x-font-opentype"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.woff" --content-type "application/font-woff"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.woff2" --content-type "application/font-woff2"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.eot" --content-type "application/vnd.ms-fontobject"
                aws s3 cp . ${S3_BUCKET} --no-progress --metadata-directive="REPLACE" --recursive --exclude "*" --include "*.js" --content-type "application/javascript"

                if [ "${ENVIRONMENT}" == "DEV" || "${ENVIRONMENT}" == "DEV2" ]; then
                    echo "listing on em-prequal folder see there is docs " && ls -altr ${DEPLOY_FOLDER}/docs
                    cd docs
                    aws s3 sync . ${S3_BUCKET}docs/ --delete --no-progress
                    aws cloudfront create-invalidation --distribution-id ${DISTRIBUTION_ID} --paths "/*"
                fi
            '''
            sh '''
                aws s3 cp ${S3_BUCKET}index.html deployed.html --no-progress
                cat deployed.html
            '''
            }
            hipchatSend color: 'GREEN',
                message: "Completed EM-PREQUAL Deploy ${env.ARTIFACT_ID} to " + myEnvironment,
                notify: true,
                room: "${env.HIPCHAT_ROOM}",
                sendAs: '',
                server: 'hipchat.elliemae.io',
                textFormat: true,
                token: "${env.HIPCHAT_API_TOKEN}",
                v2enabled: true
        }

    } catch (exception) {
      err = exception
    } finally {
      if (err) {
        println("printing error text when Environment = ${ENVIRONMENT}")
        throw err
      }
    }
  }
}
