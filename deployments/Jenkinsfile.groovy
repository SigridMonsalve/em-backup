// Ensure below env variables are passed from Jenkins Job
// GIT_REPO
// GIT_BRANCH
node('rhel7 && docker') {
  env.REPO_NAME = "http://githubdev.dco.elmae/Elliemae/em-prequal/"
  env.HIPCHAT_ROOM = "122"
  env.HIPCHAT_API_TOKEN = "UFWgYkCVf2NQptEBi4aCW7rV8M5yzwFtMCWccaVk"
  env.HIPCHAT_MENTION = "@all"

  def devEnv = "${DEV_ENV}"
  def environment = "${ENVIRONMENT}"
  def artifactCodelineTag = "${artifactCodelineTag}"
  env.DEPLOY_FOLDER = "${WORKSPACE}/em-prequal/deploy"
  env.BRANCH = "${GIT_BRANCH}"
  env.NEXUS_APP_NAME = "EmPrequalcompassApp.${artifactCodelineTag}"

  env.TAG_NAME_PREFIX = "${TAG_PREFIX}"
  env.DEPLOY_FILE = "deploy.tar"
  if ("${env.TAG_NAME_PREFIX}" != "") {
    env.TAG_NAME = "${artifactCodelineTag}-${env.TAG_NAME_PREFIX}-${env.BUILD_NUMBER}"
  } else {
    env.TAG_NAME = "${artifactCodelineTag}-${env.BUILD_NUMBER}"
  }

  if (devEnv == "DEV2") {
    env.DEV_URL = "http://dev2.encompass.elliemae.io/em-prequal/"
  } else {
    env.DEV_URL = "http://encompass-dev.elliemae.io/em-prequal/"
  }

  def err = null
  def errMsg = ""
  currentBuild.result = "SUCCESS"

  stage('ENV') {
    echo sh(returnStdout: true, script: 'env')
    echo sh(returnStdout: true, script: 'pwd')
    echo sh(returnStdout: true, script: 'ls -lart')
  }

  dir("${GIT_REPO}") {
    try {
      stage('CLEANUP') {
        deleteDir()
      }

      stage('CHECKOUT') {
        env.gitRemote = "githubdev.dco.elmae/Elliemae/${GIT_REPO}.git"
        checkout([
            $class                           : 'GitSCM',
            branches                         : [[name: "refs/heads/${GIT_BRANCH}"]],
            depth                            : 1,
            doGenerateSubmoduleConfigurations: false,
            extensions                       : [[$class: 'WipeWorkspace']],
            submoduleCfg                     : [],
            userRemoteConfigs                : [[credentialsId: '62bc9b3f-0b49-456b-b01a-ea1fe0950979',
                                                 url          : "${env.REPO_NAME}",
                                                 poll         : "true"]]
        ])
        env.COMMIT_ID = sh(returnStdout: true, script: 'git rev-parse refs/remotes/origin/${GIT_BRANCH}^{commit}').trim()
      }

      stage('DOCKER') {
        timeout(45) {
          docker.withServer('unix:///var/run/docker.sock') {
            docker.withRegistry('https://index.docker.io/v1/', '05bb85cd-94a5-48cc-9a78-485217dddf5f') {
              sh 'docker build -t buildenv --no-cache --pull --file docker/Dockerfile .'
            }
          }
        }
      }

      def dockerInsideArgs = "--privileged -e \"APP_DIR=/usr/src/app\" -e WORKSPACE=\"${WORKSPACE}\" -e \"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\" -e \"HOME=/tmp/prequal/build\" -e \"DEPLOY=${env.DEPLOY_FOLDER}\""
      println("running docker.image ${WORKSPACE}")
      docker.image('buildenv').inside(dockerInsideArgs) {

        // BUILD
        stage('BUILD') {
          timeout(15) {
            sh '''
              cd $APP_DIR
            
              yarn
              cd applications/em-dimsum && preup

              cd $APP_DIR/applications/em-prequal
              yarn build

              # Copy files to workspace for deployment if tests pass
              rm -rf $DEPLOY
              mkdir $DEPLOY || echo "directory already exists"
              cd $APP_DIR/applications/em-prequal/build
              echo "listing in build folder" && ls -altr $APP_DIR/applications/em-prequal/build
              cp -rf $APP_DIR/applications/em-prequal/build/* $DEPLOY/.
              cd $DEPLOY
              echo "listing in deploy folder" && ls -altr $APP_DIR/applications/em-prequal/build
              echo "copy success into $DEPLOY"

            '''
          }
        }

        // BUNDLE
        stage('BUNDLE') {
          sh '''
            cd $DEPLOY
            cp -R ${WORKSPACE}/em-prequal/docs ./docs
            #Remove api folder, it only contains json files
            #remove this later
            #rm -rf api  

            # Apply a version file to the deployment for future reference
            # touch "${TAG_NAME}.ver"
            echo "${TAG_NAME}" > version
            echo "${COMMIT_ID}" > commit
            cd ..
              echo "listing in deploy folder before cleanup" && ls -altr $APP_DIR/applications/em-prequal/build

            # Cleanup any old package/tar files
            rm -f ${DEPLOY_FILE}
            echo "listing in deploy folder after cleanup" && ls -altr $DEPLOY
            tar -cf ${DEPLOY_FILE} deploy
            echo "list the ${DEPLOY_FILE}" && pwd && ls -altr
          '''
        }
      } // docker

      stage('NEXUS') {
        // Push to Artifact Repo
        println("sending to nexus before push")
        sh '''
          # Push to Nexus
          curl -s \
              -S \
              -F r=AppDevelopment \
              -F g=com.ellieMae.Apps \
              -F a="${NEXUS_APP_NAME}" \
              -F v="${TAG_NAME}" \
              -F p=tar \
              -F file=@./${DEPLOY_FILE} \
              -u deployment:deployment123 \
              http://eq1vutcrm01.dco.elmae:8081/nexus/service/local/artifact/maven/content

          # Push to JFrog
          # curl -v -uadmin:APmAV7aSxD2Wsr4xsmagriP5AS -T ${env.DEPLOY_FOLDER}/${DEPLOY_FILE} "http://eqarch5000301.dco.elmae:8081/artifactory/${NEXUS_APP_NAME}/${TAG_NAME}"
        '''
      }

      // PUSH
      stage('DEV PUSH') {          
        build job: 'BuildPrequalPromote',
          parameters: [
            [$class: 'StringParameterValue', name  : 'ENVIRONMENT', value : "${DEV_ENV}"],
            [$class: 'StringParameterValue', name  : 'NEXUS_APP_NAME', value : "${NEXUS_APP_NAME}"],
            [$class: 'StringParameterValue', name  : 'DEPLOY_ARTIFACT_VERSION', value : "${TAG_NAME}"],
          ],
          wait: true                
      }   

/*
          stage('HEALTH_CHECK') {
            def promoteImmediately = true
            try {
              timeout(60) {
                build job: 'ENCW_APP/EMPREQUAL_DEV_HealthCheck',
                  parameters: [
                    [$class: 'StringParameterValue',
                    name  : 'DEV_URL',
                    value : "${env.DEV_URL}"]],
                  quietPeriod: 0,
                  wait: true
              }
            } catch (caughtError) {
              promoteImmediately = true
              errMsg = "QA Health Check failed."
              err = caughtError
            }
          }
*/
      stage('PROMOTE FOR QA') {
        if (promoteImmediately && err == null) {
          def envs = environment.trim().split(',').toList().unique()
          envs.each {
            build job:
                'BuildPrequalPromote',
                parameters: [
                    [$class: 'StringParameterValue', name  : 'ENVIRONMENT', value : it],
                    [$class: 'StringParameterValue', name: 'NEXUS_APP_NAME', value: "${NEXUS_APP_NAME}"],
                    [$class: 'StringParameterValue', name  : 'DEPLOY_ARTIFACT_VERSION', value : "${TAG_NAME}" ]
                ],
                wait: true
          }
        }
        if (err != null) {
          println "Health check failed.  Not promoting!!!!"
          throw err
        }
      }

    } catch (exception) {
      err = exception;

      currentBuild.result = "FAILURE"

      hipchatSend color: 'RED',
        message: '@all BUILD FAILURE: $JOB_NAME #$BUILD_NUMBER ($CHANGES_OR_CAUSE)' + errMsg + ' (<a href="$URL">Open</a>)',
        notify: true, room: "${env.HIPCHAT_ROOM}",
        sendAs: '',
        server: 'hipchat.elliemae.io',
        message_format: 'html',
        token: "${env.HIPCHAT_API_TOKEN}",
        v2enabled: true

    } finally {
      stage('CLEANUP') {
        // remove all containers and ignore errors - i.e. don't report failures
        sh 'docker ps -as --no-trunc --format \'{{.ID}}\' | xargs --no-run-if-empty docker rm || echo "Some containers not removed"'
        // remove all images and ignore errors - i.e. don't report failures if disk usage % > 90
        sh '''
        if [ `df /var -P | awk '+$5 >= 90 {print "TRUE"}'` == "TRUE"  ]; then
          docker images --no-trunc --format \'{{.ID}}\' | xargs --no-run-if-empty docker rmi || echo "Some images not removed"
        fi
        '''
      }
      /* Must re-throw exception to propagate error */
      if (err) {
        throw err
      }
    }

  }
}
